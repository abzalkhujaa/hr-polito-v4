<div class="col-lg-4 col-md-4 col-sm-12 col-12">
    @include('components.staff.card')
    @role(\App\Helpers\Enums\UserRoleEnum::STAFF)
        @include('components.staff.staff-qrcode-button')
    @else
        @include('components.staff.qrcode-button')
    @endrole

</div>

