<div class="card">
    <div class="card-body">
        <div class="card-content">
            <div class="row align-items-center">
                <div class="col-4">
                    <div>
                        <img src="{{ $user->qr_code }}" class="w-100 rounded" alt="QR Code">
                    </div>
                </div>
                <div class="col-8">
                    @if($user->has_certificate)
                        <a href="{{ route('staff-dashboard.staff.reference') }}" class="btn btn-primary btn-block">
                            <i class="feather icon-file-text"></i> Объективка
                        </a>
                    @else
                        <button disabled="disabled" class="btn btn-outline-danger btn-block disabled" title="Заполняйте все поля">
                            <i class="feather icon-x-circle"></i> Объективка
                        </button>
                    @endif
                    @if($user->labor)
                        <a href="{{ route('staff-dashboard.staff.certificate') }}" class="btn btn-primary btn-block">
                            <i class="feather icon-file"></i> Справка
                        </a>
                    @else
                        <button disabled="disabled" class="btn btn-primary btn-block">
                            <i class="feather icon-x-circle"></i> Справка
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
