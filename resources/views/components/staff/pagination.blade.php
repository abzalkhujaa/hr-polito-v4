<div class="row">
    <div class="col-md order-md-1 col-sm-4 order-sm-1 col-6 order-1">
        <a
                href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.profile.index') : route('dashboard.staff.profile.index', ['user' => $user->username]) }}"
                class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::MAIN) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::MAIN)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::MAIN) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::MAIN) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::MAIN) text-white @endif">
                    {{ __('dashboard.profile') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-2 col-sm-4 order-sm-2 col-6 order-2">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.information.index') : route('dashboard.staff.information.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::INFORMATION) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::INFORMATION)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::INFORMATION) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::INFORMATION) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::INFORMATION) text-white @endif">
                    {{ __('dashboard.info') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-3 col-sm-4 order-sm-3 col-6 order-3">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.education.index') : route('dashboard.staff.education.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::EDUCATION) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::EDUCATION)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::EDUCATION) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::EDUCATION) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::EDUCATION) text-white @endif">
                    {{ __('dashboard.education') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-4 col-sm-12 order-sm-4 col-12 order-5">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.labor.index') : route('dashboard.staff.labor.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::LABOR) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::LABOR)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::LABOR) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::LABOR) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::LABOR) text-white @endif">
                    {{ __('dashboard.labor') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-5 col-sm-4 order-sm-5 col-6 order-4">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.work.index') : route('dashboard.staff.work.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::WORK) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::WORK)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::WORK) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::WORK) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::WORK) text-white @endif">
                    {{ __('dashboard.work') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-6 col-sm-4 order-sm-6 col-6 order-6">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.family.index') : route('dashboard.staff.family.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::FAMILY) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::FAMILY)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::FAMILY) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::FAMILY) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::FAMILY) text-white @endif">
                    {{ __('dashboard.family') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-7 col-sm-4 order-sm-7 col-6 order-7">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.file.index') : route('dashboard.staff.file.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::FILE) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::FILE)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::FILE) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::FILE) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::FILE) text-white @endif">
                    {{ __('dashboard.file') }}
                </h4>
            </div>
        </a>
    </div>
    <div class="col-md order-md-8 col-sm-12 order-sm-8 col-12 order-8">
        <a href="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.vacation.index') : route('dashboard.staff.vacation.index', ['user' => $user->username]) }}" class="card user-pagination-item @if($active === \App\Helpers\Enums\PaginationTypeEnum::VACATION) bg-primary @endif mb-1 mt-0 my-md-1">
            <div class="card-body text-center px-0">
                <h2>
                    <i class="feather {{$user->getIcon(\App\Helpers\Enums\PaginationTypeEnum::VACATION)}}  @if($active === \App\Helpers\Enums\PaginationTypeEnum::VACATION) text-white @else {{ $user->getColor(\App\Helpers\Enums\PaginationTypeEnum::VACATION) }} @endif"></i>
                </h2>
                <br>
                <h4 class="@if($active === \App\Helpers\Enums\PaginationTypeEnum::VACATION) text-white @endif">
                    {{ __('dashboard.vacation') }}
                </h4>
            </div>
        </a>
    </div>
</div>
