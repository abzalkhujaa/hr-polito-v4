<div class="card">
    <div class="card-body">
        <div class="card-content">
            <div class="row align-items-center">
                <div class="col-4">
                    <div>
                        <img src="{{ $user->avatar }}" class="img-fluid img-thumbnail rounded" alt="">
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-center">
                        <div class="text-secondary">{{ __('dashboard.full_name') }}</div>
                        <small class="font-weight-bold text-dark">
                            {{ (!is_null($user->first_name)) ? $user->full_name : __('dashboard.data_empty') }}
                        </small>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="text-secondary">{{ __('dashboard.email') }}</div>
                            <small class="font-weight-bold text-dark">{{ $user->email }}</small>
                            <div class="text-secondary pt-1">{{ __('dashboard.info_field.phone') }}</div>
                            <small class="font-weight-bold text-dark">
                                {{ ($user->hasInformation()) ? $user->information->phone : __('dashboard.data_empty') }}
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row align-items-center">
            <div class="col-md-4 col-5 text-center">{{ __('dashboard.info_field.address') }}</div>
            <div class="col-md-8 col-7">
                <small class="font-weight-bold text-dark">
                    {{ ($user->hasInformation()) ? $user->information->address : __('dashboard.data_empty') }}
                </small>
            </div>
        </div>
    </div>
</div>
