<fieldset class="form-label-group mt-1">
    <input
            type="{{ $type }}"
            class="form-control @error($name) is-invalid @enderror"
            id="{{ $idName }}"
            name="{{ $name }}"
            placeholder="{{ $label }}"
            {{ $attributes->wire('model') }}
            @if($required) {{ 'required' }} @endif
            @if($disabled) {{ 'disabled' }} @endif
            value="{{ old($name) ?? $value }}"
            @if($pattern)
                {{ $pattern }}
            @endif
    >
    <label for="{{ $name }}">{{ $label }}</label>
    @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</fieldset>

