<div class="container">
    <div class="card">
        <div class="card-header"></div>
        <div class="card-body">
            <form action="{{ route('dashboard.decree.update',['decree' => $decree->alias]) }}" method="post">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-12">
                        <x-html.input type="text" name="title" :value="$decree->title" :label="__('dashboard.news_field.title')" :required="true" :disabled="false"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="card card-apply-job">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center mb-1">
                                    <div class="media">
                                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                                            @if($image)
                                                <i class="feather icon-check-circle h1 text-success"></i>
                                            @else
                                                <i class="feather icon-x-circle h1 text-danger"></i>
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h5 class="mb-0">{{ __('dashboard.file_field.image') }}</h5>
                                            <small class="text-muted">image</small>
                                        </div>
                                    </div>
                                </div>
                                <p class="card-text mb-2">
                                    Рекомендуемые размеры : <strong>3 Mb</strong><br>
                                    Рекомендуемые файлы : <strong>JPG, PNG</strong>
                                </p>
                                <div class="mb-1">
                                    <small>
                                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                                    </small>
                                </div>
                                <div class="input-group mb-1">
                                    <div class="custom-file">
                                        <input type="file" wire:model="uploaded" class="custom-file-input" id="image" aria-describedby="image">
                                        <label class="custom-file-label text-center" for="image"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                                    </div>
                                </div>
                                @error('uploaded')
                                <small class="text-danger my-1">
                                    <i class="feather icon-x-circle"></i> {{ $message }}
                                </small>
                                @enderror
                            </div>
                            <div class="card-footer">
                                @if ($uploaded and $uploaded->isPreviewable() and $uploaded->temporaryUrl())
                                    <div class="mb-3">
                                        Предварительный просмотр:
                                        <div class="text-center">
                                            <img src="{{ $uploaded->temporaryUrl() }}" class="img-thumbnail w-50" alt="Preview">
                                        </div>
                                    </div>
                                @elseif($decree->image)
                                    <div class="mb-3">
                                        {{ __('dashboard.file_field.image') }}:
                                        <div class="text-center">
                                            <img src="{{ asset('storage/'.$decree->image) }}" class="img-thumbnail w-50" alt="Preview">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-apply-job">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center mb-1">
                                    <div class="media">
                                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                                            @if($file)
                                                <i class="feather icon-check-circle h1 text-success"></i>
                                            @else
                                                <i class="feather icon-x-circle h1 text-danger"></i>
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h5 class="mb-0">{{ __('Файл') }}</h5>
                                            <small class="text-muted">file</small>
                                        </div>
                                    </div>
                                </div>
                                <p class="card-text mb-2">
                                    Рекомендуемые размеры : <strong>3 Mb</strong><br>
                                    Рекомендуемые файлы : <strong>JPG, PNG, PDF, DOC, DOCX</strong>
                                </p>
                                <div class="mb-1">
                                    <small>
                                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                                    </small>
                                </div>
                                <div class="input-group mb-1">
                                    <div class="custom-file">
                                        <input type="file" wire:model="uploaded2" class="custom-file-input" id="image" aria-describedby="image">
                                        <label class="custom-file-label text-center" for="image"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                                    </div>
                                </div>
                                @error('uploaded2')
                                <small class="text-danger my-1">
                                    <i class="feather icon-x-circle"></i> {{ $message }}
                                </small>
                                @enderror
                            </div>
                            <div class="card-footer">
                                @if ($uploaded2 and $uploaded2->isPreviewable() and $uploaded2->temporaryUrl())
                                    <div class="mb-3">
                                        Предварительный просмотр:
                                        <div class="text-center">
                                            <img src="{{ $uploaded2->temporaryUrl() }}" class="img-thumbnail w-50" alt="Preview">
                                        </div>
                                    </div>
                                @else
                                    @if($file)
                                        <div class="mb-3">
                                            Файл:
                                            <div class="text-center">
                                                <a href="{{ asset('storage/'.$file) }}" download class="btn btn-primary">Скачать</a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="editor">{{ __('dashboard.news_field.content') }}</label>
                    <textarea id="editor" name="content" class="rounded shadow-sm" style="min-height: 200px">{!! $decree->content !!}</textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" wire:model="image" value="{{ $decree->image }}" name="image">
                    <input type="hidden" wire:model="file" value="{{ $decree->file }}" name="file">
                    <button type="submit" class="btn btn-primary btn-block waves-effect waves-float waves-light">
                        <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.edit') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>




