<section class="app-user-list">
    <!-- users filter start -->
    <div class="card">
        <h5 class="card-header">Фильтр поиска</h5>
        <div class="d-flex justify-content-between align-items-center mx-50 row pt-0 pb-2">
            <div class="col-md-4 user_role">
                <label for="staff_role">{{ __('dashboard.role') }}</label>
                <select id="staff_role" class="custom-select" wire:change="changeRoleFilter($event.target.value)">
                    <option value="0">Все</option>
                    @forelse($roles as $one)
                        <option value="{{ $one->id }}">{{ $one->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            <div class="col-md-4 user_section">
                <label for="staff_role">{{ __('dashboard.sections') }}</label>
                <select id="staff_role" class="custom-select" wire:change="changeSectionFilter($event.target.value)">
                    <option value="0">Все</option>
                    @forelse($sections as $one)
                        <option value="{{ $one->id }}">{{ $one->title }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            <div class="col-md-4 user_department">
                <label for="staff_role">{{ __('dashboard.departments') }}</label>
                <select id="staff_role" class="custom-select" wire:change="changeDepartmentFilter($event.target.value)">
                    <option value="0">Все</option>
                    @forelse($departments as $one)
                        <option value="{{ $one->id }}">{{ $one->title }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>
    </div>
    <!-- users filter end -->
    <!-- list section start -->
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="mr-auto">
                    <div class="form-group">
                        <input type="text" wire:model="search" class="form-control pr-5" placeholder="Поиск...">
                    </div>
                </div>
                <div class="px-2">
                    <a class="btn add-new btn-success mt-50" href="{{ route('dashboard.staff.table') }}">
                        <i class="feather icon-database mr-1"></i> <span>{{ __('page.dashboard.staff.table') }}</span>
                    </a>
                </div>
                <div class="px-2">
                    <button class="btn add-new btn-primary mt-50" tabindex="0" type="button" data-toggle="modal" data-target="#modals-slide-in">
                        <i class="feather icon-user-plus mr-1"></i> <span>{{ __('page.dashboard.staff.create') }}</span>
                    </button>
                </div>
            </div>
        </div>
        <!-- Modal to add new user starts-->
        <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
            <div class="d-flex align-items-center justify-content-center h-100 w-75">
                <img src="{{asset('images/pages/login-v2.svg')}}" class="img-fluid" alt="">
            </div>
            <div class="modal-dialog">
                <form class="add-new-user modal-content pt-0 was-validated" method="POST" action="{{ route('dashboard.staff.store') }}">
                    @csrf
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title" id="user_create">{{ __('dashboard.new_user')}}</h5>
                    </div>
                    <div class="modal-body flex-grow-1">
                        <div class="mb-2">
                            <x-html.input name="username" :label="__('dashboard.username')" type="text" pattern="{{ 'pattern=^[a-z0-9_]{1,30}$' }}" :disabled="false" :required="true" />
                            <small class="form-text text-muted">Вы можете использовать строчные латинские буквы, цифры и подчеркивание. </small>
                        </div>
                        <div class="mb-2">
                            <x-html.input name="email" :label="__('dashboard.email')" type="email" :disabled="false" :required="true" />
                        </div>
                        <div class="mb-2">
                            <x-html.input name="password" :label="__('dashboard.password')" type="text" :disabled="false" :required="true" />
                        </div>
                        <div class="mb-2">
                            <x-html.input name="first_name" :label="__('dashboard.profile_field.fname')" type="text" :disabled="false" :required="true" />
                        </div>
                        <div class="mb-2">
                            <x-html.input name="last_name" :label="__('dashboard.profile_field.lname')" type="text" :disabled="false" :required="true" />
                        </div>
                        <div class="mb-2">
                            <x-html.input name="patronymic" :label="__('dashboard.profile_field.pname')" type="text" :disabled="false" :required="true" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="user-role">Пол пользователя:</label>
                            <select id="user-gender" class="form-control" name="gender" required>
                                <option value="N">{{ __('dashboard.no_gender') }}</option>
                                <option value="M">{{ __('dashboard.male') }}</option>
                                <option value="F">{{ __('dashboard.female') }}</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mr-1 data-submit">{{ __('page.dashboard.staff.create') }}</button>
                        <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('message.cancel') }}</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- Modal to add new user Ends-->
    </div>

    <div class="row">
        @forelse($staff as $one)
            <div class="col-md-3 col-sm-12 col-12">
                <a href="{{ route('dashboard.staff.profile.index',['user' => $one->username]) }}" class="card card-profile">
                    <div class="bg-primary card-img-top" style="height: 70px;"></div>
                    <div class="card-body">
                        <div class="profile-image-wrapper">
                            <div class="profile-image">
                                <div class="avatar" style="width: 100px;height: 100px">
                                    <img src="{{ $one->avatar }}" alt="Profile Picture" style="object-fit: cover;object-position: top;">
                                </div>
                            </div>
                        </div>
                        <h3>{{ $one->first_name }} {{ $one->last_name }}<br><small>{{$one->patronymic}}</small></h3>
                        <h6 class="text-muted">{{ $one->labor->position->title ?? __('dashboard.no_information') }}</h6>
                        <div class="badge badge-light-primary profile-badge">{{ $one->information->phone ?? '' }}</div>
                        <div class="badge badge-light-primary profile-badge">{{ $one->email ?? '' }}</div>
                        <hr class="mb-2">
                        <div class="d-flex justify-content-between align-items-center">
                            @foreach(\App\Helpers\Enums\PaginationTypeEnum::toArray() as $pagination)
                                    @continue($pagination === \App\Helpers\Enums\PaginationTypeEnum::VACATION)
                                    <i class="feather  {{ $one->getIcon($pagination) }} {{ $one->getColor($pagination) }} h3"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="{{ __('dashboard.'.$pagination) }}"></i>

                            @endforeach
                        </div>
                    </div>
                </a>
            </div>
        @empty
        @endforelse
    </div>
    <!-- list section end -->
</section>
