
<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            @if($type === \App\Helpers\Enums\ActionTypeEnum::UPDATE)
                @method('PUT')
            @endif
            <h1 class="text-center">{{ __('dashboard.labor_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>
            <div class="row mt-2">
                <div class="col-md-4 col-sm-4 col-12 mb-2">
                    <label for="section_id">{{ __('dashboard.section') }}</label>
                    <select name="section_id" id="section_id" class="form-control" wire:change="selectSection($event.target.value)" @if($disabled) disabled @endif required>
                        <option value="" >{{ __('dashboard.labor_field.section') }}...</option>
                        @forelse($sections as $one)
                            <option value="{{ $one->id }}" @if($temp_section == $one->id) selected @endif>{{ $one->title }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-md-4 col-sm-4 col-12 mb-2">
                    <label for="department_id">{{ __('dashboard.department') }}</label>
                    <select name="department_id" id="department_id" class="form-control" wire:change="selectDepartment($event.target.value)" @if($disabled) disabled @endif required>
                        <option value="" >{{ __('dashboard.labor_field.department') }}...</option>
                        @forelse($departments as $one)
                            <option value="{{ $one->id }}" @if($temp_department == $one->id) selected @endif>{{ $one->title }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-md-4 col-sm-4 col-12 mb-2" >
                    <label for="position_id">{{ __('dashboard.position') }}</label>
                    <select name="position_id" id="position_id" class="form-control" @if($disabled) disabled @endif required>
                        <option value="">{{ __('dashboard.labor_field.position') }}...</option>
                        @forelse($positions as $one)
                            <option value="{{ $one->id }}" @if($temp_position == $one->id) selected @endif>{{ $one->title }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->stavka ?? ''" name="stavka" :required="true" label="{{ __('dashboard.labor_field.stavka') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-4 col-12 mb-2" >
                    <label for="basis_labor">{{ __('dashboard.labor_field.basis_labor') }}</label>
                    <select name="basis_labor" id="basis_labor" class="form-control" @if($disabled) disabled @endif required>
                        <option value="" @if(!$disabled) selected @endif>{{ __('dashboard.labor_field.basis_labor') }}...</option>
                        @forelse(\App\Helpers\Enums\StaffBasisLaborEnum::toArray() as $one)
                            <option value="{{$one}}" @if($user->labor and !is_null($user->labor->basis_labor) and ($one === $user->labor->basis_labor)) selected @endif>{{$one}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->working_limit ?? ''" name="working_limit" :required="true" label="{{ __('dashboard.labor_field.working_limit') }}" :disabled="$disabled"/>
                </div>

                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->start_command ?? ''" name="start_command" :required="true" label="{{ __('dashboard.labor_field.start_command') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->finish_command ?? ''" name="finish_command" :required="true" label="{{ __('dashboard.labor_field.finish_command') }}" :disabled="$disabled"/>
                </div>
                <div class="{{  ($user->gender === 'F') ? 'col-md-4 col-sm-4' : 'col-md-6 col-sm-6' }} col-12">
                    <x-html.input type="text" :value="$user->labor->last_vacation ?? ''" name="last_vacation" :required="true" label="{{ __('dashboard.labor_field.last_vacation') }}" :disabled="$disabled"/>
                </div>
                @if($user->gender === 'F')
                <div class="col-md-4 col-sm-4 col-12">
                    <x-html.input type="text" :value="$user->labor->maternity ?? ''" name="maternity" :required="true" label="{{ __('dashboard.labor_field.maternity') }}" :disabled="$disabled"/>
                </div>
                @endif
                <div class="{{ ($user->gender === 'F') ? 'col-md-4 col-sm-4' : 'col-md-6 col-sm-6' }} col-12">
                    <x-html.input type="text" :value="$user->labor->pensioner ?? ''" name="pensioner" :required="true" label="{{ __('dashboard.labor_field.pensioner') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->development ?? ''" name="development" :required="true" label="{{ __('dashboard.labor_field.development') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->labor->disciplinary ?? ''" name="disciplinary" :required="true" label="{{ __('dashboard.labor_field.disciplinary') }}" :disabled="$disabled"/>
                </div>

                <div class="col-md-4 col-sm-4 col-12">
                    <x-html.input type="text" :value="$user->labor->contract_number ?? ''" name="contract_number" :required="true" label="{{ __('dashboard.labor_field.contract_number') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <x-html.input type="text" :value="$user->labor->employment_num ?? ''" name="employment_num" :required="true" label="{{ __('dashboard.labor_field.employment_num') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <x-html.input type="text" :value="$user->labor->inn_number ?? ''" name="inn_number" :required="true" label="{{ __('dashboard.labor_field.inn_number') }}" :disabled="$disabled"/>
                </div>
                @if(!$disabled)
                    <div class="col-12"><hr></div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-check-circle"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>
