
<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            @if($type === \App\Helpers\Enums\ActionTypeEnum::UPDATE)
                @method('PUT')
            @endif
            <h1 class="text-center">{{ __('dashboard.education_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>
            <ul class="timeline">
                @forelse($user->finished as $one)
                    <li class="timeline-item">
                        <span class="timeline-point timeline-point-indicator"></span>
                        <div class="timeline-event">
                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                <h6>{{ $one->year }} й</h6>
                                @if(!$disabled)
                                    <span class="timeline-event-time">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_finished_education_{{$one->id}}">
                                        <i class="feather icon-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete_finished_education_{{$one->id}}').submit()">
                                        <i class="feather icon-trash"></i>
                                    </button>
                                </span>
                                @endif
                            </div>
                            <div>{{ $one->place }}</div>
                        </div>
                    </li>
                @empty
                    <h3 class="text-center">
                        {{ __('dashboard.no_information') }}
                    </h3>
                @endforelse
            </ul>
            @if(!$disabled)
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_finished_education">
                        <i class="feather icon-plus"></i> {{ __('dashboard.action.add') }}
                    </button>
                </div>
            @endif
            <hr>
            <div class="row mt-2">
                <div class="col-md-12 col-sm-12 col-12">
                    <x-html.input type="text" :value="$user->education->education ?? ''" name="education" :required="true" label="{{ __('dashboard.education_field.education') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-12 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->specialty ?? ''" name="specialty" :required="true" label="{{ __('dashboard.education_field.specialty') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->partying ?? ''" name="partying" :required="true" label="{{ __('dashboard.education_field.partying') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->academic_degree ?? ''" name="academic_degree" :required="true" label="{{ __('dashboard.education_field.academic_degree') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->academic_title ?? ''" name="academic_title" :required="true" label="{{ __('dashboard.education_field.academic_title') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->foreign_lang ?? ''" name="foreign_lang" :required="true" label="{{ __('dashboard.education_field.foreign_lang') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <x-html.input type="text" :value="$user->education->military_rank ?? ''" name="military_rank" :required="true" label="{{ __('dashboard.education_field.military_rank') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-12 col-sm-12 col-12">
                    <x-html.input type="text" :value="$user->education->state_award ?? ''" name="state_award" :required="true" label="{{ __('dashboard.education_field.state_award') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label for="state_member">{{ __('dashboard.education_field.state_member') }}</label>
                        <input type="text" class="form-control" name="state_member" id="state_member" value="{{ $user->education->state_member ?? '' }}" required @if($disabled) {{ 'disabled' }} @endif>
                    </div>
                </div>
                @if(!$disabled)
                    <div class="col-12"><hr></div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-check-circle"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>

<div class="modal fade text-left"
     id="add_finished_education"
     tabindex="-1"
     role="dialog"
     aria-labelledby="add_finished_education_label"
     aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_finished_education_label">{{ __('dashboard.education_field.finished') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.education.finished.store') : route('dashboard.staff.education.finished.store', ['user' => $user->username]) }}" method="POST" class="was-validated">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <x-html.input type="text" :disabled="false" name="year" :required="true" label="{{ __('dashboard.education_field.year') }}"/>
                        </div>
                        <div class="col-6">
                            <x-html.input type="text" :disabled="false" name="place" :required="true" label="{{ __('dashboard.education_field.finished') }}"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('dashboard.action.add') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@forelse($user->finished as $one)
    <div class="modal fade text-left"
         id="edit_finished_education_{{$one->id}}"
         tabindex="-1"
         role="dialog"
         aria-labelledby="edit_finished_education_label_{{$one->id}}"
         aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="edit_finished_education_label_{{$one->id}}">{{ __('dashboard.education_field.finished') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.education.finished.update',['finished' => $one->id]) : route('dashboard.staff.education.finished.update', ['user' => $user->username,'finished' => $one->id]) }}" method="POST" class="was-validated">
                    @csrf
                    @method("PUT")
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <x-html.input type="text" :value="$one->year" :disabled="false" name="year" :required="true" label="{{ __('dashboard.education_field.year') }}"/>
                            </div>
                            <div class="col-6">
                                <x-html.input type="text" :value="$one->place" :disabled="false" name="place" :required="true" label="{{ __('dashboard.education_field.finished') }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('dashboard.action.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="delete_finished_education_{{$one->id}}" action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.education.finished.destroy',['finished' => $one->id]) : route('dashboard.staff.education.finished.destroy', ['user' => $user->username,'finished' => $one->id]) }}" method="POST" class="d-none">
        @csrf
        @method('DELETE')
    </form>
@empty
@endforelse


