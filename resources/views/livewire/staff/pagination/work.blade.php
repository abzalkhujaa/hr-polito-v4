
<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            <h1 class="text-center">{{ __('dashboard.work_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        @if($user->works->count() > 0)
                            <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                        @else
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        @endif
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>
            <ul class="timeline">
                @forelse($user->works()->orderBy('created_at')->get() as $one)
                    <li class="timeline-item">
                        <span class="timeline-point timeline-point-indicator"></span>
                        <div class="timeline-event">
                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                <h6>{{ $one->start }} - {{ $one->finish }} йй</h6>
                                @if(!$disabled)
                                    <span class="timeline-event-time">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_work_{{$one->id}}">
                                            <i class="feather icon-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete_work_{{$one->id}}').submit()">
                                            <i class="feather icon-trash"></i>
                                        </button>
                                    </span>
                                @endif
                            </div>
                            <div>{{ $one->place }}</div>
                        </div>
                    </li>
                @empty
                    <h3 class="text-center">
                        {{ __('dashboard.no_information') }}
                    </h3>
                @endforelse
            </ul>

            @if(!$disabled)
                <hr>
                <div class="row mt-2">
                    <div class="col-md-3 col-sm-6 col-12">
                        <x-html.input type="text"  name="start" :required="true" label="{{ __('dashboard.work_field.start') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <x-html.input type="text"  name="finish" :required="true" label="{{ __('dashboard.work_field.finish') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <x-html.input type="text" name="place" :required="true" label="{{ __('dashboard.work_field.place') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        </button>
                    </div>
                </div>
            @endif
        </form>
    </div>
</div>

@forelse($user->works as $one)
    <div class="modal fade text-left"
         id="edit_work_{{$one->id}}"
         tabindex="-1"
         role="dialog"
         aria-labelledby="edit_finished_education_label_{{$one->id}}"
         aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="edit_finished_education_label_{{$one->id}}">{{ __('dashboard.work_field.title') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.work.update',['work' => $one->id]) : route('dashboard.staff.work.update', ['user' => $user->username,'work' => $one->id]) }}" method="POST" class="was-validated">
                    @csrf
                    @method("PUT")
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->start"  name="start" :required="true" label="{{ __('dashboard.work_field.start') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                                <x-html.input type="text" :value="$one->finish"  name="finish" :required="true" label="{{ __('dashboard.work_field.finish') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-12 col-sm-6 col-12">
                                <x-html.input type="text" :value="$one->place" name="place" :required="true" label="{{ __('dashboard.work_field.place') }}" :disabled="!$disabled"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('dashboard.action.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="delete_work_{{$one->id}}" action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.work.destroy',['work' => $one->id]) : route('dashboard.staff.work.destroy', ['user' => $user->username,'work' => $one->id]) }}" method="POST" class="d-none">
        @csrf
        @method('DELETE')
    </form>
@empty
@endforelse


