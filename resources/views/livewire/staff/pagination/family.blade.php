<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            <h1 class="text-center">{{ __('dashboard.family_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        @if($user->works->count() > 0)
                            <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                        @else
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        @endif
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>
            <table class="table table-hover table-striped">
                <thead>
                <tr class="text-center">
                    <th>{{ __('dashboard.family_field.family_member') }}</th>
                    <th>{{ __('dashboard.family_field.full_name') }}</th>
                    <th>{{ __('dashboard.family_field.b_day') }}<br>{{ __('dashboard.family_field.city') }}</th>
                    <th>{{ __('dashboard.family_field.work') }}</th>
                    <th>{{ __('dashboard.family_field.current_place') }}</th>
                    @if(!$disabled)
                    <th>{{ __('dashboard.action.title') }}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($user->family()->orderBy('created_at')->get() as $one)
                    <tr>
                        <td class="text-center">{{ $one->family_member }}</td>
                        <td class="text-center">{{ $one->last_name }}<br>{{ $one->first_name }}<br>{{ $one->patronymic }}</td>
                        <td class="text-center">{{ $one->b_day }} год<br>{{ $one->city }}</td>
                        <td class="text-center">{{ $one->work }}</td>
                        <td class="text-center">{{ $one->current_place }}</td>
                        @if(!$disabled)
                        <td class="text-center">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_family_{{$one->id}}">
                                <i class="feather icon-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="event.preventDefault();document.getElementById('delete_family_{{$one->id}}').submit()">
                                <i class="feather icon-trash"></i>
                            </button>
                        </td>
                        @endif
                    </tr>
                @empty
                    <h3 class="text-center">
                        {{ __('dashboard.no_information') }}
                    </h3>
                @endforelse
                </tbody>
            </table>

            @if(!$disabled)
                <div class="row mt-2">
                    <div class="col-md-12 col-sm-12 col-12">
                        <x-html.input type="text"  name="family_member" :required="true" label="{{ __('dashboard.family_field.family_member') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <x-html.input type="text"  name="first_name" :required="true" label="{{ __('dashboard.family_field.first_name') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <x-html.input type="text" name="last_name" :required="true" label="{{ __('dashboard.family_field.last_name') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                        <x-html.input type="text" name="patronymic" :required="false" label="{{ __('dashboard.family_field.patronymic') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <x-html.input type="text" name="b_day" :required="true" label="{{ __('dashboard.family_field.b_day') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <x-html.input type="text" name="city" :required="true" label="{{ __('dashboard.family_field.city') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <x-html.input type="text" name="work" :required="true" label="{{ __('dashboard.family_field.work') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <x-html.input type="text" name="current_place" :required="true" label="{{ __('dashboard.family_field.current_place') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        </button>
                    </div>
                </div>
            @endif
        </form>
    </div>
</div>

@forelse($user->family as $one)
    <div class="modal fade text-left"
         id="edit_family_{{$one->id}}"
         tabindex="-1"
         role="dialog"
         aria-labelledby="edit_family_label_{{$one->id}}"
         aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="edit_family_label_{{$one->id}}">{{ __('dashboard.work_field.title') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.family.update',['family' => $one->id]) : route('dashboard.staff.family.update', ['user' => $user->username,'family' => $one->id]) }}" method="POST" class="was-validated">
                    @csrf
                    @method("PUT")
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->family_member"  name="family_member" :required="true" label="{{ __('dashboard.family_field.family_member') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <x-html.input type="text" :value="$one->first_name"  name="first_name" :required="true" label="{{ __('dashboard.family_field.first_name') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <x-html.input type="text" :value="$one->last_name" name="last_name" :required="true" label="{{ __('dashboard.family_field.last_name') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-4 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->patronymic" name="patronymic" :required="false" label="{{ __('dashboard.family_field.patronymic') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->b_day" name="b_day" :required="true" label="{{ __('dashboard.family_field.b_day') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->city" name="city" :required="true" label="{{ __('dashboard.family_field.city') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->work" name="work" :required="true" label="{{ __('dashboard.family_field.work') }}" :disabled="!$disabled"/>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <x-html.input type="text" :value="$one->current_place" name="current_place" :required="true" label="{{ __('dashboard.family_field.current_place') }}" :disabled="!$disabled"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('dashboard.action.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="delete_family_{{$one->id}}" action="{{ (auth()->check() && auth()->user()->hasRole(\App\Helpers\Enums\UserRoleEnum::STAFF)) ? route('staff-dashboard.staff.family.destroy',['family' => $one->id]) : route('dashboard.staff.family.destroy', ['user' => $user->username,'family' => $one->id]) }}" method="POST" class="d-none">
        @csrf
        @method('DELETE')
    </form>
@empty
@endforelse


