<form action="{{ $action ?? '' }}" class="row match-height" method="POST" enctype="multipart/form-data">
    @csrf
    @if($type == \App\Helpers\Enums\ActionTypeEnum::UPDATE)
        @method('PUT')
    @endif
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-content">
                    <h1 class="text-center">{{ __('dashboard.file_field.title') }}</h1>
                    <hr>
                    <div class="d-flex justify-content-end">
                        <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                            @if($disabled)
                                <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                            @else
                                <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                            @endif
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--  IMAGE  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->image)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.image') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>
                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG</strong>
                </p>
                @if($user->file and $user->file->image)
                    <a href="{{ asset('storage/'.$user->file->image) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="image" aria-describedby="image">
                            <label class="custom-file-label text-center" for="image"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('image')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror
            </div>
        </div>
    </div>
    {{--  DIPLOMA  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->diploma)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.diploma') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>

                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG, PDF</strong>
                </p>
                @if($user->file and $user->file->diploma)
                    <a href="{{ asset('storage/'.$user->file->diploma) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="diploma" class="custom-file-input" id="diploma" aria-describedby="diploma">
                            <label class="custom-file-label text-center" for="diploma"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('diploma')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror
            </div>
        </div>
    </div>
    {{--  PASSPORT  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->passport)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.passport') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>

                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG, PDF</strong>
                </p>
                @if($user->file and $user->file->passport)
                    <a href="{{ asset('storage/'.$user->file->passport) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="passport" class="custom-file-input" id="passport" aria-describedby="passport">
                            <label class="custom-file-label text-center" for="passport"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('passport')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror

            </div>
        </div>
    </div>
    {{--  INN  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->inn)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.inn') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>

                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG</strong>
                </p>
                @if($user->file and $user->file->inn)
                    <a href="{{ asset('storage/'.$user->file->inn) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="inn" class="custom-file-input" id="inn" aria-describedby="inn">
                            <label class="custom-file-label text-center" for="inn"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('inn')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror
            </div>
        </div>
    </div>
    {{--  INPS  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->inps)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.inps') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>
                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG</strong>
                </p>
                @if($user->file and $user->file->inps)
                    <a href="{{ asset('storage/'.$user->file->inps) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="inps" class="custom-file-input" id="inps" aria-describedby="inps">
                            <label class="custom-file-label text-center" for="inps"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('inps')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror
            </div>
        </div>
    </div>
    {{--  MILITARY  --}}
    <div class="col-md-4 col-sm-6-col-12">
        <div class="card card-apply-job">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-1">
                    <div class="media">
                        <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                            @if($user->file and $user->file->military)
                                <i class="feather icon-check-circle h1 text-success"></i>
                            @else
                                <i class="feather icon-x-circle h1 text-danger"></i>
                            @endif
                        </div>
                        <div class="media-body">
                            <h5 class="mb-0">{{ __('dashboard.file_field.military') }}</h5>
                            <small class="text-muted">image</small>
                        </div>
                    </div>
                </div>
                <p class="card-text mb-2">
                    Рекомендуемые размеры : <strong>3 Mb</strong>
                    Рекомендуемые файлы : <strong>JPG, PNG</strong>
                </p>
                @if($user->file and $user->file->military)
                    <a href="{{ asset('storage/'.$user->file->military) }}" download class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-download"></i> {{ __('dashboard.action.download') }}
                        </div>
                    </a>
                @else
                    <div class="apply-job-package bg-light-primary rounded d-flex justify-content-center">
                        <div>
                            <i class="feather icon-x-circle"></i> {{ __('message.file.not_uploaded') }}
                        </div>
                    </div>
                @endif
                <div class="mb-1">
                    <small>
                        <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                    </small>
                </div>
                @if(!$disabled)
                    <div class="input-group mb-1">
                        <div class="custom-file">
                            <input type="file" name="military" class="custom-file-input" id="military" aria-describedby="military">
                            <label class="custom-file-label text-center" for="military"><i class="feather icon-upload"></i> {{ __('dashboard.action.choose') }}</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-upload"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
                @error('military')
                <small class="text-danger my-1">
                    <i class="feather icon-x-circle"></i> {{ $message }}
                </small>
                @enderror
            </div>
        </div>
    </div>
</form>

