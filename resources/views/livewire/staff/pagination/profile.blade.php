<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            @if($type === \App\Helpers\Enums\ActionTypeEnum::UPDATE)
                @method('PUT')
            @endif
            <h1 class="text-center">{{ __('dashboard.profile_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->username" name="username" :required="true" label="{{ __('dashboard.username') }}" pattern="pattern=^[A-Za-z0-9_]{1,15}$" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="email" :value="$user->email" name="email" :required="true" label="{{ __('dashboard.email') }}" :disabled="$disabled"/>
                </div>
                @if($disabled)
                    <div class="col-md-4 col-sm-6 col-sm-12">
                        <x-html.input type="password" value="qwerty123$" name="password" :required="false" label="{{ __('dashboard.password') }}" :disabled="$disabled"/>
                    </div>
                @else
                    <div class="col-md-4 col-sm-6 col-sm-12">
                        <div class="row">
                            @role(\App\Helpers\Enums\UserRoleEnum::STAFF)
                                <div class="col-12">
                                    <button type="button" class="btn btn-warning btn-block mb-1" data-toggle="modal" data-target="#new_password_modal">
                                        {{ __('dashboard.new_password') }}
                                    </button>
                                </div>
                            @else
                                <div class="col-6">
                                    <button type="button" class="btn btn-warning btn-block mb-1" data-toggle="modal" data-target="#new_password_modal">
                                        {{ __('dashboard.new_password') }}
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" onclick="event.preventDefault();document.getElementById('reset-password-form').submit()" class="btn btn-success btn-block mb-1">{{ __('dashboard.reset_password') }}</button>
                                </div>
                            @endrole
                        </div>
                    </div>
                @endif
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->first_name" name="first_name" :required="true" label="{{ __('dashboard.profile_field.fname') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->last_name" name="last_name" :required="true" label="{{ __('dashboard.profile_field.lname') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->patronymic" name="patronymic" :required="true" label="{{ __('dashboard.profile_field.pname') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-12 col-12">
                    <label for="gender">{{ __('dashboard.profile_field.gender') }}</label>
                    <select name="gender" id="gender" class="custom-select" @if($disabled) disabled="{{ $disabled }}" @endif>
                        <option value="M" @if($user->gender === 'M') selected @endif>{{ __('dashboard.male') }}</option>
                        <option value="F" @if($user->gender === 'F') selected @endif>{{ __('dashboard.female') }}</option>
                        <option value="N" @if($user->gender === 'N') selected @endif>{{ __('dashboard.no_gender') }}</option>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6 col-12"></div>
                @if(!$disabled)
                    <div class="col-12"><hr></div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-check-circle"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>

<div class="modal fade text-left"
     id="new_password_modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel33"
     aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">{{ __('dashboard.new_password') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('dashboard.staff.profile.password.new',['user' => $user->username]) }}" method="POST" class="was-validated">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <x-html.input type="password" :disabled="false" name="old_password" :required="true" label="{{ __('dashboard.old_password') }}"/>
                        </div>
                        <div class="col-12">
                            <x-html.input type="password" :disabled="false" name="password" :required="true" label="{{ __('dashboard.new_password') }}"/>
                        </div>
                        <div class="col-12">
                            <x-html.input type="password" :disabled="false" name="password_confirmation" :required="true" label="{{ __('dashboard.confirm_password') }}"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('dashboard.action.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<form class="d-none" id="reset-password-form" action="{{ route('dashboard.staff.profile.password.reset',['user' => $user->username]) }}" method="POST">
    @csrf
    @method('PUT')
</form>
