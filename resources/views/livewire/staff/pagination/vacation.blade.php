<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            <h1 class="text-center">{{ __('dashboard.vacation_field.title') }}</h1>
            <hr>
            @unlessrole(\App\Helpers\Enums\UserRoleEnum::STAFF)
                <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        @if($user->works->count() > 0)
                            <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                        @else
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        @endif
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
                <hr>
            @endrole
            <table class="table table-hover table-striped">
                <thead>
                <tr class="text-center">
                    <th>{{ __('dashboard.vacation_field.type') }}</th>
                    <th>{{ __('dashboard.vacation_field.period') }}</th>
                    <th>{{ __('dashboard.vacation_field.start') }}<br>{{ __('dashboard.family_field.city') }}</th>
                    <th>{{ __('dashboard.vacation_field.days') }}</th>
                    <th>{{ __('dashboard.vacation_field.finish') }}</th>
                    @if(!$disabled)
                        <th>{{ __('dashboard.action.title') }}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($user->vacation()->orderBy('created_at')->get() as $one)
                    <tr>
                        <td class="text-center">{{ $one->type }}</td>
                        <td class="text-center">{{ $one->period }}</td>
                        <td class="text-center">{{ $one->start->format('d.m.Y') }}</td>
                        <td class="text-center">{{ $one->duration }}</td>
                        <td class="text-center">{{ $one->finish->format('d.m.Y') }}</td>
                        @if(!$disabled)
                            <td class="text-center">
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_vacation_{{$one->id}}">
                                    <i class="feather icon-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="event.preventDefault();document.getElementById('delete_vacation_{{$one->id}}').submit()">
                                    <i class="feather icon-trash"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                @empty
                    <h3 class="text-center">
                        {{ __('dashboard.no_information') }}
                    </h3>
                @endforelse
                </tbody>
            </table>

            @if(!$disabled)
                <hr>
                <div class="row mt-2">
                    <div class="col-md-12 col-sm-12 col-12 mb-1">
                        <div class="from-group">
                            <label for="type">{{ __('dashboard.vacation_field.type') }}</label>
                            <select name="type" id="type" class="custom-select" required>
                                <option value="">{{ __('dashboard.vacation_field.choose_type') }}...</option>
                                @foreach(\App\Helpers\Enums\VacationTypeEnum::toArray() as $one)
                                    <option value="{{ $one }}">{{$one}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-12">
                        <x-html.input type="text"  name="period"  :required="true" label="{{ __('dashboard.vacation_field.period') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <x-html.input type="date" name="start" wire:model="start" :required="true" label="{{ __('dashboard.vacation_field.start') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <x-html.input type="number" wire:model="duration" :pattern="'min=0'" name="duration" :required="true" label="{{ __('dashboard.vacation_field.days') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <x-html.input type="date" name="finish" wire:model="finish" :required="true" label="{{ __('dashboard.vacation_field.finish') }}" :disabled="$disabled"/>
                    </div>

                    <div class="col-md-12 col-sm-12 col-12">
                        <x-html.input type="text" name="additional" :required="false" label="{{ __('dashboard.vacation_field.additional') }}" :disabled="$disabled"/>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        </button>
                    </div>
                </div>
            @endif
        </form>
    </div>
</div>

@forelse($user->vacation as $one)
    <div class="modal fade text-left"
         id="edit_vacation_{{$one->id}}"
         tabindex="-1"
         role="dialog"
         aria-labelledby="edit_vacation_label_{{$one->id}}"
         aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="edit_vacation_label_{{$one->id}}">{{ __('dashboard.vacation_field.title') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('dashboard.staff.vacation.update',['user' => $user->username,'vacation' => $one->id]) }}" method="POST" class="was-validated">
                    @csrf
                    @method("PUT")
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-md-12 col-sm-12 col-12 mb-1">
                                <div class="from-group">
                                    <label for="type">{{ __('dashboard.vacation_field.type') }}</label>
                                    <select name="type" id="type" class="custom-select" required>
                                        <option value="">{{ __('dashboard.vacation_field.choose_type') }}...</option>
                                        @foreach(\App\Helpers\Enums\VacationTypeEnum::toArray() as $item)
                                            <option value="{{ $item }}" @if($item == $one->type ) selected @endif>{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">
                                <x-html.input type="text"  name="period" :value="$one->period" :required="true" label="{{ __('dashboard.vacation_field.period') }}" :disabled="false"/>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <x-html.input type="date" name="start" :value="$one->start->toDateString()" :required="true" label="{{ __('dashboard.vacation_field.start') }}" :disabled="false"/>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <x-html.input type="number" :value="$one->duration" :pattern="'min=0'" name="duration" :required="true" label="{{ __('dashboard.vacation_field.days') }}" :disabled="false"/>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <x-html.input type="date" name="finish" :value="$one->finish->toDateString()" :required="true" label="{{ __('dashboard.vacation_field.finish') }}" :disabled="false"/>
                            </div>

                            <div class="col-md-12 col-sm-12 col-12">
                                <x-html.input type="text" name="additional" :value="$one->additional" :required="false" label="{{ __('dashboard.vacation_field.additional') }}" :disabled="false"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('dashboard.action.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="delete_vacation_{{$one->id}}" action="{{ route('dashboard.staff.vacation.destroy',['user' => $user->username,'vacation' => $one->id]) }}" method="POST" class="d-none">
        @csrf
        @method('DELETE')
    </form>
@empty
@endforelse


