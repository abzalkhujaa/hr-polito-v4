<div class="card">
    <div class="card-body">
        <form class="card-content was-validated" method="POST" action="{{ $action ?? '' }}">
            @csrf
            @if($type === \App\Helpers\Enums\ActionTypeEnum::UPDATE)
                @method('PUT')
            @endif
            <h1 class="text-center">{{ __('dashboard.profile_field.title') }}</h1>
            <hr>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn {{ ($disabled) ? 'btn-primary' : 'btn-danger' }}" wire:click="editToggle">
                    @if($disabled)
                        <i class="feather icon-edit"></i> {{ __('dashboard.action.edit') }}
                    @else
                        <i class="feather icon-x-circle"></i> {{ __('dashboard.action.cancel') }}
                    @endif
                </button>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="date" :value="$user->information->b_day ?? ''" name="b_day" :required="true" label="{{ __('dashboard.info_field.bday') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->city ?? ''" name="city" :required="true" label="{{ __('dashboard.info_field.city') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->nationality ?? ''" name="nationality" :required="true" label="{{ __('dashboard.info_field.nationality') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->region ?? ''" name="region" :required="true" label="{{ __('dashboard.info_field.region') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->district ?? ''" name="district" :required="true" label="{{ __('dashboard.info_field.district') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->address ?? ''" name="address" :required="true" label="{{ __('dashboard.info_field.address') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->phone ?? ''" name="phone" :required="true" label="{{ __('dashboard.info_field.phone') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->home_phone ?? ''" name="home_phone" :required="true" label="{{ __('dashboard.info_field.homephone') }}" :disabled="$disabled"/>
                </div>
                <div class="col-md-4 col-sm-6 col-sm-12">
                    <x-html.input type="text" :value="$user->information->passport_number ?? ''" name="passport_number" :required="true" label="{{ __('dashboard.info_field.passport_number') }}" :disabled="$disabled"/>
                </div>
                @if(!$disabled)
                    <div class="col-12"><hr></div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="feather icon-check-circle"></i> {{ __('dashboard.action.save') }}
                        </button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>

