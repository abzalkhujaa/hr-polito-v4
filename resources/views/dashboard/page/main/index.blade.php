@extends('dashboard.layouts.dashboard')

@section('content')
    <section>
        <div class="row match-height">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card card-congratulation-medal">
                    <div class="card-body">
                        <h5>Добро пожаловать 🎉 {{ auth()->user()->username }}!</h5>
                        <p class="card-text font-small-3">Вы вошли как Администратор</p>
                        <h3 class="mb-75 mt-2 pt-50">
                            <a href="javascript:void(0);" class="text-white" style="color: rgba(0,0,0,0);!important;">
                                <hr style="border-color: rgba(0,0,0,0);">
                            </a>
                        </h3>
                        <a href="{{ route('dashboard.staff.profile.index',['user'=>auth()->user()->username]) }}" class="btn btn-primary waves-effect waves-float waves-light">Профиль</a>
                        <img src="{{ asset('images/illustration/badge.svg') }}" class="congratulation-medal" alt="Medal Pic">
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6 col-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <h4 class="card-title">Статистика</h4>
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="media">
                                    <div class="avatar bg-light-primary mr-2">
                                        <div class="avatar-content">
                                            <i class="fal fa-users fa-2x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$statistics['users']}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{ __('page.dashboard.staff.title') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="media">
                                    <div class="avatar bg-light-info mr-2">
                                        <div class="avatar-content">
                                            <i class="fal fa-newspaper fa-2x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{ $statistics['news'] }}</h4>
                                        <p class="card-text font-small-3 mb-0">{{ __('page.dashboard.blog.news.index') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                <div class="media">
                                    <div class="avatar bg-light-danger mr-2">
                                        <div class="avatar-content">
                                            <i class="fal fa-tag fa-2x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{ $statistics['vacancy'] }}</h4>
                                        <p class="card-text font-small-3 mb-0">{{ __('page.dashboard.blog.vacancy.index') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12">
                                <div class="media">
                                    <div class="avatar bg-light-success mr-2">
                                        <div class="avatar-content">
                                            <i class="fal fa-file-alt fa-2x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{ $statistics['decree'] }}</h4>
                                        <p class="card-text font-small-3 mb-0">{{ __('page.dashboard.blog.decree.index') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row match-height">
            <div class="col-lg-8 col-12">
                <div class="card card-company-table">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('page.dashboard.staff.index') }}</th>
                                        <th>{{ __('dashboard.role') }}</th>
                                        <th>Последнее действие</th>
                                        <th class="text-center">{{ __('dashboard.action.title') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <span class="avatar">
                                                    @if($user->file && $user->file->image)
                                                        <img class="round" src="{{asset('storage/'.$user->file->image)}}" alt="avatar" height="40" width="40" style="object-fit: cover;object-position: top;">
                                                    @else
                                                        <span class="avatar-content shadow" style="width: 40px;height: 40px">{{ $user->short_name }}</span>
                                                    @endif
                                                </span>
                                                <div class="ml-1">
                                                    <div class="font-weight-bolder">{{ $user->full_name ?? $user->username }}</div>
                                                    @if($user->email_verified_at)
                                                        <div class="font-small-2 text-muted" title="Электронная почта подтверждена" data-toggle="tooltip">
                                                            {{ $user->email }}
                                                            <i class="fas fa-circle text-success"></i>
                                                        </div>
                                                    @else
                                                        <div class="font-small-2 text-muted" title="Электронная почта не подтверждена" data-toggle="tooltip">
                                                            {{ $user->email }}
                                                            <i class="fas fa-circle text-danger"></i>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="avatar bg-light-primary mr-1">
                                                    <div class="avatar-content">
                                                        {!! $user->role_icon() !!}
                                                    </div>
                                                </div>
                                                <span>{{ __('role.'.$user->roles->first()->name) ?? 'Гость' }}</span>
                                            </div>
                                        </td>
                                        <td class="text-nowrap">
                                            <div class="d-flex flex-column">
                                                <span class="font-weight-bolder mb-25">
                                                    <i class="feather icon-clock text-primary"></i> {{ $user->updated_at->format('d.m.Y H:i') }}
                                                </span>
                                                <span class="font-small-2 text-muted">
                                                    {{ $user->created_at->format('d.m.Y H:i') }}
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('dashboard.staff.profile.index',['user' => $user->username]) }}">
                                                <i class="feather icon-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card card-congratulations">
                    <div class="card-body text-center">
                        <img src="{{ asset('images/elements/decore-left.png') }}" class="congratulations-img-left" alt="card-img-left">
                        <img src="{{ asset('images/elements/decore-right.png') }}" class="congratulations-img-right" alt="card-img-right">
                        <div class="avatar avatar-xl bg-primary shadow">
                            <div class="avatar-content">
                                <i class="feather icon-award"></i>
                            </div>
                        </div>
                        <div class="text-center">
                            <h1 class="mb-1 text-white">День рождения<br><small>сотрудников</small></h1>
                            @forelse($users as $user)
                                @if($user->birthday_left() > 0 and $user->birthday_left() <= 240)
                                    <a href="{{ route('dashboard.staff.profile.index',['user' => $user->username]) }}" class="card text-left">
                                        <div class="card-header">
                                            <div>
                                                <h2 class="font-weight-bolder mb-0">
                                                    @if($user->birthday_left() < 25)
                                                        Осталось {{ $user->birthday_left() }} часов
                                                    @else
                                                        Осталось {{ round($user->birthday_left() / 24) }} дней
                                                    @endif
                                                </h2>
                                                <h5 class="card-text">{{ $user->full_name ?? $user->username }}</h5>
                                            </div>
                                            <div class="avatar bg-light-success p-50 m-0">
                                                <div class="avatar-content">
                                                    <i class="feather icon-gift"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
