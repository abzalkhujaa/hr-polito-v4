@extends('dashboard.layouts.dashboard')

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
                <form action="{{ route('dashboard.vacancy.update',['vacancy' => $vacancy->alias]) }}" method="POSt">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12">
                            <x-html.input type="text" name="title" :value="$vacancy->title" :label="__('dashboard.news_field.title')" :required="true" :disabled="false"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="position">{{ __('dashboard.position') }}</label>
                        <select name="position_id" id="position" required class="select2">
                            <option value="">{{ __('dashboard.labor_field.position') }}</option>
                            @foreach($positions as $one)
                                <option value="{{ $one->id }}" @if($one->id === $vacancy->position_id) selected @endif>{{ $one->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="responsibility">Должностные обязанности:</label>
                        <textarea id="responsibility" name="responsibility" class="rounded shadow-sm" style="min-height: 200px">{!! $vacancy->responsibility !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="requirement">Требования:</label>
                        <textarea id="requirement" name="requirement" class="rounded shadow-sm" style="min-height: 200px">{!! $vacancy->requirement !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="content">Мы предлагаем:</label>
                        <textarea id="content" name="content" class="rounded shadow-sm" style="min-height: 200px">{!! $vacancy->content !!}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block waves-effect waves-float waves-light">
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.edit') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/scripts/forms/form-select2.js') }}"></script>
    <!-- Page js files -->
    <script>
        function ckeditorInit(editor_id){
            ClassicEditor
                .create( document.querySelector( editor_id ), {
                    toolbar: {
                        items: [
                            'heading',
                            'fontFamily',
                            'fontSize',
                            'fontColor',
                            'fontBackgroundColor',
                            '|',
                            'bold',
                            'italic',
                            'underline',
                            'link',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'alignment',
                            'indent',
                            'outdent',
                            '|',
                            'imageUpload',
                            'imageInsert',
                            'htmlEmbed',
                            'blockQuote',
                            'insertTable',
                            'mediaEmbed',
                            '|',
                            'pageBreak',
                            'horizontalLine',
                            'MathType',
                            'ChemType',
                            'restrictedEditingException',
                            'specialCharacters',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    language: 'ru',
                    image: {
                        styles: [
                            'alignLeft', 'alignCenter', 'alignRight'
                        ],
                        resizeOptions: [
                            {
                                name: 'imageResize:original',
                                label: 'Original',
                                value: null
                            },
                            {
                                name: 'imageResize:50',
                                label: '50%',
                                value: '50'
                            },
                            {
                                name: 'imageResize:75',
                                label: '75%',
                                value: '75'
                            }
                        ],
                        toolbar: [
                            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                            '|',
                            'imageResize',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: [
                            'tableColumn',
                            'tableRow',
                            'mergeTableCells',
                            'tableCellProperties',
                            'tableProperties'
                        ]
                    },
                    licenseKey: '',
                } )
                .then( editor => {
                    window.editor = editor;
                } )
                .catch( error => {
                    console.error( 'Visual Editor not working! Check CKEditor' );
                    console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                    console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                    console.error( error );
                } );
        }
        ckeditorInit('#responsibility')
        ckeditorInit('#requirement')
        ckeditorInit('#content')
    </script>

@endsection

@push('livewire-script')
    <script>
        Livewire.on('reRenderCKEditor', () => {
            ckeditorInit('#editor')
        })
        Livewire.on('fileSaved', () => {
            Swal.fire({
                icon: 'success',
                title: '{{ __('message.file.uploaded') }}',
                customClass: {
                    confirmButton: 'btn btn-success'
                }
            });
        })
    </script>
@endpush
