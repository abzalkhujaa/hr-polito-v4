@extends('dashboard.layouts.dashboard')

@section('title', __('page.dashboard.group.title'))

@section('content')
    <div class="row">
        <div class="col-md-4 col-12">
            <a href="{{ route('dashboard.group.section.index') }}" class="card">
                <div class="card-body text-center">
                        <div>
                            <i class="fal fa-users text-primary" style="font-size: 72px;"></i>
                        </div>
                        <div class="h1 text-primary">
                            {{ __('page.dashboard.group.section.title') }}
                        </div>
                    </div>
                </a>
            </div>
        <div class="col-md-4 col-12">
            <a href="{{ route('dashboard.group.department.index') }}" class="card">
                <div class="card-body text-center">
                        <div>
                            <i class="fal fa-user-friends text-primary" style="font-size: 72px;"></i>
                        </div>
                        <div class="h1 text-primary">
                            {{ __('page.dashboard.group.department.title') }}
                        </div>
                    </div>
                </a>
            </div>
        <div class="col-md-4 col-12">
            <a href="{{ route('dashboard.group.position.index') }}" class="card">
                <div class="card-body text-center">
                        <div>
                            <i class="fal fa-user text-primary" style="font-size: 72px;"></i>
                        </div>
                        <div class="h1 text-primary">
                            {{ __('page.dashboard.group.position.title') }}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
