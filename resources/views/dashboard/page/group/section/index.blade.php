@extends('dashboard.layouts.dashboard')

@section('title',__('page.dashboard.group.section.title'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection


@section('content')
    <section id="data-list-view" class="data-list-view-header">
        <div class="card">
            <div class="card-body">
                <div class="card-content">
                    <div class="row" id="table-hover-animation">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#js__create"><i class="feather icon-plus"></i> {{ __('page.dashboard.group.section.create') }}</button>
                        </div>
                        <div class="col-12">
                            <div class="card-datatable">
                                <table class="dt-responsive table">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>№</th>
                                        <th>{{ __('page.dashboard.group.section.input.title') }}</th>
                                        <th>__</th>
                                        <th>{{ __('page.dashboard.group.section.input.created_at') }}</th>
                                        <th>{{ __('dashboard.action.title') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($sections as $section)
                                        <tr>
                                            <td></td>
                                            <td>{{ $section->id }}</td>
                                            <td>{{ $section->title }}</td>
                                            <td>{{ count($section->department) }}</td>
                                            <td><i class="feather icon-clock text-primary"></i> {{ $section->created_at->format('H:i d.m.Y') }}</td>
                                            <td>
                                                <button type="button"
                                                        class="btn btn-link m-0 p-0 text-primary js__edit"
                                                        data-toggle="modal"
                                                        data-target="#js__edit_{{$section->id}}"
                                                ><i class="feather icon-edit"></i></button>
                                                <button type="button"
                                                        class="btn btn-link m-0 p-1 text-danger"
                                                        onclick="js_delete('{{route('dashboard.group.section.destroy',['section' => $section->id])}}')"
                                                ><i class="feather icon-trash"></i></button>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal modal-slide-in fade" id="js__create">
        <div class="modal-dialog sidebar-sm">
            <form method="POST" action="{{ route('dashboard.group.section.store') }}" class="was-validated add-new-record modal-content pt-0">
                @csrf
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="modal-header mb-1">
                    <h5 class="modal-title" id="create_modal-label">{{ __('page.dashboard.group.section.create') }}</h5>
                </div>
                <div class="modal-body flex-grow-1">
                    <x-html.input :required="true" name="title" type="text" :label="__('page.dashboard.group.section.input.title')" :disabled="false"/>
                    <button type="submit" class="btn btn-primary data-submit mr-1">{{ __('dashboard.action.add') }}</button>
                    <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
    @foreach($sections as $section)
        <div class="modal modal-slide-in fade" id="js__edit_{{$section->id}}">
        <div class="modal-dialog sidebar-sm">
            <form class="was-validated add-new-record modal-content pt-0" action="{{ route('dashboard.group.section.update',['section' => $section->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="modal-header mb-1">
                    <h5 class="modal-title" id="edit_modal-label-{{$section->id}}">{{ __('page.dashboard.group.section.update') }}</h5>
                </div>
                <div class="modal-body flex-grow-1">
                    <x-html.input :required="true" name="title" :value="$section->title" id-name="edit-title-{{$section->id}}" type="text" :label="__('page.dashboard.group.section.input.title')" :disabled="false"/>
                    <button type="submit" class="btn btn-primary data-submit mr-1">{{ __('dashboard.action.edit') }}</button>
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('dashboard.action.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
    @endforeach
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script>
        function js_delete(route){
            Swal.fire({
                title: '{{ __('message.are_you_sure') }}',
                text: '{{ __('message.dont_revert') }}',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '{{ __('message.yes_delete') }}',
                cancelButtonText: '{{ __('message.cancel') }}',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    axios.delete(route).then(response => {
                        let data = response.data
                        if (data.status === 'success'){
                            Swal.fire({
                                title: data.message.title,
                                text: data.message.text,
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok',
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                                buttonsStyling: false
                            }).then(function (result){
                                if (result.value){
                                    window.location.reload()
                                }else{

                                }
                            });
                        }
                    }).catch(response => {
                        Swal.fire({
                            icon: 'error',
                            title: '{{ __('message.deleted') }}',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    })

                }
            });
        }

        $(document).ready(function (){
            $('.js__edit').on('click',function (){
                let route = $(this).data('route')
                let title = $(this).data('title')
                $('#edit-title').val(title);
                $('#js__form-edit').attr('action',route)
            });
        });



        let dt_responsive_table = $('.dt-responsive')
            let dt_responsive = dt_responsive_table.DataTable({
                pageLength: 10,
                aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
                columnDefs: [
                    {
                        className: 'control',
                        orderable: false,
                        targets: 0
                    },
                ],
                dom:
                    '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details of ' + data['full_name'];
                            }
                        }),
                        type: 'column',
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                            tableClass: 'table'
                        })
                    }
                },
                language: {
                    "decimal":        "",
                    "emptyTable":     "Нет данных в таблице",
                    "info":           "Показано с _START_ по _END_ из _TOTAL_ записей",
                    "infoEmpty":      "Показано с 0 по 0 из 0 записей",
                    "infoFiltered":   "(отфильтровано из _MAX_ записей)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Показать _MENU_ записей",
                    "loadingRecords": "Загрузка...",
                    "processing":     "Обработка...",
                    "search":         "Поиск:",
                    "zeroRecords":    "Совпадающих записей не найдено",
                    "paginate": {
                        "first":      "Первый",
                        "last":       "Последний",
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

    </script>
@endsection



