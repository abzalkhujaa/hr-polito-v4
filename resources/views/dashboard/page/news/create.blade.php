@extends('dashboard.layouts.dashboard')

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
                <form action="{{ route('dashboard.news.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <x-html.input type="text" name="title" :label="__('dashboard.news_field.title')" :required="true" :disabled="false"/>
                        </div>
                        <div class="col-12">
                            <x-html.input type="text" name="description" :label="__('dashboard.news_field.description')" :required="true" :disabled="false"/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="card card-apply-job">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between align-items-center mb-1">
                                        <div class="media">
                                            <div class="avatar mr-3" style="background-color: rgba(0,0,0,0);">
                                                @if(true)
                                                    <i class="feather icon-check-circle h1 text-success"></i>
                                                @else
                                                    <i class="feather icon-x-circle h1 text-danger"></i>
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mb-0">{{ __('dashboard.file_field.image') }}</h5>
                                                <small class="text-muted">image</small>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="card-text mb-2">
                                        Рекомендуемые размеры : <strong>3 Mb</strong><br>
                                        Рекомендуемые файлы : <strong>JPG, PNG</strong>
                                    </p>
                                    <div class="mb-1">
                                        <small>
                                            <span class="text-danger">*</span> При загрузке нового файла старый файл удаляется
                                        </small>
                                    </div>
                                    <div class="file-zone">
                                        <div class="dropzone rounded mb-2" id="file-image"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 d-none" id="image-preview">
                                Предварительный просмотр:
                                <div class="text-center">
                                    <img src="" id="image-preview-url" class="img-thumbnail w-50" alt="Preview">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editor">{{ __('dashboard.news_field.content') }}</label>
                        <textarea id="editor" name="content" class="rounded shadow-sm" style="min-height: 200px"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="image" id="js__upload_img">
                        <button type="submit" class="btn btn-primary btn-block waves-effect waves-float waves-light">
                            <i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->
    <script>
        function ckeditorInit(editor_id){
            ClassicEditor
                .create( document.querySelector( editor_id ), {
                    toolbar: {
                        items: [
                            'heading',
                            'fontFamily',
                            'fontSize',
                            'fontColor',
                            'fontBackgroundColor',
                            '|',
                            'bold',
                            'italic',
                            'underline',
                            'link',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'alignment',
                            'indent',
                            'outdent',
                            '|',
                            'imageUpload',
                            'imageInsert',
                            'htmlEmbed',
                            'blockQuote',
                            'insertTable',
                            'mediaEmbed',
                            '|',
                            'pageBreak',
                            'horizontalLine',
                            'MathType',
                            'ChemType',
                            'restrictedEditingException',
                            'specialCharacters',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    language: 'ru',
                    image: {
                        styles: [
                            'alignLeft', 'alignCenter', 'alignRight'
                        ],
                        resizeOptions: [
                            {
                                name: 'imageResize:original',
                                label: 'Original',
                                value: null
                            },
                            {
                                name: 'imageResize:50',
                                label: '50%',
                                value: '50'
                            },
                            {
                                name: 'imageResize:75',
                                label: '75%',
                                value: '75'
                            }
                        ],
                        toolbar: [
                            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                            '|',
                            'imageResize',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: [
                            'tableColumn',
                            'tableRow',
                            'mergeTableCells',
                            'tableCellProperties',
                            'tableProperties'
                        ]
                    },
                    licenseKey: '',
                } )
                .then( editor => {
                    window.editor = editor;
                } )
                .catch( error => {
                    console.error( 'Visual Editor not working! Check CKEditor' );
                    console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                    console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                    console.error( error );
                } );
        }
        ckeditorInit('#editor')
    </script>
    <script>
        Dropzone.autoDiscover = false;
        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("file.upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('dashboard.action.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('dashboard.action.dropzone_delete') !!}",
            success: function (file,response){
                document.getElementById('js__upload_img').value = response.file
                if (response.status){
                    $('#image-preview').removeClass('d-none');
                    $('#image-preview-url').attr('src','{{ asset('storage') }}/' + response.file)
                    Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
                }
                else
                    Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
            },
            removedfile: function (){
                $('#image-preview').addClass('d-none');
                let old_file = $('#js__upload_img').val()
                let data = {
                    file : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('file.delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }
        new Dropzone("#file-image",{...dropzone_params,...{
                paramName: "file",
                acceptedFiles: "image/*",
                params:{
                    file: $('#js__upload_img').val(),
                    type: '{{ \App\Helpers\Enums\UploadFileTypeEnum::NEWS }}',
                    folder: 'main'
                }
            }
        });
    </script>
@endsection
