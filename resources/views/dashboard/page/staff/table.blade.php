@extends('dashboard.layouts.dashboard')

@section('title', __('page.dashboard.staff.table_title'))

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
    <style>
        @media (max-width: 576px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }

        @media (min-width: 576px) and (max-width: 768px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }
        .modal-backdrop{
            background-color: #ffffff!important;
        }
        .modal-backdrop.show{
            opacity: 0.8!important;
        }
    </style>
@endsection

@section('content')
    <!-- users list start -->
    <section class="app-user-list">
        <!-- list section start -->
        <div class="card">
            <div class="card-body">
                <div class="card-content">
                    <div class="card-datatable table-responsive pt-0">
                        <table class="user-list-table table">
                            <thead class="thead-light">
                            <tr>
                                <th></th>
                                <th><i class="feather icon-user"></i> {{ __('dashboard.staff') }}</th>
                                <th><i class="feather icon-at-sign"></i> {{ __('dashboard.email') }}</th>
                                <th><i class="feather icon-shield"></i> {{ __('dashboard.position') }}</th>
                                <th><i class="feather icon-slack"></i> {{ __('dashboard.role') }}</th>
                                <th><i class="feather icon-edit"></i> {{ __('dashboard.action.title') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($users as $one)
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="d-flex justify-content-left align-items-center">
                                                <div class="avatar-wrapper">
                                                    <div class="avatar  mr-1" style="width: 32px; height: 32px;">
                                                        @if($one->file && $one->file->image)
                                                            <img src="{{ $one->avatar }}" alt="Avatar" class="w-100" style="object-fit: cover;object-position: top center;">
                                                        @else
                                                            <span class="avatar-content">{{ $one->short_name }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <a href="{{ route('dashboard.staff.profile.index',['user' => $one->username]) }}" class="user_name text-truncate">
                                                        <span class="font-weight-bold">{{ $one->full_name }}</span>
                                                    </a>
                                                    <small class="emp_post text-muted">{{ '@'.$one->username }}</small>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $one->email }}</td>
                                        <td>{{ $one->labor->position->title ?? __('dashboard.no_information') }}</td>
                                        <td>{!! $one->role_icon() !!} {{ $one->roles->first()->name }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-link text-danger" onclick="deleteStaff('{{ route('dashboard.staff.profile.destroy', ['user' => $one->username]) }}')">
                                                <i class="feather icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Modal to add new user starts-->
            <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
                <div class="d-flex align-items-center justify-content-center h-100 w-75">
                    <img src="{{asset('images/pages/login-v2.svg')}}" class="img-fluid" alt="">
                </div>
                <div class="modal-dialog">
                    <form class="add-new-user modal-content pt-0 was-validated" method="POST" action="{{ route('dashboard.staff.store') }}">
                        @csrf
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                        <div class="modal-header mb-1">
                            <h5 class="modal-title" id="user_create">{{ __('dashboard.new_user')}}</h5>
                        </div>
                        <div class="modal-body flex-grow-1">
                            <div class="mb-2">
                                <x-html.input name="username" :label="__('dashboard.username')" type="text" pattern="{{ 'pattern=^[a-z0-9_]{1,30}$' }}" :disabled="false" :required="true" />
                                <small class="form-text text-muted">Вы можете использовать строчные латинские буквы, цифры и подчеркивание. </small>
                            </div>
                            <div class="mb-2">
                                <x-html.input name="email" :label="__('dashboard.email')" type="email" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="password" :label="__('dashboard.password')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="first_name" :label="__('dashboard.profile_field.fname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="last_name" :label="__('dashboard.profile_field.lname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="patronymic" :label="__('dashboard.profile_field.pname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="user-role">Пол пользователя:</label>
                                <select id="user-gender" class="form-control" name="gender" required>
                                    <option value="N">{{ __('dashboard.no_gender') }}</option>
                                    <option value="M">{{ __('dashboard.male') }}</option>
                                    <option value="F">{{ __('dashboard.female') }}</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mr-1 data-submit">{{ __('page.dashboard.staff.create') }}</button>
                            <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('message.cancel') }}</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal to add new user Ends-->
        </div>
        <!-- list section end -->
    </section>
    <!-- users list ends -->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
        let dt_responsive_table = $('.user-list-table')
        let dt_responsive = dt_responsive_table.DataTable({
            pageLength: 10,
            aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0
                },
            ],
            buttons: [
                {
                    text: '<i class="feather icon-database"></i> Excel',
                    className: 'btn btn-success mt-50',
                    attr: {
                        'onclick': 'downloadExcelFile()'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                },
                {
                    text: '<i class="feather icon-user-plus"></i> {{ __('dashboard.action.add') }}',
                    className: 'add-new btn btn-primary mt-50',
                    attr: {
                        'data-toggle': 'modal',
                        'data-target': '#modals-slide-in'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                '>t' +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            language: {
                "decimal":        "",
                "emptyTable":     "Нет данных в таблице",
                "info":           "Показано с _START_ по _END_ из _TOTAL_ записей",
                "infoEmpty":      "Показано с 0 по 0 из 0 записей",
                "infoFiltered":   "(отфильтровано из _MAX_ записей)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Показать _MENU_ записей",
                "loadingRecords": "Загрузка...",
                "processing":     "Обработка...",
                "search":         "Поиск:",
                "zeroRecords":    "Сотрудник не найден",
                "paginate": {
                    "first":      "Первый",
                    "last":       "Последний",
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        function deleteStaff(route){
            Swal.fire({
                title: '{{ __('message.are_you_sure') }}',
                text: '{{ __('message.dont_revert') }}',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '{{ __('message.yes_delete') }}',
                cancelButtonText: '{{ __('message.cancel') }}',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    axios.delete(route).then(response => {
                        let data = response.data
                        if (data.status === 'success'){
                            Swal.fire({
                                title: data.message,
                                text: '',
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok',
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                                buttonsStyling: false
                            }).then(function (result){
                                if (result.value){
                                    window.location.reload()
                                }else{

                                }
                            });
                        }
                    }).catch(response => {
                        Swal.fire({
                            icon: 'error',
                            title: '{{ __('message.deleted') }}',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    })

                }
            });
        }
        function downloadExcelFile(){
            window.location.href = '{{ route('dashboard.staff.excel') }}'
        }
    </script>
@endsection
