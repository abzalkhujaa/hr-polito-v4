@extends('dashboard.layouts.dashboard')

@section('title', __('page.dashboard.blog.decree.index'))

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
    <style>
        @media (max-width: 576px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }

        @media (min-width: 576px) and (max-width: 768px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }
        .modal-backdrop{
            background-color: #ffffff!important;
        }
        .modal-backdrop.show{
            opacity: 0.8!important;
        }
    </style>
@endsection

@section('content')
    <!-- users list start -->
    <section class="app-user-list">
        <!-- list section start -->
        <div class="card">
            <div class="card-body">
                <div class="card-content">
                    <div class="card-datatable table-responsive pt-0">
                        <table class="user-list-table table">
                            <thead class="thead-light">
                            <tr>
                                <th></th>
                                <th>{{ __('dashboard.news_field.title') }}</th>
                                <th>{{ __('dashboard.news_field.creator') }}</th>
                                <th>{{ __('dashboard.news_field.created_at') }}</th>
                                <th>{{ __('dashboard.action.title') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($decree as $one)
                                <tr>
                                    <td></td>
                                    <td>{{ $one->title }}</td>
                                    <td>
                                        <a href="{{ route('dashboard.staff.profile.index',['user' => $one->creator->username]) }}">
                                            <i class="feather icon-user"></i> {{ $one->creator->username }}</a>
                                    </td>
                                    <td><i class="feather icon-calendar"></i> {{ $one->created_at->format('d.m.Y H:i') }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('dashboard.decree.edit',['decree' => $one->alias]) }}"><i class="feather icon-edit"></i></a>
                                        <button type="button" class="btn btn-link text-danger" onclick="deleteDecree('{{ route('dashboard.decree.destroy', ['decree' => $one->alias]) }}')">
                                            <i class="feather icon-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- list section end -->
    </section>
    <!-- users list ends -->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
        let dt_responsive_table = $('.user-list-table')
        let dt_responsive = dt_responsive_table.DataTable({
            pageLength: 10,
            aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0
                },
            ],
            buttons: [
                {
                    text: '<i class="feather icon-plus-circle"></i> {{ __('dashboard.action.add') }}',
                    className: 'btn btn-primary mt-50',
                    attr: {
                        'onclick': 'createDecreeRoute()'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                '>t' +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            language: {
                "decimal":        "",
                "emptyTable":     "Нет данных в таблице",
                "info":           "Показано с _START_ по _END_ из _TOTAL_ записей",
                "infoEmpty":      "Показано с 0 по 0 из 0 записей",
                "infoFiltered":   "(отфильтровано из _MAX_ записей)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Показать _MENU_ записей",
                "loadingRecords": "Загрузка...",
                "processing":     "Обработка...",
                "search":         "Поиск:",
                "zeroRecords":    "Сотрудник не найден",
                "paginate": {
                    "first":      "Первый",
                    "last":       "Последний",
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        function deleteDecree(route){
            Swal.fire({
                title: '{{ __('message.are_you_sure') }}',
                text: '{{ __('message.dont_revert') }}',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '{{ __('message.yes_delete') }}',
                cancelButtonText: '{{ __('message.cancel') }}',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.isConfirmed) {
                    axios.delete(route).then(response => {
                        if (response.data.status){
                            Swal.fire({
                                title: response.data.message,
                                text: '',
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok',
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                                buttonsStyling: false
                            }).then(function (result){
                                window.location.reload()
                            });
                        }
                    }).catch(response => {
                        Swal.fire({
                            icon: 'error',
                            title: '{{ __('message.deleted') }}',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    })
                }
            });
        }
        function createDecreeRoute(){
            window.location.href = '{{ route('dashboard.decree.create') }}'
        }
    </script>
@endsection
