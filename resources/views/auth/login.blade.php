@extends('dashboard.layouts.auth')

@section('title', 'Авторизоваться')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
  <div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
      <!-- Brand logo-->
      <a class="brand-logo d-flex align-items-center" href="javascript:void(0);">
        <div>
          <img src="{{ asset('images/logo_blue-full.webp') }}" height="100" alt="">
        </div>
        <h2 class="brand-text text-primary ml-1"><small>Туринский политехнический университет в городе Ташкенте</small><br>HR платформа</h2>
      </a>
      <!-- /Brand logo-->
      <!-- Left Text-->
      <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
        <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
            <img class="img-fluid" src="{{asset('images/pages/login-v2.svg')}}" alt="Login V2" />
        </div>
      </div>
      <!-- /Left Text-->
      <!-- Login-->
      <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
          <h4 class="card-title mb-1">Добро пожаловать на платформу HR! &#x1F44B;</h4>
          <p class="card-text mb-2">Пожалуйста, войдите в свою учетную запись и начните приключение</p>
          <form class="auth-login-form mt-2" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group">
              <label class="form-label" for="login-username"><i class="feather icon-user"></i> Имя пользователя</label>
              <input class="form-control @error('username') is-invalid @enderror" pattern="^[a-z0-9_]{1,15}$" id="login-username" type="text" name="username" placeholder="username" aria-describedby="login-username" autofocus="" tabindex="1" />
              @error('username')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between">
                <label for="login-password"><i class="feather icon-lock"></i> Пароль</label>
                <a href="{{ route('password.request') }}">
                  <small>Забыли пароль?</small>
                </a>
              </div>
              <div class="input-group input-group-merge form-password-toggle">
                <input class="form-control form-control-merge" id="login-password" type="password" name="password" placeholder="············" aria-describedby="login-password" tabindex="2" />
                <div class="input-group-append">
                  <span class="input-group-text cursor-pointer">
                    <i data-feather="eye"></i>
                  </span>
                </div>
                @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="remember" id="remember-me" type="checkbox" tabindex="3" />
                <label class="custom-control-label" for="remember-me">Запомнить меня</label>
              </div>
            </div>
            <button class="btn btn-primary btn-block" tabindex="4" type="submit">Войти в систему</button>
          </form>
          <p class="text-center mt-2">
            <span>Новичок на нашей платформе?</span>
            <a href="{{ route('register') }}"><span>&nbsp;Создать аккаунт</span></a>
          </p>
          <div class="divider my-2">
            <div class="divider-text">Скоро</div>
          </div>
          <div class="auth-footer-btn d-flex justify-content-center">
            <a class="btn btn-facebook" href="javascript:void(0)">
              <i data-feather="facebook"></i>
            </a>
            <a class="btn btn-twitter white" href="javascript:void(0)">
              <i data-feather="twitter"></i>
            </a>
            <a class="btn btn-google" href="javascript:void(0)">
              <i data-feather="mail"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /Login-->
    </div>
  </div>
@endsection

@section('vendor-script')
  <script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
  <script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
@endsection
