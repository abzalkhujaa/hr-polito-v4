@extends('dashboard.layouts.auth')

@section('title', 'Авторизоваться')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
    <div class="auth-wrapper auth-v2">
        <div class="auth-inner row m-0">
            <!-- Brand logo-->
            <a class="brand-logo d-flex align-items-center" href="javascript:void(0);">
                <div>
                    <img src="{{ asset('images/logo_blue-full.webp') }}" height="100" alt="">
                </div>
                <h2 class="brand-text text-primary ml-1"><small>Туринский политехнический университет в городе Ташкенте</small><br>HR платформа</h2>
            </a>
            <!-- /Brand logo-->
            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                    <img class="img-fluid" src="{{asset('images/pages/login-v2.svg')}}" alt="Login V2" />
                </div>
            </div>
            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                    <h4 class="card-title mb-1 text-center">Добро пожаловать на платформу {{ __('dashboard.hr.title') }}! &#x1F44B;</h4>
                    <div>
                        <div class="row">
                            <div class="col-2">
                                <span class="avatar">
                                    @if(auth()->check() && auth()->user()->file && auth()->user()->file->image)
                                        <img class="round" src="{{asset('storage/'.auth()->user()->file->image)}}" alt="avatar" height="40" width="40" style="object-fit: cover;object-position: top;">
                                    @else
                                        <span class="avatar-content shadow" style="width: 40px;height: 40px">{{ auth()->user()->short_name }}</span>
                                    @endif
                                    <span class="avatar-status-away"></span>
                                </span>
                            </div>
                            <div class="col-10">
                                <small>{{ (auth()->check() && auth()->user()->full_name) ? auth()->user()->full_name : __('dashboard.no_information') }}</small>
                                <br>
                                <strong>{{ (auth()->user()) ? auth()->user()->username : __('dashboard.no_information') }}</strong>
                            </div>
                        </div>

                    </div>
                    <p class="card-text mb-2 text-center mt-3">
                        <i class="fal fa-lock fa-5x"></i>
                    </p>
                    <h3 class="text-center">Вы заблокировали или у вас нет разрешения на использование платформы</h3>
                    <p class="text-right text-danger font-italic">Обратитесь в отдел кадров или к администратору сайта</p>
                    <a href="{{ route('logout') }}" class="btn btn-block btn-primary" onclick="event.preventDefault();document.getElementById('logout-form').submit()">
                        <i class="mr-50 feather icon-power"></i> Войти в другой профиль
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    <div class="divider my-2">
                        <div class="divider-text">Скоро</div>
                    </div>
                    <div class="auth-footer-btn d-flex justify-content-center">
                        <a class="btn btn-facebook" href="javascript:void(0)">
                            <i data-feather="facebook"></i>
                        </a>
                        <a class="btn btn-twitter white" href="javascript:void(0)">
                            <i data-feather="twitter"></i>
                        </a>
                        <a class="btn btn-google" href="javascript:void(0)">
                            <i data-feather="mail"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /Login-->
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
    <script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
@endsection
