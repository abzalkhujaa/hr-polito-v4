@extends('dashboard.layouts.auth')

@section('title', 'Подтвердите адрес электронной почты')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Login v1 -->
    <div class="card mb-0">
      <div class="card-body text-center">
        <img src="{{ asset('images/logo_blue-full.webp') }}" height="150px" alt="Logo">
        <a href="javascript:void(0);" class="brand-logo">
          <h2 class="brand-text text-primary ml-1"><small>Туринский политехнический университет<br>в городе Ташкенте</small><br>HR платформа</h2>
        </a>

        <h4 class="card-title mb-1">Подтвердите адрес Эл. почты! </h4>
        @if (session('resent'))
          <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
          </div>
        @endif
        <p class="card-text mb-2">Прежде чем продолжить, проверьте свою электронную почту на наличие ссылки для подтверждения.</p>
        <p class="card-text">Если вы не получили письмо ,

          <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="btn btn-primary  align-baseline">Нажмите здесь, чтобы запросить другой</button>.
          </form>
        </p>
        <p class="text-center mt-2">
          <a href="{{ route('home.index') }}">
            <i class="feather icon-chevron-left"></i> Вернуться на главную страницу
          </a>
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
