@extends('dashboard.layouts.auth')

@section('title', 'Авторизоваться')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
  <div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
      <!-- Brand logo-->
      <a class="brand-logo d-flex align-items-center" href="javascript:void(0);">
        <div>
          <img src="{{ asset('images/logo_blue-full.webp') }}" height="100" alt="">
        </div>
        <h2 class="brand-text text-primary ml-1"><small>Туринский политехнический университет в городе Ташкенте</small><br>HR платформа</h2>
      </a>
      <!-- /Brand logo-->
      <!-- Left Text-->
      <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
        <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
          <img class="img-fluid" src="{{asset('images/pages/forgot-password-v2.svg')}}" alt="Password Reset" />
        </div>
      </div>
      <!-- /Left Text-->
      <!-- Login-->
      <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
          @if(is_null(session()->get('status')))
          <h4 class="card-title mb-1">Забыли пароль? 🔒</h4>
          <p class="card-text mb-2">Введите свой адрес электронной почты, и мы отправим вам инструкции по сбросу пароля.</p>
          <form class="auth-forgot-password-form mt-2 was-validated" action="{{ route('password.email') }}" method="POST">
            @csrf
            <div class="form-group">
              <label class="form-label" for="forgot-password-email"><i class="fe"></i> Эл. почта</label>
              <input class="form-control @error('email') is-invalid @enderror" id="forgot-password-email" type="email" required name="email" placeholder="john@example.com" aria-describedby="forgot-password-email" autofocus="" tabindex="1" />
              @error('email')
                <span class="invalid-feedback" role="alert">
                  <span>{{ $message }}</span>
                </span>
              @enderror
            </div>
            <button class="btn btn-primary btn-block" tabindex="2">Отправить ссылку для сброса</button>
          </form>
          @else
            <div class="alert alert-primary">
             <div class="alert-body">
               <i class="feather icon-star"></i>
               <strong>Успешно! </strong> Мы отправили вам ссылку для сброса пароля по электронной почте! <br><br> Пожалуйста, проверьте свою электронную почту!
             </div>
            </div>
          @endif
          <p class="text-center mt-2">
            <a href="{{url('login')}}">
              <i class="feather icon-chevron-left"></i> Вернуться на страницу авторизации
            </a>
          </p>
        </div>
      </div>
      <!-- /Login-->
    </div>
  </div>
@endsection

@section('vendor-script')
  <script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
  <script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
@endsection
