@extends('dashboard.layouts.auth')

@section('title', 'Сбросить пароль')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
  <div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
      <!-- Brand logo-->
      <a class="brand-logo d-flex align-items-center" href="javascript:void(0);">
        <div>
          <img src="{{ asset('images/logo_blue-full.webp') }}" height="100" alt="">
        </div>
        <h2 class="brand-text text-primary ml-1"><small>Туринский политехнический университет в городе Ташкенте</small><br>HR платформа</h2>
      </a>
      <!-- /Brand logo-->
      <!-- Left Text-->
      <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
        <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
          <img class="img-fluid" src="{{asset('images/pages/login-v2.svg')}}" alt="Login V2" />
        </div>
      </div>
      <!-- /Left Text-->
      <!-- Login-->
      <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
          <h4 class="card-title mb-1">Сбросить пароль 🔒</h4>
          <p class="card-text mb-2">Ваш новый пароль должен отличаться от ранее использовавшихся паролей.</p>
          <form class="auth-login-form mt-2" action="{{ route('password.update') }}" method="POST">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
              <label class="form-label" for="login-username"><i class="feather icon-user"></i> Эл. почта</label>
              <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="john@example.com" aria-describedby="email" tabindex="1" autofocus value="{{ $email ?? old('email') }}" />
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between">
                <label for="login-password"><i class="feather icon-lock"></i> Пароль</label>
              </div>
              <div class="input-group input-group-merge form-password-toggle">
                <input class="form-control form-control-merge @error('password') is-invalid @enderror" id="login-password" type="password" name="password" autocomplete="new-password" placeholder="············" aria-describedby="login-password" tabindex="3" />
                <div class="input-group-append">
                  <span class="input-group-text cursor-pointer">
                    <i data-feather="eye"></i>
                  </span>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between">
                <label for="login-password"><i class="feather icon-lock"></i>Подтвердите Пароль</label>
              </div>
              <div class="input-group input-group-merge form-password-toggle">
                <input class="form-control form-control-merge @error('password_confirmation') is-invalid @enderror" id="login-password" type="password" name="password_confirmation" placeholder="············" aria-describedby="login-password" tabindex="4" />
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <button class="btn btn-primary btn-block" type="submit" tabindex="4">Установить новый пароль</button>
          </form>
          <p class="text-center mt-2">
            <a href="{{url('login')}}">
              <i class="feather icon-chevron-left"></i> Вернуться на страницу авторизации
            </a>
          </p>
        </div>
      </div>
      <!-- /Login-->
    </div>
  </div>
@endsection

@section('vendor-script')
  <script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
  <script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
@endsection
