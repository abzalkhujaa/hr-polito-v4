{{-- For submenu --}}
<ul class="menu-content">
    @if(isset($menu))
        @foreach($menu as $submenu)
            <li class="{{ (request()->route()->getName() === $submenu->url) ? 'active' : '' }}">
                <a @if(empty($submenu->url))  @else href="{{ route($submenu->url) }}" @endif>
                    <i class="{{ $submenu->icon ?? "" }}"></i>
                    <span class="menu-title">{{ __($submenu->name) }}</span>
                </a>
            </li>
        @endforeach
    @endif
</ul>
