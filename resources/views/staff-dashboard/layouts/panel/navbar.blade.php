            <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
                <div class="navbar-container d-flex content">
                    <div class="bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav d-xl-none">
                            <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav align-items-center ml-auto">
                        <li class="nav-item dropdown dropdown-language">
                            <a class="nav-link dropdown-toggle" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-ru"></i>
                                <span class="selected-language">Русский</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag">
                                <a class="dropdown-item" href="{{url('lang/ru')}}" data-language="en">
                                    <i class="flag-icon flag-icon-ru"></i>Русский
                                </a>
                                <a class="dropdown-item" data-language="uz" disabled>
                                    <i class="flag-icon flag-icon-fr"></i> Uzbek <sup class="badge-primary">Скоро</sup>
                                </a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon feather icon-moon" ></i></a></li>
                        @auth
                            <li class="nav-item dropdown dropdown-user">
                                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="user-nav d-sm-flex d-none">
                                        <span class="user-name font-weight-bolder">{{ auth()->user()->username }}</span>
                                        <span class="user-status">{!! auth()->user()->role_icon() !!} {{ auth()->user()->roles->first()->name ?? 'Гость' }}</span>
                                    </div>
                                    <span class="avatar">
                                        @if(auth()->user() && auth()->user()->file && auth()->user()->file->image)
                                            <img class="round" src="{{asset('storage/'.auth()->user()->file->image)}}" alt="avatar" height="40" width="40" style="object-fit: cover;object-position: top;">
                                        @else
                                            <span class="avatar-content shadow" style="width: 40px;height: 40px">{{ auth()->user()->short_name }}</span>
                                        @endif
                                        <span class="avatar-status-online"></span>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                                    <a class="dropdown-item" href="{{ \App\Helpers\Classes\AuthRedirectHelper::getRedirectPath() }}">
                                        <i class="mr-50 feather icon-user"></i> Профиль
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">
                                        <i class="mr-50" data-feather="help-circle"></i> FAQ
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit()">
                                        <i class="mr-50 feather icon-power"></i> Выйти
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
            </nav>

            {{-- Search Start Here --}}
            <ul class="main-search-list-defaultlist d-none"></ul>

            {{-- if main search not found! --}}
            <ul class="main-search-list-defaultlist-other-list d-none">
                <li class="auto-suggestion justify-content-between">
                    <a class="d-flex align-items-center justify-content-between w-100 py-50">
                        <div class="d-flex justify-content-start">
                            <span class="mr-75 feather icon-alert-circle"></span>
                            <span>Результатов не найдено.</span>
                        </div>
                    </a>
                </li>
            </ul>
        {{-- Search Ends --}}
        <!-- END: Header-->
