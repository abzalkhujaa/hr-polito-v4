<!-- BEGIN: Footer-->
<footer class="footer footer-light">
    <p class="clearfix mb-0">
    <span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2019 - {{ now()->year }} <a class="ml-25" href="https://polito.uz" target="_blank" title="Туринский политехнический университет в городе Ташкенте">TTPU</a>
      <span class="d-none d-sm-inline-block">,Все права защищены</span>
    </span>
        <span class="float-md-right d-none d-md-block">Crafted with<i class="feather icon-heart"></i> by <a href="http://abzalkhuja.uz" target="_blank">Abzalkhujaa</a></span>
    </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
<!-- END: Footer-->
