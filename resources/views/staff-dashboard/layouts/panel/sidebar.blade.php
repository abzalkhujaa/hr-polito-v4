<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{url('/')}}">
                  <span class="brand-logo">
                    <img src="{{ asset('images/logo_blue-full.webp') }}" alt="Logo">
                  </span>
                    <h2 class="brand-text">{{ __('dashboard.hr.title') }}</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            {{-- Foreach menu item starts --}}
            @if(isset($sidebarStaffMenuData))
                @foreach($sidebarStaffMenuData->menu as $menu)
                    @if(isset($menu->navheader))
                        <li class="navigation-header">
                            <span>{{ __($menu->navheader) }}</span>
                        </li>
                    @else
                        <li class="nav-item {{ (request()->route()->getName() === $menu->url) ? 'active' : '' }}">
                            <a @if(empty($menu->url))  @else href="{{ route($menu->url) }}" @endif>
                                <i class="{{ $menu->icon }}"></i>
                                <span class="menu-title">{{ trans($menu->name) }}</span>
                                @if (isset($menu->badge))
                                    <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                                    <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">
                                        {{$menu->badge}}
                                    </span>
                                @endif
                            </a>
                            @if(isset($menu->submenu))
                                @include('dashboard.layouts.panel.submenu', ['menu' => $menu->submenu])
                            @endif
                        </li>
                    @endif
                @endforeach
            @endif
            {{-- Foreach menu item ends --}}
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
