<html lang="{{ app()->getLocale() }}"
      data-textdirection="ltr" xmlns:livewire="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') TTPU HR платформа</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    @include('vendor.favicon')
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    {{-- Include core + vendor Styles --}}
    @include('staff-dashboard.layouts.panel.style')
    @livewireStyles
</head>
<body class="vertical-layout vertical-menu-modern 2-columns floating navbar-floating footer-static menu-expanded pace-done"
      data-menu="vertical-menu-modern"
      data-col="2-columns"
      data-layout="light"
      data-framework="laravel"
      data-asset-path="{{ asset('/')}}">
{{-- Include Sidebar --}}

@include('staff-dashboard.layouts.panel.sidebar')

{{-- Include Navbar --}}
@include('staff-dashboard.layouts.panel.navbar')

<!-- BEGIN: Content-->
    <div class="app-content content">
    <!-- BEGIN: Header-->
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        {{-- Include Breadcrumb --}}
        @include('staff-dashboard.layouts.panel.breadcrumb')

        <div class="content-body">
            {{-- Include Page Content --}}
            @yield('content')
        </div>
    </div>

</div>
<!-- End: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

{{-- include footer --}}
    @include('staff-dashboard.layouts.panel.footer')

    {{-- include default scripts --}}
<script>
    const staff_search_url = '{{ route('api.search-data') }}'
</script>
    @include('staff-dashboard.layouts.panel.scripts')

    <script type="text/javascript">
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14, height: 14
                });
            }
        })
    </script>
    @if(!empty($errors->toArray()))
        <script type="text/javascript">
            let error_data = JSON.parse('@json($errors->toArray())');
            Swal.fire({
                icon: 'error',
                title: error_data[Object.keys(error_data)[0]][0],
                customClass: {
                    confirmButton: 'btn btn-success'
                }
            });
        </script>
    @endif
@livewireScripts
@stack('livewire-script')
</body>
</html>

