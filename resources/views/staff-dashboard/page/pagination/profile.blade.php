@extends('staff-dashboard.layouts.dashboard')

@section('title', auth()->user()->username)

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <section id="user-pagination">
        @include('components.staff.pagination',['user' => $user, 'active' => (string)\App\Helpers\Enums\PaginationTypeEnum::MAIN])
    </section>
    <section class="mt-2">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-12">
                @livewire('staff.pagination.profile',['user' => $user, 'type' => \App\Helpers\Enums\ActionTypeEnum::UPDATE])
            </div>

            @include('components.staff.panel',['user' => $user])
        </div>
    </section>
@endsection

@section('vendor-script')@endsection

@section('page-script')
    <script src="{{ asset('js/scripts/components/components-tooltips.js') }}"></script>
@endsection
