@extends('dashboard.layouts.dashboard')

@section('title', __('dashboard.staffs'))

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
    <style>
        @media (max-width: 576px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }

        @media (min-width: 576px) and (max-width: 768px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }
        .modal-backdrop{
            background-color: #ffffff!important;
        }
        .modal-backdrop.show{
            opacity: 0.8!important;
        }
    </style>
@endsection

@section('content')
    @livewire('staff.all-with-filter')
@endsection
