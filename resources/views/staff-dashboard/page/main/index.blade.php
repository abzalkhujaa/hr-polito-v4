@extends('staff-dashboard.layouts.dashboard')

@section('content')
    <section>
        <div class="row match-height">
            @include('components.staff.panel',['user' => auth()->user()])
            <div class="col-lg-8 col-md-8 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-congratulation-medal">
                            <div class="card-body">
                                <h5>Добро пожаловать 🎉 {{ auth()->user()->username }}!</h5>
                                <p class="card-text font-small-3">Вы вошли как {{ __('dashboard.staff') }}</p>
                                <h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);" class="text-white" style="color: rgba(0,0,0,0);!important;">
                                        <hr style="border-color: rgba(0,0,0,0);">
                                    </a>
                                </h3>
                                <a href="{{ route('staff-dashboard.staff.profile.index') }}" class="btn btn-primary waves-effect waves-float waves-light">Профиль</a>
                                <img src="{{ asset('images/illustration/badge.svg') }}" class="congratulation-medal" alt="Medal Pic">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card card-congratulations">
                            <div class="card-body text-center">
                                <img src="{{ asset('images/elements/decore-left.png') }}" class="congratulations-img-left" alt="card-img-left">
                                <img src="{{ asset('images/elements/decore-right.png') }}" class="congratulations-img-right" alt="card-img-right">
                                <div class="avatar avatar-xl bg-primary shadow">
                                    <div class="avatar-content">
                                        <i class="feather icon-award"></i>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <h1 class="mb-1 text-white">День рождения<br><small>сотрудника</small></h1>
                                    <a href="{{ route('staff-dashboard.staff.profile.index') }}" class="card text-left">
                                        <div class="card-header">
                                            <div>
                                                <h2 class="font-weight-bolder mb-0">
                                                    @if(auth()->user()->birthday_left() < 25)
                                                        Осталось {{ auth()->user()->birthday_left() }} часов
                                                    @else
                                                        Осталось {{ round(auth()->user()->birthday_left() / 24) }} дней
                                                    @endif
                                                </h2>
                                                <h5 class="card-text">{{ auth()->user()->full_name ?? auth()->user()->username }}</h5>
                                            </div>
                                            <div class="avatar bg-light-success p-50 m-0">
                                                <div class="avatar-content">
                                                    <i class="feather icon-gift"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row match-height">

        </div>
    </section>
@endsection
