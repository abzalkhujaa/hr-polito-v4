@extends('front.layouts.main')

@section('title',$breadcrumbs[2]['name'])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('home.certificate.search') }}" method="POST" class="row was-validated align-items-center">
                            @csrf
                            <div class="col-md-3">
                                <x-html.input type="text" name="passport_series" label="Серия паспорта" pattern="pattern=^[A-Z]{2}$" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-3">
                                <x-html.input type="number" name="passport_number" label="Номер паспорта" pattern="min=1000000 max=9999999" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-3">
                                <x-html.input type="date" name="b_day" label="{{ __('dashboard.info_field.bday') }}" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-facebook btn-block">
                                    Поиск
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                @isset($user)
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <div class="container mb-2 px-2 px-sm-3 px-md-4 px-lg-5">
                                        <h1 class="text-center">МАЪЛУМОТНОМА</h1>
                                        <p style="text-indent: 80px;">
                                            Фуқаро <strong>{{ $user->last_name }} {{$user->first_name}} {{$user->patronymic}}</strong> хақиқатдан ҳам Тошкент шаҳридаги Турин политехника университети <b>{{$user->labor->department->title ?? __('dashboard.no_information')}}</b> нинг  <b>{{$user->labor->position->title ?? __('dashboard.no_information')}}</b> лавозимида,
                                            <strong>{{$user->labor->start_command ?? __('dashboard.no_information')}}</strong> асосида ҳозирги кунга қадар фаолият юритиб келмоқда.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-12 text-right">
                                    <img src="{{ asset('storage/qrcode/'.$user->username.'(certificate).png') }}" alt="QR Code" class="img-thumbnail w-25 mb-2">
                                </div>
                                <div class="col-md-6 col-sm-6 col-12 text-left">
                                    <a href="{{ route('home.certificate.download', ['user' => $user->username]) }}" class="btn btn-bitbucket">
                                        <i class="feather icon-download"></i> {{ __('page.dashboard.certificate.index') }}
                                    </a>
                                </div>
                                <div class="col-12 text-right">
                                    <i class="feather icon-check-circle text-success mr-2"></i> <span class="text-primary font-italic font-weight-bold">Все данные подтверждены отделом кадров.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.family_field.first_name') }}</th>
                                        <td class="text-left">{{ $user->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.family_field.last_name') }}</th>
                                        <td class="text-left">{{ $user->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.family_field.patronymic') }}</th>
                                        <td class="text-left">{{ $user->patronymic }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.section') }}</th>
                                        <td class="text-left">{{ $user->labor->section->title ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.department') }}</th>
                                        <td class="text-left">{{ $user->labor->department->title ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.position') }}</th>
                                        <td class="text-left">{{ $user->labor->position->title ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.labor_field.stavka') }}</th>
                                        <td class="text-left">{{ $user->labor->stavka ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.labor_field.start_command') }}</th>
                                        <td class="text-left">{{ $user->labor->start_command ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">{{ __('dashboard.labor_field.finish_command') }}</th>
                                        <td class="text-left">{{ $user->labor->finish_command ?? __("dashboard.no_information") }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-body text-center py-4">
                            @if(request()->getMethod() !== "GET")
                                <h1>Сотрудник не найден с этими данными.</h1>
                                <h2><em>Пожалуйста, попробуйте еще раз с другими данными.</em></h2>
                            @endif
                            <h1 class="text-center">
                                <span class="font-italic font-weight-bold">
                                    <i class="feather icon-check-circle text-success mr-2"></i> Все данные подтверждены отделом кадров.
                                </span>
                            </h1>
                        </div>
                    </div>
                @endisset
            </div>
            <div class="col-md-4 col-12">
                <div class="row match-height">
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.news.index') }}" class="card main-section-item" >
                            <div class="card-header justify-content-center">
                                <i class="fal fa-newspaper text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">{{ __('page.dashboard.blog.news.index') }}</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.decree.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-file-alt text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        {{ __('page.dashboard.blog.decree.index') }}
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.certificate.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tie text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Получить справку
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.vacancy.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tag text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Вакансии
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
