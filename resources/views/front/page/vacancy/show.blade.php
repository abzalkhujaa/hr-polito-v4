@extends('front.layouts.main')

@section('content')
   <div class="container my-4">
       <div class="row align-items-center">
           <div class="col-12 mb-3">
               <h1 class="text-center">{{ $vacancy->title }}</h1>
           </div>
           <div class="col-md-4 mb-3">
               <img src="{{ asset('images/vacancy-hero.webp') }}" class="w-100" alt="">
           </div>
           <div class="col-md-8">
               <div class="mb-3">
                   <h3>Должностные обязанности:</h3>
                   <div>{!! $vacancy->responsibility !!}</div>
               </div>
               <div class="mb-3">
                   <h3>Требования:</h3>
                   <div>{!! $vacancy->requirement !!}</div>
               </div>
               <div class="mb-3">
                   <h3>Мы предлагаем:</h3>
                   <div>{!! $vacancy->content !!}</div>
               </div>
           </div>
           <div class="col-12">
               <div class="card">
                   <div class="card-body text-center py-md-5 my-md-5">
                       <h2 class="text-center mx-1 mx-sm-2 mx-md-4 mx-md-5 mb-4">Если вас заинтересовала данная вакансия,<br>
                           заполните форму у нас на сайте:</h2>
                       <button type="button" class="btn btn-danger rounded btn-lg">Заполнить форму на вакансию <sup class="badge badge-warning">Скоро</sup></button>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection
