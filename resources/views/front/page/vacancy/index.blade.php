@extends('front.layouts.main')

@section('title', __('page.dashboard.blog.vacancy.index'))

@section('content')
    <div class="container">
        <div class="row mb-5 align-items-center">
            <div class="col-8">
                <img src="{{ asset('images/vacancy-hero.webp') }}" alt="Vacancy Hero" class="w-75">
            </div>
            <div class="col-4 text-center">
                <h2 class="text-center text-uppercase mb-3">ВСТУПИ В НАШУ<br>ПРОФЕССИОНАЛЬНУЮ КОМАНДУ</h2>
{{--                <a href="{{ route('home.vacancy.index') }}" class="btn btn-facebook">Вступить</a>--}}
            </div>
        </div>
        <div class="row">
            @forelse($sections as $section)
                <div class="col-md-4 col-12">
                    <a href="{{ route('home.vacancy.group',['section' => $section->alias]) }}" class="card main-section-item" >
                        <div class="card-body">
                            <div class="card-content">
                                <h2 class="text-center">{{ $section->title }}</h2>
                            </div>
                        </div>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
@endsection
