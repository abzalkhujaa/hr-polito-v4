@extends('front.layouts.main')

@section('content')
    <section class="py-5 mb-3" style="background-image: url('{{ asset("images/vacancy_bg-full.webp") }}'); background-position: top center; background-repeat: no-repeat; background-size: cover;">
        <h1 class="text-center text-white my-5">
            {{ $section->title }} | {{ __('page.dashboard.blog.vacancy.index') }}
        </h1>
    </section>
    <div class="container">
        <div class="row">
            @forelse($vacancies as $vacancy)
                <div class="col-md-6 col-12">
                    <a href="{{ route('home.vacancy.show',['section' => $section->alias,'vacancy' => $vacancy->alias]) }}" class="card main-section-item" >
                        <div class="card-body">
                            <div class="card-content">
                                <h2 class="text-left">{{ $vacancy->title }}</h2>
                            </div>
                        </div>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
@endsection
