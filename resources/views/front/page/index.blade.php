@extends('front.layouts.front')

@section('content')
    <section id="main-layout-section" class="container">
        <div class="row align-items-center main-height">
            <div class="col-lg-6 col-md-6 align-items-center justify-content-center text-center mb-4 mb-sm-3 mb-md-0">
                <img src="{{ asset('images/logo_white-full.webp') }}" class="my-4" alt="Polito LOGO">
                <h2 class="text-center text-white text-uppercase mb-3">ВСТУПИ В НАШУ<br>ПРОФЕССИОНАЛЬНУЮ КОМАНДУ</h2>
                <a href="{{ route('home.vacancy.index') }}" class="btn btn-facebook">Вступить</a>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="row match-height">
                    <div class="col-lg-6 col-md-12 col-6">
                        <a href="{{ route('home.news.index') }}" class="card main-section-item" >
                            <div class="card-header justify-content-center">
                                <i class="fal fa-newspaper text-dark"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                   <h2 class="text-center">{{ __('page.dashboard.blog.news.index') }}</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-12 col-6">
                        <a href="{{ route('home.decree.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-file-alt text-dark"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                   <h2 class="text-center">
                                       {{ __('page.dashboard.blog.decree.index') }}
                                   </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-12 col-6">
                        <a href="{{ route('home.certificate.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tie text-dark"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                   <h2 class="text-center">
                                       Получить справку
                                   </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-12 col-6">
                        <a href="{{ route('home.vacancy.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tag text-dark"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Вакансии
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
