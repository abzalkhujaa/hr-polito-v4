@extends('front.layouts.main')

@section('title', $breadcrumbs[2]['name'])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="row match-height">
                    <div class="col-12 mb-2">
                        <div class="card mb-3 text-center">
                            @if($decree->image)
                                <img class="card-img-top" src="{{ asset('storage/'.$decree->image) }}" alt="News image">
                            @else
                                <div class="mt-4 py-1"></div>
                            @endif
                            <div class="card-body text-left">
                                <div class="row mt-3">
                                    <div class="col-8">
                                        <h1>{{ $decree->title }}</h1>
                                    </div>
                                    <div class="col-4 text-right">
                                        <small class="text-muted"><i class="feather icon-calendar"></i> {{ $decree->created_at->format('H:i d.m.Y') }}</small>
                                    </div>
                                    @if($decree->file)
                                        <div class="col-12 text-right">
                                            <a href="{{ asset('storage/'.$decree->file) }}" class="btn btn-facebook">
                                                <i class="feather icon-download"></i> Скачать приказ
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <p class="card-text text-dark mt-4">
                                    {!! $decree->content !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="row match-height">
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.news.index') }}" class="card main-section-item" >
                            <div class="card-header justify-content-center">
                                <i class="fal fa-newspaper text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">{{ __('page.dashboard.blog.news.index') }}</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.decree.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-file-alt text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        {{ __('page.dashboard.blog.decree.index') }}
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.certificate.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tie text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Получить справку
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.vacancy.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tag text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Вакансии
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
