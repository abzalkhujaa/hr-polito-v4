@extends('front.layouts.main')

@section('title', __('page.dashboard.blog.decree.index'))

@section('content')
    <section>
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="row match-height">
                    @forelse($decrees as $one)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-2">
                            <a href="{{ route('home.decree.show', ['decree' => $one->alias]) }}" class="card mb-3">
                                <img class="card-img-top" src="{{ ($one->image) ? asset('storage/'.$one->image) : asset('images/no_photo_hor_min.webp') }}" alt="News image">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $one->title }}</h4>
                                    <p class="card-text text-dark">
                                        {{ $one->description }}
                                    </p>
                                    <p class="card-text text-right">
                                        <small class="text-muted"><i class="feather icon-calendar"></i> {{ $one->created_at->format('H:i d.m.Y') }}</small>
                                    </p>
                                </div>
                            </a>
                        </div>
                    @empty
                        <div class="col-12 mb-2">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <h2 class="card-title text-center">Нет приказов</h2>
                                </div>
                            </div>
                        </div>
                    @endforelse
                </div>

            </div>
            <div class="col-md-4 col-12">
                <div class="row match-height">
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.news.index') }}" class="card main-section-item" >
                            <div class="card-header justify-content-center">
                                <i class="fal fa-newspaper text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">{{ __('page.dashboard.blog.news.index') }}</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.decree.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-file-alt text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        {{ __('page.dashboard.blog.decree.index') }}
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.certificate.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tie text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Получить справку
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-6">
                        <a href="{{ route('home.vacancy.index') }}" class="card main-section-item">
                            <div class="card-header justify-content-center">
                                <i class="fal fa-user-tag text-dark fa-5x"></i>
                            </div>
                            <div class="card-body">
                                <div class="card-content">
                                    <h2 class="text-center">
                                        Вакансии
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
