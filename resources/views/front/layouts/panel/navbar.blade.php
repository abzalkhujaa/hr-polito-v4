<nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center " data-nav="brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="navbar-brand" href="{{ route('home.index') }}">
                    <span class="brand-logo">
                        <img src="{{ asset('images/logo_blue-full.webp') }}" alt="">
                    </span>
                    <h2 class="brand-text mb-0 text-dark">HR платформа</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav">
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link bookmark-star"  data-toggle="tooltip" title="Объективка шаблон" data-placement="right" >
                        <i class="ficon text-warning" data-feather="star"></i>
                    </a>
                    <div class="bookmark-input search-input px-2">
                        <div class="row">
                            <div class="col-12 mb-2 mt-2">
                                <h4 class="text-capitalize text-center">
                                    объективка шаблон
                                </h4>
                            </div>
                            <div class="col-6">
                                <a href="{{ asset('template/uz.doc') }}" download class="card bg-transparent">
                                    <div class="card-body text-center">
                                        <i class="flag-icon flag-icon-uz h1"></i>
                                        <p class="text-uppercase text-dark">узбекский</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="{{ asset('template/ru.doc') }}" download class="card bg-transparent">
                                    <div class="card-body text-center">
                                        <i class="flag-icon flag-icon-ru h1"></i>
                                        <p class="text-uppercase text-dark">Русский</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ml-auto">
            <li class="nav-item dropdown dropdown-language">
                <a class="nav-link dropdown-toggle" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="flag-icon flag-icon-ru"></i>
                    <span class="selected-language">Русский</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag">
                    <a class="dropdown-item"
                       href="#"
                       data-language="en">
                        <i class="flag-icon flag-icon-ru"></i> Русский
                    </a>
                    <a class="dropdown-item"
                       disabled
                       data-language="uz">
                        <i class="flag-icon flag-icon-uz"></i> Узбекский <span class="badge badge-primary">Скоро</span>
                    </a>
                </div>
            </li>
            <li class="nav-item d-none d-lg-block">
                <a class="nav-link nav-link-style">
                    <i class="ficon" data-feather="moon"></i>
                </a>
            </li>
            @guest
                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="avatar">
                            <span class="avatar-content shadow" style="width: 40px;height: 40px">
                                <i class="fal fa-user"></i>
                            </span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user" style="min-width: 18rem;">
                        <a class="dropdown-item" href="{{ route('login') }}">
                            <i class="mr-50 feather icon-log-in"></i> {{ __('dashboard.login') }}
                        </a>
                        <a class="dropdown-item" href="{{ route('register') }}">
                            <i class="mr-50 feather icon-user-plus"></i> {{ __('dashboard.signup') }}
                        </a>

                    </div>
                </li>
            @endguest
            @auth
                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none">
                            <span class="user-name font-weight-bolder">{{ auth()->user()->username }}</span>
                            <span class="user-status">{!! auth()->user()->role_icon() !!} {{ Str::of(auth()->user()->roles->first()->name  ?? 'Гость')->replace('-',' ')->upper() }}</span>
                        </div>
                        <span class="avatar">
                        @if(auth()->user() && auth()->user()->hasFile('image'))
                                <img class="round" src="{{asset(auth()->user()->file->image)}}" alt="avatar" height="40" width="40">
                            @else
                                <span class="avatar-content shadow" style="width: 40px;height: 40px">{{ auth()->user()->short_name }}</span>
                            @endif
                        <span class="avatar-status-online"></span>
                    </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                        <a class="dropdown-item" href="{{ \App\Helpers\Classes\AuthRedirectHelper::getRedirectPath() }}">
                            <i class="mr-50 feather icon-user"></i> Профиль
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <i class="mr-50" data-feather="help-circle"></i> FAQ
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit()">
                            <i class="mr-50 feather icon-power"></i> Выйти
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                    </div>
                </li>
            @endauth
        </ul>
    </div>
</nav>
