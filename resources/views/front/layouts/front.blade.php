<html lang="{{ app()->getLocale() }}"
      data-textdirection="ltr" xmlns:livewire="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') TTPU HR платформа</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    @include('vendor.favicon')
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    {{-- Include core + vendor Styles --}}
    @include('front.layouts.panel.style')
    @livewireStyles
</head>
<body class="horizontal-layout horizontal-menu navbar-floating 1-column footer-static"
      data-menu="horizontal-menu"
      data-col="1-column"
      data-layout="light"
      data-framework="laravel"
      data-asset-path="{{ asset('/')}}">

{{-- Include Navbar --}}
@include('front.layouts.panel.navbar')

<!-- BEGIN: Content-->
<div class="app-content content" id="home-layout" style="background-image:url('{{ asset('images/300.webp') }}')" >
    <!-- BEGIN: Header-->
    <div class="content-overlay"></div>
{{--    <div class="header-navbar-shadow"></div>--}}
    <div class="content-wrapper" >

        <div class="content-body">
            {{-- Include Page Content --}}
            @yield('content')
        </div>
    </div>

</div>
<!-- End: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

{{-- include footer --}}
<div class="creator">
    <span class="float-md-right d-none d-md-block text-white mr-2 mb-2">
        Crafted with <i class="feather icon-heart text-white"></i> by <a href="http://abzalkhuja.uz" target="_blank" class="text-white font-weight-bold">Abzalkhujaa</a>
    </span>
</div>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
{{--@include('front.layouts.panel.footer')--}}

{{-- include default scripts --}}
@include('front.layouts.panel.scripts')

<script type="text/javascript">
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14, height: 14
            });
        }
    })
</script>
@if(!empty($errors->toArray()))
    <script type="text/javascript">
        let error_data = JSON.parse('@json($errors->toArray())');
        Swal.fire({
            icon: 'error',
            title: error_data[Object.keys(error_data)[0]][0],
            customClass: {
                confirmButton: 'btn btn-success'
            }
        });
    </script>
@endif
@livewireScripts
@stack('livewire-script')
</body>
</html>

