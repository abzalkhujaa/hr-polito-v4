<?php

return [
    'super-admin' => 'Супер админ',
    'admin' => 'Админ',
    'moderator' => 'Модератор',
    'staff' => 'Сотрудник',
    'user' => 'Пользователь'
];
