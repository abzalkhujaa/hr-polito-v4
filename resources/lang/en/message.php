<?php

return [
    "file" => [
        "uploaded" => "Файл загружен",
        "uploading" => "Файл загружается",
        "error" => "Ошибка при загрузке",
        "not_uploaded" => "Файл не загружен",
    ],

    'are_you_sure' => 'Вы уверены?',
    'dont_revert' => 'Вы не сможете восстановить!',
    'yes_delete' => 'Да, удалить!',
    'deleted' => 'Удалено!',
    'model_deleted' => 'Запись удалена.',
    'cancel' => 'Отменить',

    'code_500' => 'Внутренняя ошибка сервера',
    'code_404' => 'Не обнаружена',
    'code_200' => 'Успешно!',
    'code_201' => 'Создано!'
];
