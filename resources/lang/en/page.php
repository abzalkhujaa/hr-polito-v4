<?php

return [
    'dashboard' => [
        'title' => 'Главная',
        'group' => [
            'title' => 'Группы',
            'control' => 'Управления',
            'section' => [
                'title' => 'Разделы',
                'create' => 'Добавить раздел',
                'update' => 'Редактировать раздел',
                'input' => [
                    'title' => ' Наименование раздела',
                    'created_at' => 'Создано'
                ]
            ],
            'department' => [
                'title' => 'Отделы',
                'create' => 'Добавить отдел',
                'update' => 'Редактировать отдел',
                'input' => [
                    'title' => 'Наименование отдела',
                    'parent' => 'Выберите Раздел',
                    'parent_title' => 'Раздел',
                    'created_at' => 'Создано'
                ]
            ],
            'position' => [
                'title' => 'Должности',
                'create' => 'Добавить должность',
                'update' => 'Редактировать должность',
                'input' => [
                    'title' => 'Наименование должности',
                    'parent' => 'Выберите Отдел',
                    'parent_title' => 'Отдел',
                    'created_at' => 'Создано'
                ]
            ],
        ],
        'staff' => [
            'title' => 'Сотрудники',
            'table' => 'Таблица',
            'table_title' => 'Таблица сотрудников',
            'index' => 'Список',
            'create' => 'Добавить',
            'control' => 'Статистика',
        ],
        'blog' => [
            'title' => 'Блог',
            'news' => [
                'index' => 'Новости',
                'create' => 'Добавить'
            ],
            'vacancy' => [
                'index' => 'Вакансии',
                'create' => 'Добавить',
                'application' => 'Все заявки'
            ],
            'decree' => [
                'index' => 'Приказы',
                'create' => 'Добавить'
            ]
        ],
        'setting' => [
            'title' => 'Настройки',
            'activity' => [
                'title' => 'Активность'
            ],
            'trash' => [
                'title' => 'Корзина'
            ]
        ],
        'certificate' => [
            'title' => 'Справка',
            'index' => 'Справка',
            'template' => 'Шаблон для справки'
        ],


    ]
];

//page.dashboard.group.title
