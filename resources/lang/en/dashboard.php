<?php

return array(
    "hr" => [
        'title' => 'HR Polito'
    ],
    "username" => "Имя пользователя",
    "email" => "Эл. почта",
    "password" => "Пароль",
    "old_password" => "Старый пароль",
    "new_password" => "Новый пароль",
    "confirm_password" => "Подтвердите Пароль",
    "again_password" =>"Введите пароль еще раз",

    "login" => "Авторизоваться",
    "signup" => "Зарегистрироваться",
    "registration" => "Регистрация",
    "forgot_password" => "Забыл пароль?",
    "reset_password" => "Сброс пароля",
    "remember" => "Запомнить меня",

    "soon" => "Скоро",


    "icon" => array(
        "admin" => "fa-user-crown",
        "user" => "fa-user",
        "staff" => "fa-user-tie",
        "vacant" => "fa-user-tag",
    ),

    "to_home" => "На главную страницу",

    "add" => "Добавить",
    "adding" => "Добавление",
    "added_m" => "Добавлен",
    "added_n" => "Добавлено",
    "added_j" => "Добавлена",

    "editing" => "Редактирование",

    "updated" => "Обновлено",
    "deleted" => "Удалено",

    "section" => "Раздел",
    "sections" => "Разделы",
    "department" => "Отдел",
    "departments" => "Отделы",
    "position" => "Должность",
    "positions" => "Должности",

    "section_added" => "Раздел добавлен",
    "department_added" => "Отдел добавлен",
    "position_added" => "Должность добавлена",

    "section_updated" => "Раздел обновлен",
    "department_updated" => "Отдел обновлен",
    "position_updated" => "Должность обновлена",

    "not_saved" => "Не сохранен",
    "not_updated" => "Не обновлен",
    "not_deleted" => "Ошибка при удалении",

//    For Form

    "empty_request" => "Данные не существуют для заполнения",

//    Staff
    "staffs" => "Сотрудники",
    "staff" => "Сотрудник",

    "main" => "Основные",
    "profile" => "Профиль",
    "info" => "Инфо",
    "information" => "Инфо",
    "education" => "Образование",
    "labor" => "Деятельность",
    "work" => "Работа",
    "family" => "Семья",
    "file" => "Файлы",
    'vacation' => 'Отпуск',

    'vacation_field' => [
        'title' => 'Отпуск',
        'type' => 'Тип отпуска',
        'choose_type' => 'Выберите тип отпуска',
        'days' => 'Количество отпускных дней',
        'start' => 'День начала отпуска',
        'finish' => 'Завершающий день отпуска',
        'period' => 'Период',
        'additional' => 'Дополнительная информация'
    ],

    "data_empty" => "Не указано",
    "staff_added" => "Сотрудник добавлен",

    "vacancy_added" => "Вакансия добавлена",
    "vacancy_updated" => "Вакансия обновлена",
    "vacancy_deleted" => "Вакансия удалена",
    "vacancy_not_deleted" => "Вакансия не удалена",

    "news_added" => "Новости добавлены",
    "news_updated" => "Новости обновлены",
    "news_deleted" => "Новости удалены",
    "news_not_deleted" => "Новости не были удалены",

    "decree_added" => "Приказ добавлен",
    "decree_updated" => "Приказ обновлен",
    "decree_deleted" => "Приказ удален",
    "decree_not_deleted" => "Приказ не удален",


    "info_added" => "Информация добавлена",
    "info_add" => "Добавить информацию",
    "info_updated" => "Информация обновлена",
    "info_update" => "Обновление информации",
    "info_not_deleted" => "Информация не удалена",
    "info_deleted" => "Информация удалена",

    "profile_field" => [
        "title" => "Информация о профиле",
        "fname" => "Имя",
        "lname" => "Фамилия",
        "pname" => "Отчество",
        "gender" => "Пол",
    ],

    "info_field" => [
        "title" => "Информация о профиле",
        "passport_number" => "Серийный номер паспорта",
        "bday" => "День рождения",
        "city" => "Место рождения",
        "nationality" => "Национальность",
        "region" => "Регион (текущий)",
        "district" => "Район (Текущий)",
        "address" => "Адрес (текущий)",
        "phone" => "Номер телефона",
        "homephone" => "Домашний телефон"
    ],

    "education_field" => [
        "title" => "Информация о профиле",
        "education" => "Образование",
        "finished" => "Окончил",
        "year" => "Год",
        "specialty" => "Специальность по образованию",
        "partying" => "Партийность",
        "academic_degree" => "Учёная степень",
        "academic_title" => "Учёное звание",
        "foreign_lang" => "Какими иностранными языками владеет",
        "military_rank" => "Военное (специальное) звание",
        "state_award" => "Имеет ли Государственные награды (какие)",
        "state_member" => "Является ли народным депутатом, членом центральных, республиканских, областных, городских, районных и других выборных органов (указать полностью)"
    ],

    "labor_field" => [
        "title" => "Информация о профиле",
        "section" => "Выберите раздела",
        "department" => "Выберите отдела",
        "position" => "Выберите должность",
        "stavka" => "Ставка",
        "basis_labor" => "Основа труда",
        "working_limit" => "Срок трудового договора",
        "start_command" => "Дата принятия на должность, № приказа",
        "finish_command" => "Дата увольнение с должности, № приказа",
        "last_vacation" => "Последний отпуск",
        "maternity" => "Декретный отпуск (для женщин)",
        "pensioner" => "Пенсионер",
        "disciplinary" => "Дисциплинарное наказание",
        "contract_number" => "Номер договора",
        "employment_num" => "Номер трудовой книжки №",
        "development" => "Повышения квалификации",
        "inn_number" => "ИНН"
    ],

    "work_field" => [
        "title" => "Трудова деятельность",
        "start" => "Начало",
        "finish" => "Конец",
        "place" => "Место работы и должност"
    ],

    "family_field" => [
        "title" => "СВЕДЕНИЯ о близких",
        "family_member" => "Степень родства",
        "full_name" => 'Ф.И.О.',
        "first_name" => "Имя",
        "last_name" => "Фамилия",
        "patronymic" => "Отчество",
        "b_day" => "Год рождения",
        "city" => "Место рождения",
        "work" => "Место работы и должность",
        "current_place" => "Место жительства"
    ],

    "file_field" => [
        "title" => "Файлы сотрудников",
        "image" => "Изображение",
        "diploma" => "Диплом",
        "inn" => "ИНН",
        "inps" => "ИНПС",
        "military" => "Военный билет",
        "passport" => "Паспорт"
    ],

    "news_field" => [
        'title' => 'Заголовок',
        'description' => 'Краткая информация',
        'content' => 'Основное содержание',
        'image' => 'Изображение новости',
        'creator' => 'Создатель',
        'created_at' => 'Время создания'
    ],

    "full_name" => "Полное имя",

    'action' => [
        'title' => 'Действие',
        'add' => 'Добавить',
        'upload' => 'Загрузить',
        'success' => 'Успешно',
        'error' => 'Ошибка',
        'choose' => 'Перетащите файл сюда или нажмите',
        'download' => 'Скачать',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
        'view' => 'Просмотр',
        'save' => 'Сохранить',
        'cancel' => 'Отменить',
        'created' => 'Created',
        'saved' => 'Saved',
        'updated' => 'Updated',
        'not_created' => 'Not created',
        'not_saved' => 'Not saved',
        'not_updated' => 'Not updated',
        'password' => [
            'changed' => 'Пароль изменен.',
        ],
        "dropzone_placeholder" => "Перетащите сюда файл или щелкните, чтобы выбрать файл <u> Выбрать </u>",
        "dropzone_hint" => "<p><small><sup class='text-danger'>*</sup>При загрузке нового файла старый файл удаляется</small></p>",
        "dropzone_delete" => "<span class='badge-pill badge-danger mt-3'>Удалить</span>",
        "dropzone_upload_error" => "Ошибка при загрузке файла",
        "dropzone_upload_success" => "Файл успешно загружен",

    ],

    'role' => 'Роль',
    'status' => 'Статус',
    'new_user' => 'Новый сотрудник',

    'no_gender' => 'Без пола',
    'male' => 'Мужчина',
    'female' => 'Женский',

    'no_information' => 'Нет информации',



);
