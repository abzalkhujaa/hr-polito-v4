<?php

return array(

    "username" => "Имя пользователя",
    "email" => "Эл. почта",
    "password" => "Пароль",
    "new_password" => "Новый пароль",
    "confirm_password" => "Подтвердите Пароль",
    "again_password" =>"Введите пароль еще раз",

    "login" => "Авторизоваться",
    "signup" => "Зарегистрироваться",
    "registration" => "Регистрация",
    "forgot_password" => "Забыл пароль?",
    "reset_password" => "Сброс пароля",
    "remember" => "Запомнить меня",

    "soon" => "Скоро",


    "icon" => array(
        "admin" => "fa-user-crown",
        "user" => "fa-user",
        "staff" => "fa-user-tie",
        "vacant" => "fa-user-tag",
    ),

    "to_home" => "На главную страницу",

    "add" => "Добавить",
    "adding" => "Добавление",
    "added_m" => "Добавлен",
    "added_n" => "Добавлено",
    "added_j" => "Добавлена",

    "editing" => "Редактирование",

    "updated" => "Обновлено",
    "deleted" => "Удалено",

    "section" => "Раздел",
    "sections" => "Разделы",
    "department" => "Отдел",
    "departments" => "Отделы",
    "position" => "Должность",
    "positions" => "Должности",

    "section_added" => "Раздел добавлен",
    "department_added" => "Отдел добавлен",
    "position_added" => "Должность добавлена",

    "section_updated" => "Раздел обновлен",
    "department_updated" => "Отдел обновлен",
    "position_updated" => "Должность обновлена",

    "not_saved" => "Не сохранен",
    "not_updated" => "Не обновлен",
    "not_deleted" => "Ошибка при удалении",

//    For Form

    "empty_request" => "Данные не существуют для заполнения",

//    Staff
    "staffs" => "Сотрудники",
    "staff" => "Сотрудник",


    "main" => "Основные",
    "profile" => "Профиль",
    "info" => "Инфо",
    "education" => "Образование",
    "labor" => "Деятельность",
    "work" => "Работа",
    "family" => "Семья",
    "file" => "Файлы",

    "data_empty" => "Не указано",
    "staff_added" => "Сотрудник добавлен",

    "vacancy_added" => "Вакансия добавлена",
    "vacancy_updated" => "Вакансия обновлена",
    "vacancy_deleted" => "Вакансия удалена",
    "vacancy_not_deleted" => "Вакансия не удалена",

    "news_added" => "Новости добавлены",
    "news_updated" => "Новости обновлены",
    "news_deleted" => "Новости удалены",
    "news_not_deleted" => "Новости не были удалены",

    "decree_added" => "Приказ добавлен",
    "decree_updated" => "Приказ обновлен",
    "decree_deleted" => "Приказ удален",
    "decree_not_deleted" => "Приказ не удален",


    "info_added" => "Информация добавлена",
    "info_add" => "Добавить информацию",
    "info_updated" => "Информация обновлена",
    "info_update" => "Обновление информации",
    "info_not_deleted" => "Информация не удалена",
    "info_deleted" => "Информация удалена",

    "profile_field" => [
        "fname" => "Имя",
        "lname" => "Фамилия",
        "pname" => "Отчество",
        "gender" => "Пол",
    ],

    "info_field" => [
        "passport_number" => "Серийный номер паспорта",
        "bday" => "День рождения",
        "city" => "Место рождения",
        "nationality" => "Национальность",
        "region" => "Регион (текущий)",
        "district" => "Район (Текущий)",
        "address" => "Адрес (текущий)",
        "phone" => "Номер телефона",
        "homephone" => "Домашний телефон"
    ],

    "education_field" => [
        "education" => "Образование",
        "finished" => "Окончил",
        "specialty" => "Специальность по образованию",
        "partying" => "Партийность",
        "academic_degree" => "Учёная степень",
        "academic_title" => "Учёное звание",
        "foreign_lang" => "Какими иностранными языками владеет",
        "military_rank" => "Военное (специальное) звание",
        "state_award" => "Имеет ли Государственные награды (какие)",
        "state_member" => "Является ли народным депутатом, членом центральных, республиканских, областных, городских, районных и других выборных органов (указать полностью)"
    ],

    "labor_field" => [
        "section" => "Выберите раздела",
        "department" => "Выберите отдела",
        "position" => "Выберите должность",
        "stavka" => "Ставка",
        "basis_labor" => "Основа труда",
        "working_limit" => "Срок трудового договора",
        "start_command" => "Дата принятия на должность, № приказа",
        "finish_command" => "Дата увольнение с должности, № приказа",
        "last_vocation" => "Последний отпуск",
        "maternity" => "Декретный отпуск (для женщин)",
        "pensioner" => "Пенсионер",
        "disciplinary" => "Дисциплинарное наказание",
        "contract_number" => "Номер договора",
        "employment_num" => "Номер трудовой книжки №",
        "development" => "Повышения квалификации",
        "inn_number" => "ИНН"
    ],

    "work_field" => [
        "start" => "Начало",
        "finish" => "Конец",
        "place" => "Место работы и должност"
    ],

    "family_field" => [
        "family_member" => "Степень родства",
        "fname" => "Имя",
        "lname" => "Фамилия",
        "pname" => "Отчество",
        "bday" => "Год рождения",
        "city" => "Место рождения",
        "work" => "Место работы и должность",
        "current_place" => "Место жительства"
    ],

    "file_field" => [
        "image" => "Изображение",
        "diploma" => "Диплом",
        "inn" => "ИНН",
        "inps" => "ИНПС",
        "military_id" => "Военный билет",
        "passport" => "Паспорт"
    ],

    "full_name" => "Полное имя",

    "page" => [
        "title" => "Dashboard",
        "group" => [
            "title" => "Группы",
            "control" => "Управления",
            "section" => [
                "title" => "Разделы",
                "create" => "Добавить раздел",
            ],
            "department" => [
                "title" => "Отделы",
                "create" => "Добавить отдел",
            ],
            "position" => [
                "title" => "Должности",
                "create" => "Добавить должность",
            ],
        ]
    ],



);
