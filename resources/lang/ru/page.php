<?php

return array(
    "dashboard" => [
        "title" => "",
        "group" => [
            "title" => "Группы",
            "control" => "Управления",
            "section" => [
                "title" => "Разделы",
                "create" => "Добавить раздел",
            ],
            "department" => [
                "title" => "Отделы",
                "create" => "Добавить отдел",
            ],
            "position" => [
                "title" => "Должности",
                "create" => "Добавить должность",
            ],
        ]
    ],
    'front' => [
        'title' => 'Main'
    ]
);
