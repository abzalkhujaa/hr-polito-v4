<?php

return [
    "file" => [
        "uploaded" => "Файл загружен",
        "uploading" => "Файл загружается",
        "error" => "Ошибка при загрузке",
        "not_uploaded" => "Файл не загружен",
    ]
];
