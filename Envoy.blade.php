@setup
    $user = "laravel-deployer";
    $timezone = 'Asia/Tashkent';
    $path = '/var/www/gitlab/clone/hr-polito-v4';
    $repo = 'https://gitlab.com/abzalkhujaa/hr-polito-v4.git';
    $chmod = 'storage/logs';
    $branch = 'master';
@endsetup

@servers(["production" => $user."@hr.polito.uz -p1295"])

@task("pull",["on" => $on])
    cd {{ $path }}
    git pull
    echo "========== #1 - Pulled changes =========="
@endtask

@task("composer",["on" => $on])
    composer self-update
    cd {{ $path }}
    composer update
    echo "#2 - Composer dependencies have been updated"
@endtask

@task("npm_runner",['on' => $on])
    cd {{ $release }}
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    npm install
    npm run production
    echo "#3 - Production dependencies have been installed"
@endtask

@task('chmod',["on" => $on])
    chgrp -R www-data {{ $path }};
    chmod -R ug+rwx {{ $path }}

    chmod -R 775 {{ $path }}/{{ $chmods }}
    echo "Permission have been set for {{ $chmods }}"

    echo "#4 - Permission has been set"
@endtask

@task('migration_fresh',['on' => $on])
    cd {{ $path }}
    php artisan migrate
@endtask

@task('migration_rollback',['on' => $on])
    cd {{ $path }}
    php artisan migrate:rollback
@endtask

@task('migration_seeder',['on' => $on])
    cd {{ $path }}
    php artisan db:seed
@endtask

@macro("deploy",["on" => "production"])
    pull
    composer
@endmacro

@macro("deploy_only_pull",["on" => "production"])
    pull
@endmacro

@macro('run_migration',["on" => "production"])
    migration_fresh
@endmacro

@macro("fresh_migration",["on" => "production"])
    migration_fresh
    migration_seeder
@endmacro

@macro("run_migration_fresh",["on" => "production"])
    migration_rollback
    migration_fresh
    migration_seeder
@endmacro

@macro("run_migration_rollback",["on" => "production"])
    migration_rollback
@endmacro

@macro("full_deploy",["on" => "production"])
    pull
    composer
    npm_runner
    chmod
@endmacro
