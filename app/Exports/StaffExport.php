<?php

namespace App\Exports;

use App\Models\User;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StaffExport extends DefaultValueBinder implements FromCollection,ShouldAutoSize,WithCustomStartCell,WithStrictNullComparison,WithStyles,WithCustomValueBinder,WithColumnWidths
{

    use Exportable;

    private string $fileName = 'waybills.xlsx';
    private string $writerType = Excel::XLSX;
    private array $headers = [
        'Content-Type' => 'text/csv',
    ];
    public Collection $users;
    private User $user;

    public function __construct(Collection $users)
    {
        $this->users = $users;
    }


    public function collection(): Collection
    {
        $collection = new Collection();
        foreach ($this->users as $key => $user){
            $this->user = $user;
            if ($this->user->has_certificate)
                $collection->push([
                    $key+1,
                    $this->user->full_name,
                    $this->user->information->getBirthDay()[2],
                    $this->user->information->getBirthDay()[1],
                    $this->user->information->getBirthDay()[0],
                    now()->diffInYears($this->user->information->b_day),
                    $this->user->information->nationality,
                    $this->user->information->city,
                    $this->user->information->address,
                    $this->user->labor->section->title,
                    $this->user->labor->department->title,
                    $this->user->labor->position->title,
                    $this->user->labor->stavka,
                    $this->user->labor->basis_labor,
                    $this->user->labor->start_command,
                    $this->user->labor->finish_command,
                    '',
                    $this->getUserFinishedDataString($this->user),
                    $this->user->education->specialty,
                    $this->user->education->academic_degree,
                    $this->user->labor->last_vacation,
                    $this->user->labor->maternity ?? '',
                    $this->user->labor->pensioner,
                    $this->user->labor->disciplinary,
                    $this->user->labor->contract_number,
                    $this->user->labor->employment_num,
                    $this->user->labor->development,
                    ($this->user->gender === 'F') ? 'Аёл' : 'Эркак',
                ]);
        }
        return $collection;
    }

    private function getUserFinishedDataString(User $user): string
    {
        $data = $user->finished->pluck('place');
        return implode("\n\n", $data->toArray());
    }

    public function startCell(): string
    {
        return 'A4';
    }

    /**
     * @param Worksheet $sheet
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function styles(Worksheet $sheet): array
    {
        $sheet->mergeCells('A1:A2')->setCellValue('A1','№');
        $sheet->mergeCells('B1:B2')->setCellValue('B1','Ф.И.Ш. (паспорт бўйича)');
        $sheet->mergeCells('C1:E1')->setCellValue('C1','Туғилган сана (паспорт бўйича)');
        $sheet->setCellValue('C2','кун');
        $sheet->setCellValue('D2','ой');
        $sheet->setCellValue('E2','йил');
        $sheet->mergeCells('F1:F2')->setCellValue('F1','Ёши');
        $sheet->mergeCells('G1:G2')->setCellValue('G1','Миллати');
        $sheet->mergeCells('H1:H2')->setCellValue('H1','Туғилган жойи (паспорт бўйича)');
        $sheet->mergeCells('I1:I2')->setCellValue('I1','Яшаш жойи (паспорт бўйича)');
        $sheet->mergeCells('J1:J2')->setCellValue('J1','Ишчи гуруҳ');
        $sheet->mergeCells('K1:K2')->setCellValue('K1','Кафедра, бўлим, деканат ва б.');
        $sheet->mergeCells('L1:L2')->setCellValue('L1','Лавозими');
        $sheet->mergeCells('M1:M2')->setCellValue('M1','Иш меъёри (ставка)');
        $sheet->mergeCells('N1:N2')->setCellValue('N1','Мехнат асоси');
        $sheet->mergeCells('O1:O2')->setCellValue('O1','Лавозимга тайинланган сана буйруқ №');
        $sheet->mergeCells('P1:P2')->setCellValue('P1','Лавозимдан олинган сана буйруқ №');
        $sheet->mergeCells('Q1:Q2')->setCellValue('Q1','Мехнат шартномаси муддати');
        $sheet->mergeCells('R1:R2')->setCellValue('R1','Тугатган олий таълим муассасаси номи');
        $sheet->mergeCells('S1:S2')->setCellValue('S1','Диплом бўйича мутахассислиги ва №');
        $sheet->mergeCells('T1:T2')->setCellValue('T1','Илмий даража');
        $sheet->mergeCells('U1:U2')->setCellValue('U1','Сўнги таътил муддати');
        $sheet->mergeCells('V1:V2')->setCellValue('V1','Туғруқ таътили муддати (аёллар)');
        $sheet->mergeCells('W1:W2')->setCellValue('W1','Пенсионер');
        $sheet->mergeCells('X1:X2')->setCellValue('X1','Интизомий жазо олганлиги');
        $sheet->mergeCells('Y1:Y2')->setCellValue('Y1','Шартнома рақами');
        $sheet->mergeCells('Z1:Z2')->setCellValue('Z1','Меҳнат дафтарчаси рақами');
        $sheet->mergeCells('AA1:AA2')->setCellValue('AA1','Малака ошириш');
        $sheet->mergeCells('AB1:AB2')->setCellValue('AB1','Жинси');

        $sheet->setCellValue('A3','1');
        $sheet->setCellValue('B3','2');
        $sheet->mergeCells('C3:E3')->setCellValue('C3','3');
        $sheet->setCellValue('G3','4');
        $sheet->setCellValue('H3','5');
        $sheet->setCellValue('I3','6');
        $sheet->setCellValue('K3','7');
        $sheet->setCellValue('L3','8');
        $sheet->setCellValue('M3','9');
        $sheet->setCellValue('N3','10');
        $sheet->setCellValue('O3','11');
        $sheet->setCellValue('P3','12');
        $sheet->setCellValue('Q3','13');
        $sheet->setCellValue('R3','14');
        $sheet->setCellValue('S3','15');
        $sheet->setCellValue('T3','16');
        $sheet->setCellValue('U3','17');
        $sheet->setCellValue('V3','18');
        $sheet->setCellValue('X3','19');
        $sheet->setCellValue('Y3','20');
        $sheet->setCellValue('Z3','21');
        $sheet->setCellValue('AA3','22');
        $sheet->setCellValue('AB3','23');

        $sheet->getStyle('A1:AB500')->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');

        return [
            'A1:AB3' => [
                'font' => [
                    'bold' => true,
                ]
            ]
        ];
    }


    public function columnWidths(): array
    {
        return [
          'A' => 5,
          'B' => 32,
          'C' => 10,
          'D' => 10,
          'E' => 10,
          'F' => 12,
          'R' => 32,
        ];
    }
}
