<?php


namespace App\Actions\Decree;


use App\DTO\DecreeDTO;
use App\Models\Decree;

class CreateDecreeAction
{

    public function execute(DecreeDTO $data): Decree
    {
        return Decree::create($data->toArray());
    }
}
