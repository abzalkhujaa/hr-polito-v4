<?php


namespace App\Actions\Decree;


use App\DTO\DecreeDTO;
use App\Models\Decree;

class UpdateDecreeAction
{

    public function execute(DecreeDTO $data, Decree $decree): Decree
    {
        $decree->update($data->toArray());
        return $decree;
    }

}
