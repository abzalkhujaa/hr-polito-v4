<?php


namespace App\Actions;

use Illuminate\Http\UploadedFile;
use Storage;

class FileAction extends Actions
{
    public const NEWS = 'news';
    public const DECREE = 'decree';
    public const VACANCY = 'vacancy';
    public const STAFF = 'staff';
    public const QRCODE = 'qrcode';
    public const REFERENCE = 'reference';

    public UploadedFile $file;
    public string $path;

    public static function save(UploadedFile $file, string $path): string{
        return $file->store($path);
    }

    public static function delete($file): bool
    {
        return Storage::delete($file);
    }

}
