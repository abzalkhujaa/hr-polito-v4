<?php


namespace App\Actions;


use App\DTO\User\UserDTO;
use App\Models\User;

class UpdateUserAction extends Actions
{

    private CreateQrCodeAction $createQrCode;

    public function __construct(CreateQrCodeAction $createQrCode)
    {
        $this->createQrCode = $createQrCode;
    }

    public function execute(UserDTO $data,User $user) : User
    {
        $user->update($data->toArray());
        $this->createQrCode->execute($user);
        return $user;
    }

}
