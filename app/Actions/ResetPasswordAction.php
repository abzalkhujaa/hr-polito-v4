<?php


namespace App\Actions;


use App\Models\User;
use Hash;


class ResetPasswordAction
{
    /*
     * Default Password
     * Password example: hr-user2021 up to date
     */
    private string $default_password = "hr-user2020";

    public function execute(User $user): User
    {
        $user->update([
            "password" => Hash::make($this->default_password),
            "reset_password" => true
        ]);
        return $user;
    }

    public function getDefaultPassword(): string
    {
        return $this->default_password;
    }

}
