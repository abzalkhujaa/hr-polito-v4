<?php


namespace App\Actions;


use App\Models\User;
use Mnvx\EloquentPrintForm\PrintFormProcessor;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CreateStaffCertificateAction
{

    private function getTemplatePath(): string
    {
        return public_path('template/certificate.docx');
    }

    private function getOutputFileName(string $name): string
    {
        return $name . '(certificate).docx';
    }

    /**
     * @throws \Mnvx\EloquentPrintForm\PrintFormException
     */
    public function execute(User $user): BinaryFileResponse
    {
        $entity = $user;
        $printFormProcessor = new PrintFormProcessor();
        $tempFileName = $printFormProcessor->process($this->getTemplatePath(), $entity, [
            'image' => function(TemplateProcessor $processor, string $variable, ?string $value) use ($user){
                $processor->setImageValue($variable,array(['path' =>public_path('storage/qrcode/'.$user->username.'(certificate).png'),'width' =>100,'height'=>100]));
            },
        ]);
        return response()
            ->download($tempFileName, $this->getOutputFileName($user->username))
            ->deleteFileAfterSend();
    }

}
