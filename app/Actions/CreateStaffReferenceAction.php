<?php


namespace App\Actions;


use App\Models\User;
use Mnvx\EloquentPrintForm\PrintFormProcessor;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CreateStaffReferenceAction
{

    private function getTemplatePath(): string
    {
        return public_path('template/reference.docx');
    }

    private function getOutputFileName(string $name): string
    {
        return $name . '(reference).docx';
    }

    /**
     * @throws \Mnvx\EloquentPrintForm\PrintFormException
     */
    public function execute(User $user): BinaryFileResponse
    {
        $entity = $user;
        $printFormProcessor = new PrintFormProcessor();
        $tempFileName = $printFormProcessor->process($this->getTemplatePath(), $entity, [
            'image' => function(TemplateProcessor $processor, string $variable, ?string $value) use ($user){
                $processor->setImageValue($variable,array(['path' =>public_path('storage/'.$user->file->image),'width' =>100,'height'=>133]));
            },
            'finished_data' => function(TemplateProcessor $processor, string $variable, ?string $value) use ($user) {
                $inline = new TextRun();
                foreach ($user->finished as $key => $one){
                    $inline->addText($one->year . ' й. ' . $one->place);
                    if ($user->finished->count() !== $key + 1) {
                        $inline->addTextBreak();
                    }
                }
                $processor->setComplexValue($variable, $inline);
            },
            'staff_start' => function(TemplateProcessor $processor, string $variable, ?string $value) use ($user) {
                $inline = new TextRun();
                $last = explode(',',$user->labor->start_command);
                $inline->addText($last[0]);
                $processor->setComplexValue($variable, $inline);
            },
            'staff_position' => function(TemplateProcessor $processor, string $variable, ?string $value) use ($user) {
                $inline = new TextRun();
                $last = $user->works->last()->place;
                $inline->addText($last);
                $processor->setComplexValue($variable, $inline);
            },
        ]);
        return response()
            ->download($tempFileName, $this->getOutputFileName($user->username))
            ->deleteFileAfterSend();
    }

}
