<?php


namespace App\Actions;


use App\Models\User;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Storage;

class CreateQrCodeAction extends Actions
{
    private string $ext = 'png';

    public function execute(User $user): string
    {
        $url = route('auth.qrcode',['user' => $user->username]);
        $path = 'app/qrcode/'.$user->username.'.'.$this->ext;
        $url_certificate = route('check.certificate',['user' => $user->username]);
        $path_certificate = 'app/qrcode/'.$user->username.'(certificate).'.$this->ext;
        if (Storage::exists($path)) return storage_path($path);
        QrCode::encoding('UTF-8')
            ->errorCorrection('L')
            ->style('round')
            ->eye('square')
            ->size(1000)
            ->margin(1.5)
            ->format($this->ext)
            ->generate($url,storage_path($path));
        QrCode::encoding('UTF-8')
            ->errorCorrection('L')
            ->style('round')
            ->eye('square')
            ->size(1000)
            ->margin(1.5)
            ->format($this->ext)
            ->generate($url_certificate,storage_path($path_certificate));
        return $path;
    }

}
