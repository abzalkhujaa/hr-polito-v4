<?php


namespace App\Actions\News;


use App\DTO\NewsDTO;
use App\Models\News;

class CreateNewsAction
{

    public function execute(NewsDTO $data): News
    {
        return News::create($data->toArray());
    }

}
