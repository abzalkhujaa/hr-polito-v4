<?php


namespace App\Actions\News;


use App\DTO\NewsDTO;
use App\Models\News;

class UpdateNewsAction
{

    public function execute(NewsDTO $data, News $news):News
    {
        $news->update($data->toArray());
        return $news;
    }

}
