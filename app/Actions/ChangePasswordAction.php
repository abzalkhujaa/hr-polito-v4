<?php


namespace App\Actions;


use App\DTO\User\UserChangePasswordDTO;
use App\Models\User;
use Hash;

class ChangePasswordAction
{

    public function execute(UserChangePasswordDTO $data,User $user): User
    {
        $user->update([
            "password" => Hash::make($data->password),
            "reset_password" => true
        ]);

        return $user;
    }

}
