<?php


namespace App\Actions\Vacancy;


use App\DTO\VacancyDTO;
use App\Models\Vacancy;

class UpdateVacancyAction
{

    public function execute(VacancyDTO $data,Vacancy $vacancy): Vacancy
    {
        $vacancy->update($data->toArray());
        return $vacancy;
    }

}
