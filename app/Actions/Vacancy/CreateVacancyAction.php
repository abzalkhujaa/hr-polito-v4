<?php


namespace App\Actions\Vacancy;


use App\DTO\VacancyDTO;
use App\Models\Vacancy;

class CreateVacancyAction
{

    public function execute(VacancyDTO $data): Vacancy
    {
        return Vacancy::create($data->toArray());
    }

}
