<?php


namespace App\Actions\File;


use App\Http\Requests\FileDeleteRequest;
use Storage;

class DeleteUploadedFileAction
{
    public function execute(FileDeleteRequest $request): bool{
        return Storage::delete($request->input('file'));
    }
}
