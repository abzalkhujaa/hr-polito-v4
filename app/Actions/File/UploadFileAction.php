<?php


namespace App\Actions\File;


use App\Helpers\Enums\UploadFileTypeEnum;
use App\Http\Requests\FileUploadRequest;

class UploadFileAction
{

    public function execute(FileUploadRequest $request): ?string
    {
        $filepath = $this->checkType($request->input('type'),$request->input('folder'));
        return (!is_null($filepath)) ? $request->file('file')->store($filepath) : null;
    }

    private function checkType(string $type,string $folder): ?string
    {
        if ($type === UploadFileTypeEnum::STAFF) return $this->staffFileType($folder);
        if ($type === UploadFileTypeEnum::NEWS) return $this->newsFileType();
        if ($type === UploadFileTypeEnum::PUBLIC) return $this->publicFileType($folder);
        return null;
    }

    private function staffFileType(string $folder): string
    {
        return UploadFileTypeEnum::STAFF.'/'.$folder;
    }

    private function newsFileType(): string
    {
        return UploadFileTypeEnum::NEWS;
    }

    private function publicFileType(string $folder): string
    {
        return UploadFileTypeEnum::PUBLIC;
    }







}
