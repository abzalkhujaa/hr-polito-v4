<?php


namespace App\Actions\Staff;


use App\DTO\User\UserLaborDTO;
use App\Models\User;

class SaveLaborStaffAction
{

    public function executeStore(UserLaborDTO $data, User $user): User
    {
        $user->labor()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserLaborDTO $data, User $user): User
    {
        $user->labor()->update($data->toArray());
        return $user;
    }

}
