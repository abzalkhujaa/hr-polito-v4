<?php


namespace App\Actions\Staff;


use App\DTO\User\UserFamilyDTO;
use App\Models\User;
use App\Models\UserFamily;

class SaveFamilyStaffAction
{

    public function executeStore(UserFamilyDTO $data, User $user): User
    {
        $user->family()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserFamilyDTO $data, User $user, UserFamily $model): User
    {
        $model->update($data->toArray());
        return $user;
    }

}
