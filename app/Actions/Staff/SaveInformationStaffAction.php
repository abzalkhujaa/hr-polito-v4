<?php


namespace App\Actions\Staff;


use App\DTO\User\UserInformationDTO;
use App\Models\User;

class SaveInformationStaffAction
{

    public function executeStore(UserInformationDTO $data,User $user): User
    {
        $user->information()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserInformationDTO $data, User $user): User
    {
        $user->information()->update($data->toArray());
        return $user;
    }

}
