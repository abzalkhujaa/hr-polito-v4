<?php


namespace App\Actions\Staff;


use App\DTO\User\UserEducationFinishedDTO;
use App\Models\User;
use App\Models\UserEducationFinished;

class SaveFinishedEducationStaffAction
{

    public function executeStore(UserEducationFinishedDTO $data,User $user): User
    {
        $user->finished()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserEducationFinishedDTO $data,User $user,UserEducationFinished $model):User
    {
        $model->update($data->toArray());
        return $user;
    }

}
