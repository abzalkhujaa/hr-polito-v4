<?php


namespace App\Actions\Staff;


use App\DTO\User\UserEducationDTO;
use App\Models\User;
use App\Models\UserEducation;

class SaveEducationStaffAction
{

    public function executeStore(UserEducationDTO $data, User $user): User
    {
        $user->education()->create($data->toArray());
        return $user;
    }
    public function executeUpdate(UserEducationDTO $data, User $user): User
    {
        $user->education()->update($data->toArray());
        return $user;
    }

}
