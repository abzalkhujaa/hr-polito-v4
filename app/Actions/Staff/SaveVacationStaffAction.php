<?php


namespace App\Actions\Staff;

use App\DTO\User\UserVacationDTO;
use App\Models\User;
use App\Models\UserVacation;

class SaveVacationStaffAction
{

    public function executeStore(UserVacationDTO $data, User $user): User
    {
        $user->vacation()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserVacationDTO $data, User $user, UserVacation $model): User
    {
        $model->update($data->toArray());
        return $user;
    }

}
