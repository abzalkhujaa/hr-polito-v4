<?php


namespace App\Actions\Staff;


use App\DTO\User\UserFileDTO;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Storage;

class SaveFileStaffAction
{

    public function executeStore(UserFileDTO $data, User $user): User
    {

        $filenames = [];
        foreach ($data->toArray() as $key => $file){
            if (!is_null($file) and $file instanceof UploadedFile){
                $filenames[$key] = $file->store($this->getPath($user));
            }
        }
        $user->file()->create($filenames);
        return $user;
    }

    public function executeUpdate(UserFileDTO $data, User $user): User
    {
        $filenames = [];

        foreach ($data->toArray() as $key => $file){
            if (!is_null($file) and $file instanceof UploadedFile){
                $filenames[$key] = $file->store($this->getPath($user));
                Storage::delete($user->getAttribute($key));
            }
        }
        $user->file()->update($filenames);
        return $user;
    }

    private function getPath(User $user): string
    {
        return 'staff/'.$user->username;
    }

}
