<?php


namespace App\Actions\Staff;


use App\DTO\User\UserWorkDTO;
use App\Models\User;
use App\Models\UserWork;

class SaveWorkStaffAction
{

    public function executeStore(UserWorkDTO $data,User $user): User
    {
        $user->works()->create($data->toArray());
        return $user;
    }

    public function executeUpdate(UserWorkDTO $data, User $user, UserWork $model): User
    {
        $model->update($data->toArray());
        return $user;
    }

}
