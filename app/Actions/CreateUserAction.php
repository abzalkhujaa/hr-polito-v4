<?php


namespace App\Actions;


use App\DTO\User\UserDTO;
use App\Helpers\Enums\UserRoleEnum;
use App\Models\User;

class CreateUserAction extends Actions
{
    private CreateQrCodeAction $createQrCode;

    public function __construct(CreateQrCodeAction $createQrCode)
    {
        $this->createQrCode = $createQrCode;
    }

    public function execute(UserDTO $data) : User{
        $user = User::create($data->toArray());
        $user->syncRoles(UserRoleEnum::STAFF);
        $this->createQrCode->execute($user);
        return $user;
    }





}
