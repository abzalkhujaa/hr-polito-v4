<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class StaffSearchDataTransform extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $model
     * @return array
     */
    public function transform(User $model): array
    {
        return [
            'name' => $model->full_name,
            'url' => route('dashboard.staff.profile.index',['user' => $model->username]),
            'icon' => 'user'
        ];
    }
}
