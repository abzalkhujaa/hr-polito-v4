<?php


namespace App\DTO;


use App\Helpers\Traits\HasAlias;
use App\Http\Requests\Dashboard\Blog\VacancyRequest;
use App\Models\Vacancy;
use Spatie\DataTransferObject\DataTransferObject;
Use Str;

class VacancyDTO extends DataTransferObject
{
    use HasAlias;

    public string $title;
    public string $alias;
    public string $responsibility;
    public string $requirement;
    public string $content;
    public int $position_id;

    public int $created_by;
    public int $updated_by;

    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public static function fromRequest(VacancyRequest $request, string $type = self::CREATING): self
    {
        $data = [
            'title' => (string)Str::of($request->input('title'))->trim(),
            'alias' => self::createAlias([$request->input('title')],Vacancy::class),
            'responsibility' => (string)Str::of($request->input('responsibility'))->trim(),
            'requirement' => (string)Str::of($request->input('requirement'))->trim(),
            'content' => (string)Str::of($request->input('content'))->trim(),
            'position_id' => (int)(string)Str::of($request->input('position_id'))->trim(),
            'updated_by' => auth()->user()->id
        ];

        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id;
        elseif($type === self::UPDATING)
            $data['created_by'] = $request->vacancy->created_by;

        return new self($data);
    }

}
