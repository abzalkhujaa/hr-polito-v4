<?php


namespace App\DTO;


use App\Helpers\Traits\HasAlias;
use App\Http\Requests\Dashboard\Blog\DecreeRequest;
use App\Models\Decree;
use Str;
use Spatie\DataTransferObject\DataTransferObject;


class DecreeDTO extends DataTransferObject
{
    use HasAlias;

    public string $title;
    public string $alias;
    public string $content;
    public int $created_by;
    public int $updated_by;
    public string $image;
    public string $file;

    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public static function fromRequest(DecreeRequest $request,string $type = self::CREATING): self
    {
        $data = [
            'title' => (string)Str::of($request->input('title'))->trim(),
            'alias' => self::createAlias([$request->input('title')],Decree::class),
            'content' => (string)Str::of($request->input('content'))->trim(),
            'updated_by' => auth()->user()->id,
            'image' => $request->input('image'),
            'file' => $request->input('file')
        ];

        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id;
        elseif($type === self::UPDATING)
            $data['created_by'] = $request->decree->created_by;

        return new self($data);
    }
}
