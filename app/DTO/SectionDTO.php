<?php


namespace App\DTO;


use App\Helpers\Traits\HasAlias;
use App\Http\Requests\Dashboard\Group\SectionRequest;
use App\Models\Section;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class SectionDTO extends DataTransferObject
{
    use HasAlias;

    public ?string $title;
    public ?string $alias;

    public static function fromRequest(SectionRequest $request): self
    {
        return new self([
            'title' => (string)Str::of($request->input('title'))->trim(),
            'alias' => self::createAlias(array($request->input('title')),Section::class),
        ]);
    }
}
