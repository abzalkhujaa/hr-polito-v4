<?php


namespace App\DTO;


use App\Helpers\Traits\HasAlias;
use App\Http\Requests\Dashboard\Group\DepartmentRequest;
use App\Models\Department;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class DepartmentDTO extends DataTransferObject
{

    use HasAlias;

    public string $title;
    public string $alias;
    public int $section_id;

    public static function fromRequest(DepartmentRequest $request): self
    {
        return new self([
            'title' => (string)Str::of($request->input('title'))->trim(),
            'alias' => self::createAlias([$request->input('title')],Department::class),
            'section_id' => (int)$request->input('section_id')
        ]);
    }

}
