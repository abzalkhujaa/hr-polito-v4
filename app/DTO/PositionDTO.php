<?php

namespace App\DTO;

use App\Helpers\Traits\HasAlias;

use App\Http\Requests\Dashboard\Group\PositionRequest;
use App\Models\Position;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class PositionDTO extends DataTransferObject
{

    use HasAlias;

    public string $title;
    public string $alias;
    public int $department_id;

    public static function fromRequest(PositionRequest $request): self
    {
        return new self([
            'title' => (string)Str::of($request->input('title'))->trim(),
            'alias' => self::createAlias([$request->input('title')],Position::class),
            'department_id' => (int)$request->input('department_id')
        ]);
    }

}
