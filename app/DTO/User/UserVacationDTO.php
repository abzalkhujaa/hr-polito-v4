<?php


namespace App\DTO\User;

use App\Http\Requests\Dashboard\User\UserVacationRequest;
use Carbon\Carbon;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserVacationDTO extends DataTransferObject
{

    public string $type;
    public Carbon $start;
    public Carbon $finish;
    public int $duration;
    public ?string $additional;
    public ?string $period;


    public static function fromRequest(UserVacationRequest $request): self
    {
        return new self([
            'type' => (string)Str::of($request->input('type'))->trim(),
            'start' => Carbon::make($request->input('start')),
            'finish' => Carbon::make($request->input('finish')),
            'duration' => (int)$request->input('duration'),
            'additional' => (string)Str::of($request->input('additional'))->trim(),
            'period' => (string)Str::of($request->input('period'))->trim(),
        ]);
    }

}
