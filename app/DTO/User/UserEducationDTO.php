<?php


namespace App\DTO\User;

use App\Http\Requests\Dashboard\User\UserEducationRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserEducationDTO extends DataTransferObject
{

    public string $education;
    public string $specialty;
    public string $partying;
    public string $academic_degree;
    public string $academic_title;
    public string $foreign_lang;
    public string $military_rank;
    public string $state_award;
    public string $state_member;

    public static function fromRequest(UserEducationRequest $request): self
    {
        return new self([
            'education' => (string)Str::of($request->input('education'))->trim(),
            'specialty' => (string)Str::of($request->input('specialty'))->trim(),
            'partying' => (string)Str::of($request->input('partying'))->trim(),
            'academic_degree' => (string)Str::of($request->input('academic_degree'))->trim(),
            'academic_title' => (string)Str::of($request->input('academic_title'))->trim(),
            'foreign_lang' => (string)Str::of($request->input('foreign_lang'))->trim(),
            'military_rank' => (string)Str::of($request->input('military_rank'))->trim(),
            'state_award' => (string)Str::of($request->input('state_award'))->trim(),
            'state_member' => (string)Str::of($request->input('state_member'))->trim()
        ]);
    }


}
