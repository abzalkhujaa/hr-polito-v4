<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UserInformationRequest;
use Carbon\Carbon;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserInformationDTO extends DataTransferObject
{
    public string $passport_number;

    public string $b_day;

    public string $nationality;

    public string $city;

    public string $region;

    public string $district;

    public string $address;

    public string $phone;

    public string $home_phone;

    public static function fromRequest(UserInformationRequest $request): self
    {
        return new self([
            'passport_number' => (string)Str::of($request->input('passport_number'))->trim(),
            'b_day' => $request->input('b_day'),
            'nationality' => (string)Str::of($request->input('nationality'))->trim(),
            'city' => (string)Str::of($request->input('city'))->trim(),
            'region' => (string)Str::of($request->input('region'))->trim(),
            'district' => (string)Str::of($request->input('district'))->trim(),
            'address' => (string)Str::of($request->input('address'))->trim(),
            'phone' => (string)Str::of($request->input('phone'))->trim(),
            'home_phone' => (string)Str::of($request->input('home_phone'))->trim()
        ]);
    }


}
