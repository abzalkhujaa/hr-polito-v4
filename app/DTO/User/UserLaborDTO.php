<?php

namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UserLaborRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserLaborDTO extends DataTransferObject
{
    public int $section_id;
    public int $department_id;
    public int $position_id;
    public float $stavka;
    public string $basis_labor;
    public string $working_limit;
    public string $start_command;
    public string $finish_command;
    public string $last_vacation;
    public string $maternity;
    public string $disciplinary;
    public string $contract_number;
    public string $employment_num;
    public string $inn_number;
    public string $development;
    public string $pensioner;

    public static function fromRequest(UserLaborRequest $request): self
    {
        return new self([
            "section_id" => (int)$request->input('section_id'),
            "department_id" => (int)$request->input('department_id'),
            "position_id" =>(int)$request->input('position_id'),
            "stavka" => (float)$request->input('stavka'),
            "basis_labor" => (string)Str::of($request->input('basis_labor'))->trim(),
            "working_limit" => (string)Str::of($request->input('working_limit'))->trim(),
            "start_command" => (string)Str::of($request->input('start_command'))->trim(),
            "finish_command" => (string)Str::of($request->input('finish_command'))->trim(),
            "last_vacation" => (string)Str::of($request->input('last_vacation'))->trim(),
            "maternity" => (string)Str::of($request->input('maternity'))->trim(),
            "disciplinary" => (string)Str::of($request->input('disciplinary'))->trim(),
            "contract_number" => (string)Str::of($request->input('contract_number'))->trim(),
            "employment_num" => (string)Str::of($request->input('employment_num'))->trim(),
            "inn_number" => (string)Str::of($request->input('inn_number'))->trim(),
            "pensioner" => (string)Str::of($request->input('pensioner'))->trim(),
            "development" => (string)Str::of($request->input('development'))->trim(),
        ]);
    }
}
