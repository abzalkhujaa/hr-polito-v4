<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UserFamilyRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserFamilyDTO extends DataTransferObject
{
    public string $family_member;
    public string $first_name;
    public string $last_name;
    public ?string $patronymic;
    public string $b_day;
    public string $city;
    public string $work;
    public string $current_place;
    public bool $status;

    public static function fromRequest(UserFamilyRequest $request): self
    {
        return new self([
            'family_member' => (string)Str::of($request->input('family_member'))->trim(),
            'first_name' => (string)Str::of($request->input('first_name'))->trim(),
            'last_name' => (string)Str::of($request->input('last_name'))->trim(),
            'patronymic' => (string)Str::of($request->input('patronymic'))->trim(),
            'b_day' => (string)Str::of($request->input('b_day'))->trim(),
            'city' => (string)Str::of($request->input('city'))->trim(),
            'work' => (string)Str::of($request->input('work'))->trim(),
            'current_place' => (string)Str::of($request->input('current_place'))->trim(),
            'status' => $request->input('status') ?? false,
        ]);
    }
}
