<?php


namespace App\DTO\User;

use App\Http\Requests\Dashboard\User\UserWorkRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserWorkDTO extends DataTransferObject
{
    public string $start;
    public string $finish;
    public string $place;

    public static function fromRequest(UserWorkRequest $request): self
    {
        return new self([
            'start' => (string)Str::of($request->input('start'))->trim(),
            'finish' => (string)Str::of($request->input('finish'))->trim(),
            'place' => (string)Str::of($request->input('place'))->trim()
        ]);
    }

}
