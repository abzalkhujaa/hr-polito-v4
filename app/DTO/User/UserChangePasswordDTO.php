<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\ChangePasswordUserRequest;
use Spatie\DataTransferObject\DataTransferObject;

class UserChangePasswordDTO extends DataTransferObject
{

    public string $password;

    public static function fromRequest(ChangePasswordUserRequest $request): self
    {
        return new self([
            "password" => (string)\Str::of($request->input('password'))->trim()
        ]);
    }
}
