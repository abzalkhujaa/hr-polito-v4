<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UserFileRequest;
use Illuminate\Http\UploadedFile;
use Spatie\DataTransferObject\DataTransferObject;

class UserFileDTO extends DataTransferObject
{

    public ?UploadedFile $image;
    public ?UploadedFile $diploma;
    public ?UploadedFile $inn;
    public ?UploadedFile $inps;
    public ?UploadedFile $military;
    public ?UploadedFile $passport;

    public static function fromRequest(UserFileRequest $request): self
    {
        return new self([
            'image' => ($request->hasFile('image')) ? $request->file('image') : null,
            'diploma' => ($request->hasFile('diploma')) ? $request->file('diploma') : null,
            'inn' => ($request->hasFile('inn')) ? $request->file('inn') : null,
            'inps' => ($request->hasFile('inps')) ? $request->file('inps') : null,
            'military' => ($request->hasFile('military')) ? $request->file('military') : null,
            'passport' => ($request->hasFile('passport')) ? $request->file('passport') : null,
        ]);
    }

}
