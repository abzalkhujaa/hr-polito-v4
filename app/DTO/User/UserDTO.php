<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Http\Requests\Dashboard\User\UserRequest;
use Hash;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserDTO extends DataTransferObject
{

    public string $username;
    public string $email;
    public string $password;
    public string $first_name;
    public string $last_name;
    public string $patronymic;
    public string $gender;

    /**
     * @param UserRequest|UpdateUserRequest $request
     * @return static
     */
    public static function fromRequest($request): self
    {
        return new self([
            'username' => (string)Str::of($request->input('username'))->lower()->trim(),
            'email' => (string)Str::of( $request->input('email'))->trim(),
            'password' => Hash::make(Str::of($request->input('password'))->trim()),
            'first_name' => (string)Str::of($request->input('first_name'))->trim(),
            'last_name' => (string)Str::of($request->input('last_name'))->trim(),
            'patronymic' => (string)Str::of($request->input('patronymic'))->trim(),
            'gender' => (string)Str::of($request->input('gender'))->upper()
        ]);
    }

}
