<?php


namespace App\DTO\User;


use App\Http\Requests\Dashboard\User\UserFinishedRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class UserEducationFinishedDTO extends DataTransferObject
{

    public string $year;
    public string $place;

    public static function fromRequest(UserFinishedRequest $request): self
    {
        return new self([
            'year' => (string)Str::of($request->input('year'))->trim(),
            'place' => (string)Str::of($request->input('place'))->trim()
        ]);
    }

}
