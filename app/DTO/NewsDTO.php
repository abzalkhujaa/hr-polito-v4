<?php


namespace App\DTO;


use App\Helpers\Traits\HasAlias;
use App\Http\Requests\Dashboard\Blog\NewsRequest;
use App\Models\News;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class NewsDTO extends DataTransferObject
{
    use HasAlias;
    public string $title;
    public string $description;
    public string $alias;
    public string $content;
    public int $created_by;
    public int $updated_by;
    public string $image;
    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public static function fromRequest(NewsRequest $request,string $type = self::CREATING): self
    {
        $data = [
            'title' => (string)Str::of($request->input('title'))->trim(),
            'description' => (string)Str::of($request->input('description'))->trim(),
            'alias' => self::createAlias([$request->input('title')],News::class),
            'content' => (string)Str::of($request->input('content'))->trim(),
            'updated_by' => auth()->user()->id,
            'image' => $request->input('image')
        ];

        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id;
        elseif($type === self::UPDATING)
            $data['created_by'] = $request->news->created_by;

        return new self($data);
    }


}
