<?php

namespace App\Models;

use App\Actions\CreateQrCodeAction;
use App\Helpers\Enums\UserRoleEnum;
use App\Helpers\Traits\HasStaffPaginationIcon;
use App\Helpers\Traits\HasUserDataChecker;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $patronymic
 * @property string $gender
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property bool $reset_password
 * @property-read \App\Models\UserEducation|null $education
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserFamily[] $family
 * @property-read int|null $family_count
 * @property-read \App\Models\UserFile|null $file
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserEducationFinished[] $finished
 * @property-read int|null $finished_count
 * @property-read string $avatar
 * @property-read string $full_name
 * @property-read bool $has_certificate
 * @property-read string|null $qr_code
 * @property-read string $short_name
 * @property-read \App\Models\UserInformation|null $information
 * @property-read \App\Models\UserLabor|null $labor
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserVacation[] $vacation
 * @property-read int|null $vacation_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserWork[] $works
 * @property-read int|null $works_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereResetPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail
{
  use Notifiable,HasRoles,SoftDeletes,HasUserDataChecker,HasStaffPaginationIcon;

  public const ADMIN = 'admin';
  public const STAFF = 'staff';
  public const USER = 'user';
  public const VACANT = 'vacant';
  public const MODERATOR = 'moderator';

  protected $fillable = [
    'username', 'email', 'password','first_name','last_name','patronymic','gender'
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];

  protected $casts = [
    'email_verified_at' => 'datetime'
  ];

  public function information(): HasOne
  {
      return $this->hasOne(UserInformation::class);
  }

  public function education() : HasOne
  {
      return $this->hasOne(UserEducation::class);
  }

  public function finished() : HasMany
  {
      return $this->hasMany(UserEducationFinished::class);
  }

  public function labor(): HasOne
  {
      return $this->hasOne(UserLabor::class);
  }

  public function works() : HasMany
  {
      return $this->hasMany(UserWork::class);
  }

  public function family() : HasMany
  {
      return $this->hasMany(UserFamily::class);
  }

  public function file() : HasOne
  {
      return $this->hasOne(UserFile::class);
  }

  public function vacation(): HasMany
  {
      return $this->hasMany(UserVacation::class);
  }

  public function getFullNameAttribute(): string
  {
      return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'] . ' ' . $this->attributes['patronymic'];
  }

  public function getShortNameAttribute(): string
  {
      if (!is_null($this->first_name) and !is_null($this->last_name)){
          return $this->first_name[0] . $this->last_name[0];
      }
      return 'No';
  }

  public function getAvatarAttribute(): string
  {
      return ($this->file and $this->file->image) ?  asset('storage/'.$this->file->image) : asset('images/no_user_photo.png');
  }

  public function getQrCodeAttribute(): ?string
  {
      if ($this->hasQRCode()) return asset('storage/qrcode/'.$this->username.'.png');
      else{
          (new CreateQrCodeAction)->execute($this);
          return asset('storage/qrcode/'.$this->username.'.png');
      }
  }

  public function getHasCertificateAttribute(): bool
  {
      if (!$this->information)
          return false;
      elseif(!$this->education)
        return false;
      elseif (!$this->labor)
          return false;
      elseif (!$this->works)
          return false;
      elseif (!$this->family)
          return false;
      elseif (!$this->file)
          return false;
      elseif(!$this->file and !$this->file->image)
          return false;
      else
          return true;
  }

    public function role_icon(): string
    {
        if ($this->hasRole(UserRoleEnum::SUPER)) return "<i class='feather icon-star text-primary'></i>";
        if ($this->hasRole(UserRoleEnum::ADMIN)) return "<i class='feather icon-shield text-danger'></i>";
        if ($this->hasRole(UserRoleEnum::STAFF)) return "<i class='feather icon-zap text-primary'></i>";
        if ($this->hasRole(UserRoleEnum::MODERATOR)) return "<i class='feather icon-pen-tool text-info'></i>";
        if ($this->hasRole(UserRoleEnum::USER)) return "<i class='feather icon-user text-warning'></i>";
        return "<i class='feather icon-x-octagon text-primary'></i>";
    }

    public function birthday_left(): int
    {
        if ($this->information && $this->information->b_day){
            $date = now()->diffInHours(Carbon::make($this->information->b_day)->year(date('Y')),false);
            return ($date < 0) ? now()->diffInHours(Carbon::make($this->information->b_day)->year(date('Y')+1),false) : $date;
        }
        return 0;
    }

}
