<?php

namespace App\Models;

use App\Helpers\Traits\HasCreator;
use App\Helpers\Traits\HasUpdater;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * App\Models\vacancy
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string|null $responsibility
 * @property string $requirement
 * @property string|null $skill
 * @property string|null $personal_quality
 * @property string|null $content
 * @property int $position_id
 * @property int $created_by
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $creator
 * @property-read \App\Models\Position $position
 * @property-read \App\Models\User $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newQuery()
 * @method static \Illuminate\Database\Query\Builder|Vacancy onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy wherePersonalQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereRequirement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereResponsibility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereSkill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|Vacancy withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Vacancy withoutTrashed()
 * @mixin \Eloquent
 */
class Vacancy extends Model
{
    use HasCreator,HasUpdater;

    protected $fillable = ['title','alias','content','responsibility','requirement','skill','personal_quality','position_id','created_by','updated_by'];


    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }

    public static function getBySection(int $section_id): Collection
    {
        $vacancies = self::all();
        $collection = collect();

        foreach ($vacancies as $vacancy){
            if ($vacancy->position->department->section->id === $section_id)
                $collection->push($vacancy);
        }
        return $collection;
    }

    public function application(): HasMany
    {
        return $this->hasMany(VacancyApplication::class);
    }
}
