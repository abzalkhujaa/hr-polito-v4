<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserFamily
 *
 * @property int $id
 * @property string $family_member
 * @property string $first_name
 * @property string $last_name
 * @property string|null $patronymic
 * @property string $b_day
 * @property string $city
 * @property string $work
 * @property string $current_place
 * @property bool $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereBDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereCurrentPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereFamilyMember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFamily whereWork($value)
 * @mixin \Eloquent
 */
class UserFamily extends Model
{
    use HasUser;

    protected $fillable = [
        'family_member','first_name','last_name','patronymic','b_day','work','current_place','status','user_id','city'
    ];
}
