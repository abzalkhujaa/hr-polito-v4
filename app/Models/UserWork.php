<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserWork
 *
 * @property int $id
 * @property string $start
 * @property string $finish
 * @property string $place
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereFinish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWork whereUserId($value)
 * @mixin \Eloquent
 */
class UserWork extends Model
{
    use HasUser;

    protected $fillable = [
        'start','finish','place','user_id'
    ];

}
