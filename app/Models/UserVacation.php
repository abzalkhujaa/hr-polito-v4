<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserVacation
 *
 * @property int $id
 * @property string $type
 * @property string $start
 * @property string $finish
 * @property int $duration
 * @property string|null $period
 * @property string|null $additional
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereAdditional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereFinish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVacation whereUserId($value)
 * @mixin \Eloquent
 */
class UserVacation extends Model
{
    use HasUser;

    protected $fillable = [
        'type',
        'start',
        'finish',
        'duration',
        'period',
        'additional'
    ];

    protected $casts = [
        'start' => 'date',
        'finish' => 'date'
    ];
}
