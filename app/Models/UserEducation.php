<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserEducation
 *
 * @property int $id
 * @property string $education
 * @property string $specialty
 * @property string|null $partying
 * @property string|null $academic_degree
 * @property string|null $academic_title
 * @property string $foreign_lang
 * @property string|null $military_rank
 * @property string|null $state_award
 * @property string|null $state_member
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereAcademicDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereAcademicTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereForeignLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereMilitaryRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation wherePartying($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereSpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereStateAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereStateMember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducation whereUserId($value)
 * @mixin \Eloquent
 */
class UserEducation extends Model
{
    use HasUser;

    protected $fillable = [
        'education','specialty','partying','academic_title','academic_degree','foreign_lang','military_rank','state_member','state_award','user_id'
    ];
}
