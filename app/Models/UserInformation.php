<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserInformation
 *
 * @property int $id
 * @property string $passport_number
 * @property string $b_day
 * @property string $city
 * @property string $nationality
 * @property string $region
 * @property string $district
 * @property string $address
 * @property string $phone
 * @property string $home_phone
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereBDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereHomePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInformation whereUserId($value)
 * @mixin \Eloquent
 */
class UserInformation extends Model
{
    use HasUser;

    protected $fillable = [
        'passport_number','b_day','city','nationality','region','district','address','phone','home_phone','user_id'
    ];

    public function getBirthDay(){
        return explode('-',$this->b_day);
    }

}
