<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserLabor
 *
 * @property int $id
 * @property int $section_id
 * @property int $department_id
 * @property int $position_id
 * @property string $stavka
 * @property string $basis_labor
 * @property string $working_limit
 * @property string $start_command
 * @property string $finish_command
 * @property string|null $last_vacation
 * @property string|null $maternity
 * @property string|null $pensioner
 * @property string|null $disciplinary
 * @property string $contract_number
 * @property string $employment_num
 * @property string|null $development
 * @property string $inn_number
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Department $department
 * @property-read \App\Models\Position $position
 * @property-read \App\Models\Section $section
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereBasisLabor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereContractNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereDevelopment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereDisciplinary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereEmploymentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereFinishCommand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereInnNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereLastVacation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereMaternity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor wherePensioner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereStartCommand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereStavka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLabor whereWorkingLimit($value)
 * @mixin \Eloquent
 */
class UserLabor extends Model
{
    use HasUser;

    protected $fillable = [
        'section_id',
        'department_id',
        'position_id',
        'stavka',
        'basis_labor',
        'working_limit',
        'start_command',
        'finish_command',
        'last_vacation',
        'maternity',
        'pensioner',
        'disciplinary',
        'contract_number',
        'employment_num',
        'development',
        'inn_number',
        'user_id'
    ];


    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }
}
