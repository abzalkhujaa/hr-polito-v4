<?php

namespace App\Models;

use App\Helpers\Traits\HasCreator;
use App\Helpers\Traits\HasUpdater;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Decree
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $content
 * @property string|null $image
 * @property string|null $file
 * @property int $created_by
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User $creator
 * @property-read \App\Models\User $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Decree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Decree newQuery()
 * @method static \Illuminate\Database\Query\Builder|Decree onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Decree query()
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Decree whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|Decree withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Decree withoutTrashed()
 * @mixin \Eloquent
 */
class Decree extends Model
{
   use HasCreator,HasUpdater,SoftDeletes;

   protected $fillable = ['title','alias','content','image','file','created_by','updated_by'];

}
