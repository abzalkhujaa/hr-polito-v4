<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserEducationFinished
 *
 * @property int $id
 * @property string $year
 * @property string $place
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEducationFinished whereYear($value)
 * @mixin \Eloquent
 */
class UserEducationFinished extends Model
{
    use HasUser;

    protected $fillable = [
        'year','place','user_id'
    ];
}
