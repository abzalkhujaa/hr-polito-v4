<?php

namespace App\Models;

use App\Helpers\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserFile
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $diploma
 * @property string|null $inn
 * @property string|null $inps
 * @property string|null $military
 * @property string|null $passport
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereDiploma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereInps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereMilitary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile wherePassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFile whereUserId($value)
 * @mixin \Eloquent
 */
class UserFile extends Model
{
    use HasUser;

    protected $fillable = [
        'image','diploma','inn','inps','military','passport','user_id'
    ];
}
