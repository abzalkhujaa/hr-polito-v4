<?php

namespace App\Http\Controllers\StaffDashboard;

use App\Actions\CreateStaffCertificateAction;
use App\Actions\CreateStaffReferenceAction;
use App\Models\User;

use Mnvx\EloquentPrintForm\PrintFormException;


class StaffController extends BaseStaffController
{

    public function reference(CreateStaffReferenceAction $createStaffReferenceAction)
    {
        try {
            return $createStaffReferenceAction->execute(auth()->user());
        } catch (PrintFormException $e) {
           return back();
        }

    }

    public function certificate(CreateStaffCertificateAction $createStaffCertificateAction){
        try {
            return $createStaffCertificateAction->execute(auth()->user());
        } catch (PrintFormException $e) {
            return back();
        }
    }
}
