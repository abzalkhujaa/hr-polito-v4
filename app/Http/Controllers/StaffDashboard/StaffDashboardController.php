<?php


namespace App\Http\Controllers\StaffDashboard;


class StaffDashboardController extends BaseStaffController
{

    public function index(){
        return view('staff-dashboard.page.main.index');
    }

}
