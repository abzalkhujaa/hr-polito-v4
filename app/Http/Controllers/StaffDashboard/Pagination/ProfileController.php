<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;

use App\Actions\ChangePasswordAction;
use App\Actions\UpdateUserAction;
use App\DTO\User\UserChangePasswordDTO;
use App\DTO\User\UserDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\ChangePasswordUserRequest;
use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;

class ProfileController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.profile')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.profile',compact('breadcrumbs','user'));
    }

    public function update(UpdateUserRequest $request,UpdateUserAction $updateUserAction): RedirectResponse
    {
        try {
            $user = $updateUserAction->execute(UserDTO::fromRequest($request),auth()->user());
            alert()->success(__('dashboard.profile_field.title'),__('dashboard.action.updated'));
            return redirect()->route('staff-dashboard.staff.profile.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.profile_field.title'),__('dashboard.action.not_updated'));
            return back()->withInput($request->all());
        }
    }

    public function changePassword(ChangePasswordUserRequest $request,ChangePasswordAction $changePasswordAction): RedirectResponse
    {
        try {
            $user = $changePasswordAction->execute(UserChangePasswordDTO::fromRequest($request),auth()->user());
            alert()->success(__('dashboard.action.password.changed'));
            return redirect()->route('staff-dashboard.staff.profile.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.action.not_updated'));
            return back()->withInput($request->all());
        }
    }

}
