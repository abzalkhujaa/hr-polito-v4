<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;


use App\Actions\Staff\SaveFamilyStaffAction;
use App\DTO\User\UserFamilyDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFamilyRequest;
use App\Models\User;
use App\Models\UserFamily;
use Exception;
use Illuminate\Http\RedirectResponse;

class FamilyController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.family_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.family',compact('breadcrumbs','user'));
    }

    public function store(UserFamilyRequest $request,SaveFamilyStaffAction $saveFamilyStaffAction): RedirectResponse
    {
        try {
            $user = $saveFamilyStaffAction->executeStore(UserFamilyDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('staff-dashboard.staff.family.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }

    public function update(UserFamilyRequest $request, UserFamily $family,SaveFamilyStaffAction $saveFamilyStaffAction): RedirectResponse
    {
        try {
            $user = $saveFamilyStaffAction->executeUpdate(UserFamilyDTO::fromRequest($request),auth()->user(),$family);
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('dashboard.staff.family.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }

    public function destroy(UserFamily $family)
    {
        try {
            $family->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.family.deleted')]);
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('dashboard.staff.family.index');
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }
}
