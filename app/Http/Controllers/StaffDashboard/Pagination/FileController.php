<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;

use App\Actions\Staff\SaveFileStaffAction;
use App\DTO\User\UserFileDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFileRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;

class FileController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.file_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.file',compact('breadcrumbs','user'));
    }

    public function store(UserFileRequest $request, SaveFileStaffAction $saveFileStaffAction): RedirectResponse
    {
        try {
            $user = $saveFileStaffAction->executeStore(UserFileDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.file.success'));
            return redirect()->route('staff-dashboard.staff.file.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.file.error'));
            return back()->withInput($request->all());
        }
    }

    public function update(UserFileRequest $request, SaveFileStaffAction $saveFileStaffAction): RedirectResponse
    {
        try {
            $user = $saveFileStaffAction->executeUpdate(UserFileDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.file.success'));
            return redirect()->route('staff-dashboard.staff.file.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.file.error'));
            return back()->withInput($request->all());
        }
    }
}
