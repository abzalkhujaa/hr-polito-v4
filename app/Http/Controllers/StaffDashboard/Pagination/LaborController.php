<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;

use App\Actions\Staff\SaveLaborStaffAction;
use App\DTO\User\UserLaborDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserLaborRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;

class LaborController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.labor_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.labor',compact('breadcrumbs','user'));
    }

    public function store(UserLaborRequest $request,SaveLaborStaffAction $saveLaborStaffAction): RedirectResponse
    {
        try {
            $user = $saveLaborStaffAction->executeStore(UserLaborDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.labor.success'));
            return redirect()->route('staff-dashboard.staff.labor.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.labor.error'));
            return back()->withInput($request->all());
        }
    }

    public function update(UserLaborRequest $request, SaveLaborStaffAction $saveLaborStaffAction): RedirectResponse
    {
        try {
            $user = $saveLaborStaffAction->executeUpdate(UserLaborDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.labor.success'));
            return redirect()->route('staff-dashboard.staff.labor.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.labor.error'));
            return back()->withInput($request->all());
        }
    }
}
