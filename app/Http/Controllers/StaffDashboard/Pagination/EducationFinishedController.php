<?php

namespace App\Http\Controllers\StaffDashboard\Pagination;


use App\Actions\Staff\SaveFinishedEducationStaffAction;
use App\DTO\User\UserEducationFinishedDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFinishedRequest;
use App\Models\User;
use App\Models\UserEducationFinished;
use Exception;
use Illuminate\Http\RedirectResponse;

class EducationFinishedController extends BaseController
{

    public function store(UserFinishedRequest $request, SaveFinishedEducationStaffAction $saveFinishedEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveFinishedEducationStaffAction->executeStore(UserEducationFinishedDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('staff-dashboard.staff.education.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

    public function update(UserFinishedRequest $request,UserEducationFinished $finished,SaveFinishedEducationStaffAction $saveFinishedEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveFinishedEducationStaffAction->executeUpdate(UserEducationFinishedDTO::fromRequest($request),auth()->user(),$finished);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('staff-dashboard.staff.education.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

    public function destroy(UserEducationFinished $finished)
    {
        try {
            $finished->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.education.deleted')]);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('staff-dashboard.staff.education.index');
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

}
