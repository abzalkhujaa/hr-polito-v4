<?php

namespace App\Http\Controllers\StaffDashboard\Pagination;


use App\Actions\Staff\SaveVacationStaffAction;
use App\DTO\User\UserVacationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserVacationRequest;
use App\Models\User;
use App\Models\UserVacation;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class VacationController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.vacation_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.vacation',compact('breadcrumbs','user'));
    }

}
