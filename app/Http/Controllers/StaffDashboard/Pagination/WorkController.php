<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;

use App\Actions\Staff\SaveWorkStaffAction;
use App\DTO\User\UserWorkDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserWorkRequest;
use App\Models\User;
use App\Models\UserWork;
use Exception;

use Illuminate\Http\RedirectResponse;

class WorkController extends BaseController
{
    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.work_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.work',compact('user','breadcrumbs'));
    }

    public function store(UserWorkRequest $request, SaveWorkStaffAction $saveWorkStaffAction): RedirectResponse
    {
        try {
            $user = $saveWorkStaffAction->executeStore(UserWorkDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.work.success'));
            return redirect()->route('staff-dashboard.staff.work.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.work.error'));
            return back();
        }
    }

    public function update(UserWorkRequest $request, UserWork $work, SaveWorkStaffAction $saveWorkStaffAction): RedirectResponse
    {
        try {
            $user = $saveWorkStaffAction->executeUpdate(UserWorkDTO::fromRequest($request),auth()->user(),$work);
            alert()->success(__('pages.staff.work.success'));
            return redirect()->route('staff-dashboard.staff.work.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.work.error'));
            return back();
        }
    }

    public function destroy(UserWork $work)
    {
        try {
            $work->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.work.deleted')]);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('staff-dashboard.staff.work.index');
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }
}
