<?php

namespace App\Http\Controllers\StaffDashboard\Pagination;


use App\Actions\Staff\SaveEducationStaffAction;
use App\DTO\User\UserEducationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserEducationRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;

class EducationController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.education_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.education',compact('breadcrumbs','user'));
    }

    public function store(UserEducationRequest $request,SaveEducationStaffAction $saveEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveEducationStaffAction->executeStore(UserEducationDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.education.success'));
            return redirect()->route('staff-dashboard.staff.education.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.education.error'));
            return back()->withInput($request->all());
        }
    }

    public function update(UserEducationRequest $request, User $user, SaveEducationStaffAction $saveEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveEducationStaffAction->executeUpdate(UserEducationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.education.success'));
            return redirect()->route('staff-dashboard.staff.education.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.education.error'));
            return back()->withInput($request->all());
        }
    }

}
