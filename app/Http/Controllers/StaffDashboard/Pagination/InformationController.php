<?php


namespace App\Http\Controllers\StaffDashboard\Pagination;


use App\Actions\Staff\SaveInformationStaffAction;
use App\DTO\User\UserInformationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserInformationRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;

class InformationController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('staff-dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('dashboard.staff')],
            ['name' => __('dashboard.info_field.title')]
        ];
        $user = auth()->user();
        return view('staff-dashboard.page.pagination.information',compact('breadcrumbs','user'));
    }

    public function store(UserInformationRequest $request,SaveInformationStaffAction $saveInformationStaffAction): RedirectResponse
    {
        try {
            $user =$saveInformationStaffAction->executeStore(UserInformationDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.information.success'));
            return redirect()->route('staff-dashboard.staff.information.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.information.error'));
            return back();
        }
    }

    public function update(UserInformationRequest $request,SaveInformationStaffAction $saveInformationStaffAction): RedirectResponse
    {
        try {
            $user  = $saveInformationStaffAction->executeUpdate(UserInformationDTO::fromRequest($request),auth()->user());
            alert()->success(__('pages.staff.information.success'));
            return redirect()->route('staff-dashboard.staff.information.index');
        }catch (Exception $exception){
            alert()->error(__('pages.staff.information.error'));
            return back();
        }
    }

}
