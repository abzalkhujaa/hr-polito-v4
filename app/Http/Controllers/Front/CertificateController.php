<?php


namespace App\Http\Controllers\Front;


use App\Actions\CreateStaffCertificateAction;
use App\Http\Requests\CertificateSearchRequest;
use App\Models\User;
use App\Models\UserInformation;
use Mnvx\EloquentPrintForm\PrintFormException;

class CertificateController extends BaseFrontController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.certificate.index')],
            ['name' => "Получить Справку"]
        ];
        return view('front.page.certificate.index',compact('breadcrumbs'));
    }

    public function check(User $user){
        return view('front.page.certificate.check',compact('user'));
    }

    public function search(CertificateSearchRequest $request)
    {
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.certificate.index')],
            ['name' => "Получить Справку"]
        ];

        $passport = $request->input('passport_series').$request->input('passport_number');
        $user = UserInformation::query()->where('passport_number',$passport)->where('b_day',$request->input('b_day'))->first();
        if ($user){
            $user = $user->user;
            return view('front.page.certificate.index',compact('breadcrumbs','user'));
        }
        return view('front.page.certificate.index',compact('breadcrumbs'));
    }

    public function download(User $user, CreateStaffCertificateAction $createStaffCertificateAction){
        try {
            return $createStaffCertificateAction->execute($user);
        } catch (PrintFormException $e) {
            return back();
        }
    }

}
