<?php


namespace App\Http\Controllers\Front;


use App\Models\News;

class NewsController extends BaseFrontController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.blog.news.index')],
        ];
        $news = News::latest()->get();
        return view('front.page.news.index',compact('breadcrumbs','news'));
    }

    public function show(News $news){
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link'=> route('home.news.index'),'name' => __('page.dashboard.blog.news.index')],
            ['name' => \Str::of($news->title)->limit()],
        ];
        return view('front.page.news.show',compact('breadcrumbs','news'));
    }

}
