<?php


namespace App\Http\Controllers\Front;


use App\Models\Decree;

class DecreeController extends BaseFrontController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.blog.decree.index')],
        ];
        $decrees = Decree::latest()->get();
        return view('front.page.decree.index',compact('breadcrumbs','decrees'));
    }

    public function show(Decree $decree){
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('home.decree.index'), 'name' => __('page.dashboard.blog.decree.index')],
            ['name' => \Str::of($decree->alias)->limit()],
        ];
        return view('front.page.decree.show',compact('breadcrumbs','decree'));
    }

}
