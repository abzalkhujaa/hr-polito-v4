<?php


namespace App\Http\Controllers\Front;


use App\Http\Requests\VacancyApplicationRequest;
use App\Models\Section;
use App\Models\Vacancy;

class VacancyController extends BaseFrontController
{

    public function index(){
        $sections = Section::all()->take(3);
        return view('front.page.vacancy.index',compact('sections'));
    }

    public function group(Section $section){
        $vacancies = Vacancy::getBySection($section->id);
        return view('front.page.vacancy.group',compact('vacancies','section'));
    }

    public function show($section,Vacancy $vacancy){
        $section = Section::where('alias',$section)->first();
        $breadcrumbs = [
            ['link' => route('home.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link'=> route('home.vacancy.index'),'name' => __('page.dashboard.blog.vacancy.index')],
            ['link'=> route('home.vacancy.group',['section' => $section->alias]),'name' => $section->title],
            ['name' => \Str::of($vacancy->title)->limit()],
        ];

        return view('front.page.vacancy.show',compact('breadcrumbs','section','vacancy'));
    }

    public function application(VacancyApplicationRequest $request){

    }

}
