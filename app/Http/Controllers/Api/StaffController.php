<?php


namespace App\Http\Controllers\Api;


use App\Helpers\Enums\UserRoleEnum;
use App\Models\User;
use App\Transformers\StaffSearchDataTransform;
use Illuminate\Http\JsonResponse;

class StaffController extends ApiController
{

    public function searchData(): JsonResponse
    {
        $query = User::role([UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF,UserRoleEnum::USER])->get();
        $data['listItems'] = fractal()->collection($query)->transformWith(new StaffSearchDataTransform)->toArray();

        return response()->json($data,200);
    }

}
