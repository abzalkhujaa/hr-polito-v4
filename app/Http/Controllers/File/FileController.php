<?php


namespace App\Http\Controllers\File;


use App\Actions\File\DeleteUploadedFileAction;
use App\Actions\File\UploadFileAction;
use App\Http\Requests\FileDeleteRequest;
use App\Http\Requests\FileUploadRequest;
use Exception;
use Illuminate\Http\JsonResponse;

class FileController
{

    public function upload(FileUploadRequest $request,UploadFileAction $uploadFileAction): JsonResponse
    {
        try {
            return response()->json([
                'status' => true,
                'file' => $uploadFileAction->execute($request)
            ],200);
        }catch (Exception $exception){
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }

    public function delete(FileDeleteRequest $request,DeleteUploadedFileAction $deleteUploadedFileAction): JsonResponse
    {
        try {
            return response()->json([
                'status' => true,
                'file' => $deleteUploadedFileAction->execute($request)
            ],200);
        }catch (Exception $exception){
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }

}
