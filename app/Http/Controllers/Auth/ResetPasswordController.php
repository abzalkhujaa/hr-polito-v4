<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Traits\HasAuthRedirectPath;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords,HasAuthRedirectPath;

    protected string $redirectTo = '/login';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->reset_password = true;
    }

    public function redirectTo(): string
    {
        return $this->authRedirectPath();
    }
}
