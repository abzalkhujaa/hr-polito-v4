<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\Staff\SaveInformationStaffAction;
use App\DTO\User\UserInformationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserInformationRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class InformationController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.info_field.title')]
        ];
        $data = $user->information;
        return view('dashboard.page.staff.information',compact('breadcrumbs','user','data'));
    }

    public function store(UserInformationRequest $request,User $user,SaveInformationStaffAction $saveInformationStaffAction): RedirectResponse
    {
        try {
            $user =$saveInformationStaffAction->executeStore(UserInformationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.information.success'));
            return redirect()->route('dashboard.staff.information.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.information.error'));
            return back();
        }
    }

    public function update(UserInformationRequest $request, User $user,SaveInformationStaffAction $saveInformationStaffAction): RedirectResponse
    {
        try {
            $user  = $saveInformationStaffAction->executeUpdate(UserInformationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.information.success'));
            return redirect()->route('dashboard.staff.information.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.information.error'));
            return back();
        }
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->information->forceDelete();
            return response()->json(['message' => __('message.deleted')]);
        }catch (Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }

}
