<?php


namespace App\Http\Controllers\Dashboard\Staff;

use App\Actions\Staff\SaveLaborStaffAction;
use App\DTO\User\UserLaborDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserLaborRequest;
use App\Models\Section;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class LaborController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.labor_field.title')]
        ];
        $data = $user->labor;
        $sections = Section::all();
        return view('dashboard.page.staff.labor',compact('breadcrumbs','user','data','sections'));
    }

    public function store(UserLaborRequest $request,User $user, SaveLaborStaffAction $saveLaborStaffAction): RedirectResponse
    {
        try {
            $user = $saveLaborStaffAction->executeStore(UserLaborDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.labor.success'));
            return redirect()->route('dashboard.staff.labor.index',['user' => $user->username])->with('success',__('pages.staff.labor.added'));
        }catch (Exception $exception){
            alert()->error(__('pages.staff.labor.error'));
            return back()->with('error',__('pages.staff.labor.not.added'))->withInput($request->all());
        }
    }

    public function update(UserLaborRequest $request, User $user, SaveLaborStaffAction $saveLaborStaffAction): RedirectResponse
    {
        try {
            $user = $saveLaborStaffAction->executeUpdate(UserLaborDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.labor.success'));
            return redirect()->route('dashboard.staff.labor.index',['user' => $user->username])->with('success',__('pages.staff.labor.updated'));
        }catch (Exception $exception){
            alert()->error(__('pages.staff.labor.error'));
            return back()->with('error',__('pages.staff.labor.not.updated'))->withInput($request->all());
        }
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->labor->forceDelete();
            return response()->json(['message' => __('message.staff.labor.deleted')]);
        }catch (Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }
}
