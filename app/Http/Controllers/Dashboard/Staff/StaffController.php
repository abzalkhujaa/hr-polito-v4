<?php

namespace App\Http\Controllers\Dashboard\Staff;

use App\Actions\CreateStaffCertificateAction;
use App\Actions\CreateStaffReferenceAction;
use App\Exports\StaffExport;
use App\Helpers\Enums\UserRoleEnum;
use App\Http\Controllers\Dashboard\BaseController;
use App\Models\User;
use Exception;
use Log;
use Mnvx\EloquentPrintForm\PrintFormException;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class StaffController extends BaseController
{
    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.staff.title')]
        ];
        return view('dashboard.page.staff.index',compact('breadcrumbs'));
    }

    public function table(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.index'), 'name' => __('page.dashboard.staff.title')],
            ['name' => __('page.dashboard.staff.table')]
        ];
        $users = User::role([UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF])->orderBy('last_name')->get();
        return view('dashboard.page.staff.table',compact('breadcrumbs','users'));
    }

//    public

    public function exportExcel(): BinaryFileResponse
    {
        try {
            $users = User::role(UserRoleEnum::STAFF)->get();
            return (new StaffExport($users))->download('Staff('.now()->format('d.m.Y').').xlsx');
        }catch (Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    public function control(){
        return null;
    }

    public function reference(User $user, CreateStaffReferenceAction $createStaffReferenceAction)
    {
        try {
            return $createStaffReferenceAction->execute($user);
        } catch (PrintFormException $e) {
           return back();
        }

    }

    public function certificate(User $user, CreateStaffCertificateAction $createStaffCertificateAction){
        try {
            return $createStaffCertificateAction->execute($user);
        } catch (PrintFormException $e) {
            return back();
        }
    }
}
