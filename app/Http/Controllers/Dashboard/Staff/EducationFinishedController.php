<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\Staff\SaveFinishedEducationStaffAction;
use App\DTO\User\UserEducationFinishedDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFinishedRequest;
use App\Models\User;
use App\Models\UserEducationFinished;
use Exception;
use Illuminate\Http\RedirectResponse;

class EducationFinishedController extends BaseController
{

    public function store(UserFinishedRequest $request,User $user, SaveFinishedEducationStaffAction $saveFinishedEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveFinishedEducationStaffAction->executeStore(UserEducationFinishedDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('dashboard.staff.education.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

    public function update(UserFinishedRequest $request,User $user,UserEducationFinished $finished,SaveFinishedEducationStaffAction $saveFinishedEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveFinishedEducationStaffAction->executeUpdate(UserEducationFinishedDTO::fromRequest($request),$user,$finished);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('dashboard.staff.education.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

    public function destroy(User $user,UserEducationFinished $finished)
    {
        try {
            $finished->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.education.deleted')]);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('dashboard.staff.education.index',['user' => $user->username]);
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }

}
