<?php


namespace App\Http\Controllers\Dashboard\Staff;

use App\Actions\Staff\SaveFileStaffAction;
use App\DTO\User\UserFileDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFileRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class FileController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.file_field.title')]
        ];
        return view('dashboard.page.staff.file',compact('breadcrumbs','user'));
    }

    public function store(UserFileRequest $request,User $user, SaveFileStaffAction $saveFileStaffAction): RedirectResponse
    {
        try {
            $user = $saveFileStaffAction->executeStore(UserFileDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.file.success'));
            return redirect()->route('dashboard.staff.file.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.file.error'));
            return back()->withInput($request->all());
        }
    }

    public function update(UserFileRequest $request, User $user, SaveFileStaffAction $saveFileStaffAction): RedirectResponse
    {
        try {
            $user = $saveFileStaffAction->executeUpdate(UserFileDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.file.success'));
            return redirect()->route('dashboard.staff.file.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.file.error'));
            return back()->withInput($request->all());
        }
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->file->forceDelete();
            return response()->json(['message' => __('message.staff.file.deleted')]);
        }catch (Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }
}
