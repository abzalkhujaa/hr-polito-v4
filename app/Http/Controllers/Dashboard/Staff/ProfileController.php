<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\ChangePasswordAction;
use App\Actions\CreateUserAction;
use App\Actions\ResetPasswordAction;
use App\Actions\UpdateUserAction;
use App\DTO\User\UserChangePasswordDTO;
use App\DTO\User\UserDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\ChangePasswordUserRequest;
use App\Http\Requests\Dashboard\User\ResetPasswordUserRequest;
use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Http\Requests\Dashboard\User\UserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class ProfileController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.index'), 'name' => __('dashboard.staffs')],
            [  'name' => $user->username],
//            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.profile')]
        ];
        return view('dashboard.page.staff.profile',compact('breadcrumbs','user'));
    }

    public function create(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.staff.title')]
        ];
        return view('dashboard.page.staff.create',compact('breadcrumbs'));
    }

    public function store(UserRequest $request,CreateUserAction $createUserAction): RedirectResponse
    {
        try {
            $user = $createUserAction->execute(UserDTO::fromRequest($request));
            alert()->success(__('dashboard.profile_field.title'),__('dashboard.action.created'));
            return redirect()->route('dashboard.staff.profile.index',['user' => $user->username])->with('success',__('pages.staff.profile.added'));
        }catch (Exception $exception){
            alert()->error(__('dashboard.profile_field.title'),__('dashboard.action.not_created'));
            return back()->withInput($request->all());
        }
    }

    public function update(UpdateUserRequest $request, User $user,UpdateUserAction $updateUserAction): RedirectResponse
    {
        try {
            $user = $updateUserAction->execute(UserDTO::fromRequest($request),$user);
            alert()->success(__('dashboard.profile_field.title'),__('dashboard.action.updated'));
            return redirect()->route('dashboard.staff.profile.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.profile_field.title'),__('dashboard.action.not_updated'));
            return back()->withInput($request->all());
        }
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->delete();
            return response()->json(['message' => __('message.staff.profile.deleted'), 'status' => 'success']);
        }catch (Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }

    public function changePassword(ChangePasswordUserRequest $request, User $user,ChangePasswordAction $changePasswordAction): RedirectResponse
    {
        try {
            $user = $changePasswordAction->execute(UserChangePasswordDTO::fromRequest($request),$user);
            alert()->success(__('dashboard.action.password.changed'));
            return redirect()->route('dashboard.staff.profile.index',['user'=> $user->username]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.action.not_updated'));
            return back()->withInput($request->all());
        }
    }

    public function resetPassword(ResetPasswordUserRequest $request, User $user,ResetPasswordAction $resetPasswordAction): RedirectResponse
    {
        try {
            $user = $resetPasswordAction->execute($user);
            alert()->success(__('dashboard.action.password.changed'),$resetPasswordAction->getDefaultPassword());
            return redirect()->route('dashboard.staff.profile.index',['user'=> $user->username]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.action.not_updated'));
            return back()->withInput($request->all());
        }
    }

}
