<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\Staff\SaveFamilyStaffAction;
use App\DTO\User\UserFamilyDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserFamilyRequest;
use App\Models\User;
use App\Models\UserFamily;
use Exception;
use Illuminate\Http\RedirectResponse;

class FamilyController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.family_field.title')]
        ];
        return view('dashboard.page.staff.family',compact('breadcrumbs','user'));
    }

    public function store(UserFamilyRequest $request,User $user,SaveFamilyStaffAction $saveFamilyStaffAction): RedirectResponse
    {
        try {
            $user = $saveFamilyStaffAction->executeStore(UserFamilyDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('dashboard.staff.family.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }

    public function update(UserFamilyRequest $request, User $user, UserFamily $family,SaveFamilyStaffAction $saveFamilyStaffAction): RedirectResponse
    {
        try {
            $user = $saveFamilyStaffAction->executeUpdate(UserFamilyDTO::fromRequest($request),$user,$family);
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('dashboard.staff.family.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }

    public function destroy(User $user, UserFamily $family)
    {
        try {
            $family->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.family.deleted')]);
            alert()->success(__('pages.staff.family.success'));
            return redirect()->route('dashboard.staff.family.index',['user' => $user->username]);
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.family.error'));
            return back();
        }
    }
}
