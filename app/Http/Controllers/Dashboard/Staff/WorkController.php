<?php


namespace App\Http\Controllers\Dashboard\Staff;

use App\Actions\Staff\SaveWorkStaffAction;
use App\DTO\User\UserWorkDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserWorkRequest;
use App\Models\User;
use App\Models\UserWork;
use Exception;

use Illuminate\Http\RedirectResponse;

class WorkController extends BaseController
{
    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.work_field.title')]
        ];
        return view('dashboard.page.staff.work',compact('user','breadcrumbs'));
    }

    public function store(UserWorkRequest $request,User $user, SaveWorkStaffAction $saveWorkStaffAction): RedirectResponse
    {
        try {
            $user = $saveWorkStaffAction->executeStore(UserWorkDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.work.success'));
            return redirect()->route('dashboard.staff.work.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.work.error'));
            return back();
        }
    }

    public function update(UserWorkRequest $request, User $user, UserWork $work, SaveWorkStaffAction $saveWorkStaffAction): RedirectResponse
    {
        try {
            $user = $saveWorkStaffAction->executeUpdate(UserWorkDTO::fromRequest($request),$user,$work);
            alert()->success(__('pages.staff.work.success'));
            return redirect()->route('dashboard.staff.work.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.work.error'));
            return back();
        }
    }

    public function destroy(User $user, UserWork $work)
    {
        try {
            $work->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.work.deleted')]);
            alert()->success(__('pages.staff.finished.success'));
            return redirect()->route('dashboard.staff.work.index',['user' => $user->username]);
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.finished.error'));
            return back();
        }
    }
}
