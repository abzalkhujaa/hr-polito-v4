<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\Staff\SaveVacationStaffAction;
use App\DTO\User\UserVacationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserVacationRequest;
use App\Models\User;
use App\Models\UserVacation;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class VacationController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.vacation_field.title')]
        ];
        return view('dashboard.page.staff.vacation',compact('breadcrumbs','user'));
    }

    public function store(UserVacationRequest $request,User $user,SaveVacationStaffAction $saveVacationStaffAction): RedirectResponse
    {
        try {
            $user = $saveVacationStaffAction->executeStore(UserVacationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.vacation.success'));
            return redirect()->route('dashboard.staff.vacation.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.vacation.error'));
            return back()->with('error',__('pages.staff.vacation.not.added'))->withInput($request->all());
        }
    }

    public function update(UserVacationRequest $request, User $user, UserVacation $vacation,SaveVacationStaffAction $saveVacationStaffAction): RedirectResponse
    {
        try {
            $user = $saveVacationStaffAction->executeUpdate(UserVacationDTO::fromRequest($request),$user,$vacation);
            alert()->success(__('pages.staff.vacation.success'));
            return redirect()->route('dashboard.staff.vacation.index',['user' => $user->username]);
        }catch (Exception $exception){
            alert()->error(__('pages.staff.vacation.error'));
            return back()->withInput($request->all());
        }
    }

    public function destroy(User $user, UserVacation $vacation)
    {

        try {
            $vacation->forceDelete();
            if (request()->ajax()) return response()->json(['message' => __('message.staff.family.deleted')]);
            alert()->success(__('pages.staff.vacation.success'));
            return redirect()->route('dashboard.staff.vacation.index',['user' => $user->username]);
        }catch (Exception $exception){
            if (request()->ajax()){
                return response()->json([
                    'message' => $exception->getMessage()
                ],$exception->getCode());
            }
            alert()->error(__('pages.staff.vacation.error'));
            return back();
        }
    }

}
