<?php


namespace App\Http\Controllers\Dashboard\Staff;


use App\Actions\Staff\SaveEducationStaffAction;
use App\DTO\User\UserEducationDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\User\UserEducationRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class EducationController extends BaseController
{

    public function index(User $user){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.staff.profile.index',['user' => $user->username]), 'name' => $user->username],
            ['name' => __('dashboard.education_field.title')]
        ];
        return view('dashboard.page.staff.education',compact('breadcrumbs','user'));
    }

    public function store(UserEducationRequest $request,User $user,SaveEducationStaffAction $saveEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveEducationStaffAction->executeStore(UserEducationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.education.success'));
            return redirect()->route('dashboard.staff.education.index',['user' => $user->username])->with('success',__('pages.staff.education.added'));
        }catch (Exception $exception){
            alert()->error(__('pages.staff.education.error'));
            return back()->with('error',__('pages.staff.education.not.added'))->withInput($request->all());
        }
    }

    public function update(UserEducationRequest $request, User $user, SaveEducationStaffAction $saveEducationStaffAction): RedirectResponse
    {
        try {
            $user = $saveEducationStaffAction->executeUpdate(UserEducationDTO::fromRequest($request),$user);
            alert()->success(__('pages.staff.education.success'));
            return redirect()->route('dashboard.staff.education.index',['user' => $user->username])->with('success',__('pages.staff.education.added'));
        }catch (Exception $exception){
            alert()->error(__('pages.staff.education.error'));
            return back()->with('error',__('pages.staff.education.not.updated'))->withInput($request->all());
        }
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->education->forceDelete();
            return response()->json(['message' => __('message.staff.education.deleted')]);
        }catch (Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ],$exception->getCode());
        }
    }

}
