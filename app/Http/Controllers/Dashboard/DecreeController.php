<?php


namespace App\Http\Controllers\Dashboard;

use App\Actions\Decree\CreateDecreeAction;
use App\Actions\Decree\UpdateDecreeAction;
use App\DTO\DecreeDTO;
use App\Http\Requests\Dashboard\Blog\DecreeRequest;
use App\Models\Decree;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Str;

class DecreeController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.blog.decree.index')]
        ];
        $decree = Decree::query()->latest()->get();
        return view('dashboard.page.decree.index',compact('breadcrumbs','decree'));
    }

    public function create(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.decree.index') ,'name' => __('page.dashboard.blog.decree.index')],
            ['name' => __('page.dashboard.blog.decree.create')]
        ];
        return view('dashboard.page.decree.create',compact('breadcrumbs'));
    }

    public function edit(Decree $decree){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.decree.index') ,'name' => __('page.dashboard.blog.decree.index')],
            ['name' => Str::limit($decree->alias,80)]
        ];
        return view('dashboard.page.decree.edit',compact('breadcrumbs','decree'));
    }

    public function store(DecreeRequest $request, CreateDecreeAction $createDecreeAction): RedirectResponse
    {
        try {
            $decree = $createDecreeAction->execute(DecreeDTO::fromRequest($request));
            alert()->success(__('pages.blog.decree.success'));
            return redirect()->route('dashboard.decree.edit',['decree' => $decree->alias]);
        }catch (Exception $exception){
            alert()->success(__('pages.blog.decree.error'));
            return back();
        }
    }

    public function update(DecreeRequest $request, Decree $decree, UpdateDecreeAction $updateDecreeAction): RedirectResponse
    {
        try {
            $decree = $updateDecreeAction->execute(DecreeDTO::fromRequest($request,DecreeDTO::UPDATING),$decree);
            alert()->success(__('pages.blog.decree.success'));
            return redirect()->route('dashboard.decree.edit',['decree' => $decree->alias]);
        }catch (Exception $exception){
            alert()->success(__('pages.blog.decree.error'));
            return back();
        }
    }

    public function destroy(Decree $decree): JsonResponse
    {
        try {
            $decree->delete();
            return response()->json([
                'message' => __('message.decree.deleted'),
                'status' => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'status' => false
            ],$e->getCode());
        }
    }

}
