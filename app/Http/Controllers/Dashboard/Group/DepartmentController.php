<?php

namespace App\Http\Controllers\Dashboard\Group;

use App\DTO\DepartmentDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\Group\DepartmentRequest;
use App\Models\Department;
use App\Models\Section;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;


class DepartmentController extends BaseController
{
    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.group.title')],
            ['name' => __('page.dashboard.group.department.title')]
        ];
        $departments = Department::query()->latest()->get();
        $sections = Section::all();
        return view('dashboard.page.group.department.index',compact('breadcrumbs','departments','sections'));
    }

    public function store(DepartmentRequest $request): RedirectResponse
    {
        $data = DepartmentDTO::fromRequest($request);
        $department = Department::create($data->toArray());
        try {
            alert()->success($department->title,__('dashboard.section_added'));
            return redirect()->route('dashboard.group.department.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.department'),__('dashboard.not_saved'));
            return redirect()->route('dashboard.group.department.index')->withInput();
        }
    }

    public function update(DepartmentRequest $request, Department $department): RedirectResponse
    {
        $data = DepartmentDTO::fromRequest($request);
        $department->update($data->toArray());
        try {
            alert()->success($department->title,__('dashboard.department_updated'));
            return redirect()->route('dashboard.group.department.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.department'),__('dashboard.not_updated'));
            return redirect()->route('dashboard.group.department.index')->withInput();
        }
    }

    public function destroy(Department $department): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $department->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => [
                'title' => __('message.code_404')
            ]
        ],404);
    }
}
