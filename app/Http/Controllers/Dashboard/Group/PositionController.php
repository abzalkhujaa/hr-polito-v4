<?php


namespace App\Http\Controllers\Dashboard\Group;


use App\DTO\PositionDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\Group\PositionRequest;
use App\Models\Department;
use App\Models\Position;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class PositionController extends BaseController
{
    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.group.title')],
            ['name' => __('page.dashboard.group.position.title')]
        ];
        $positions = Position::query()->latest()->get();
        $departments = Department::all();
        return view('dashboard.page.group.position.index',compact('breadcrumbs','departments','positions'));
    }

    public function store(PositionRequest $request): RedirectResponse
    {
        $data = PositionDTO::fromRequest($request);
        $position = Position::create($data->toArray());
        try {
            alert()->success($position->title,__('dashboard.position_added'));
            return redirect()->route('dashboard.group.position.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.position'),__('dashboard.not_saved'));
            return redirect()->route('dashboard.group.position.index')->withInput();
        }
    }

    public function update(PositionRequest $request, Position $position): RedirectResponse
    {
        $data = PositionDTO::fromRequest($request);
        $position->update($data->toArray());
        try {
            alert()->success($position->title,__('dashboard.position_updated'));
            return redirect()->route('dashboard.group.position.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.position'),__('dashboard.not_updated'));
            return redirect()->route('dashboard.group.position.index');
        }
    }
    public function destroy(Position $position): JsonResponse
    {
        if ( request()->wantsJson() ){
            try {
                $position->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => [
                'title' => __('message.code_404')
            ]
        ],404);
    }





}
