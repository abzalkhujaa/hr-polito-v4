<?php


namespace App\Http\Controllers\Dashboard\Group;


use App\Http\Controllers\Dashboard\BaseController;

class GroupController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.group.title')]
        ];
        return view('dashboard.page.group.index',compact('breadcrumbs'));
    }
}
