<?php


namespace App\Http\Controllers\Dashboard\Group;


use App\DTO\SectionDTO;
use App\Http\Controllers\Dashboard\BaseController;
use App\Http\Requests\Dashboard\Group\SectionRequest;
use App\Models\Section;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class SectionController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.group.title')],
            ['name' => __('page.dashboard.group.section.title')]
        ];
        $sections = Section::query()->latest()->get();
        return view('dashboard.page.group.section.index',compact('breadcrumbs','sections'));
    }

//    public function create(){
//        return view('dashboard.page.group.section.create');
//    }
//
//    public function edit(Section $section){
//        return view('dashboard.page.group.section.edit',compact('section'));
//    }

    public function store(SectionRequest $request): RedirectResponse
    {
        $data = SectionDTO::fromRequest($request);
        $section = Section::create($data->toArray());
        try {
            alert()->success($section->title,__('dashboard.section_added'));
            return redirect()->route('dashboard.group.section.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.section'),__('dashboard.not_saved'));
            return redirect()->route('dashboard.group.section.index')->withInput();
        }
    }

    public function update(SectionRequest $request, Section $section): RedirectResponse
    {
        $data = SectionDTO::fromRequest($request);
        $section->update($data->toArray());
        try {
            alert()->success($section->title,__('dashboard.section_updated'));
            return redirect()->route('dashboard.group.section.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.section'),__('dashboard.not_updated'));
            return redirect()->route('dashboard.group.section.index');
        }
    }

    public function destroy(Section $section): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $section->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => [
                'title' => __('message.code_404')
            ]
        ],404);
    }
}
