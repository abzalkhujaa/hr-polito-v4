<?php


namespace App\Http\Controllers\Dashboard;

use App\Actions\Vacancy\CreateVacancyAction;
use App\Actions\Vacancy\UpdateVacancyAction;
use App\DTO\VacancyDTO;
use App\Http\Requests\Dashboard\Blog\VacancyRequest;
use App\Models\Position;
use App\Models\Vacancy;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Str;

class VacancyController extends BaseController
{


    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.blog.vacancy.index')]
        ];
        $vacancy = Vacancy::query()->latest()->get();
        return view('dashboard.page.vacancy.index',compact('breadcrumbs','vacancy'));
    }

    public function create(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.vacancy.index') ,'name' => __('page.dashboard.blog.vacancy.index')],
            ['name' => __('page.dashboard.blog.vacancy.create')]
        ];
        $positions = Position::all();
        return view('dashboard.page.vacancy.create',compact('positions','breadcrumbs'));
    }

    public function edit(Vacancy $vacancy){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.vacancy.index') ,'name' => __('page.dashboard.blog.vacancy.index')],
            ['name' =>  Str::limit($vacancy->alias,80)]
        ];
        $positions = Position::all();
        return view('dashboard.page.vacancy.edit',compact('positions','breadcrumbs','vacancy'));
    }

    public function store(VacancyRequest $request, CreateVacancyAction $createVacancyAction): RedirectResponse
    {
        try {
            $vacancy = $createVacancyAction->execute(VacancyDTO::fromRequest($request));
            alert()->success(__('pages.blog.vacancy.success'));
            return redirect()->route('dashboard.vacancy.edit',['vacancy' => $vacancy->alias]);
        }catch (Exception $exception){
            alert()->error(__('pages.blog.vacancy.error'));
            return back();
        }
    }

    public function update(VacancyRequest $request, Vacancy $vacancy,UpdateVacancyAction $updateVacancyAction): RedirectResponse
    {
        try {
            $vacancy = $updateVacancyAction->execute(VacancyDTO::fromRequest($request,VacancyDTO::UPDATING),$vacancy);
            alert()->success(__('pages.blog.vacancy.success'));
            return redirect()->route('dashboard.vacancy.edit',['vacancy' => $vacancy->alias]);
        }catch (Exception $exception){
            alert()->error(__('pages.blog.vacancy.error'));
            return back();
        }
    }

    public function destroy(Vacancy $vacancy): JsonResponse
    {
        try {
            $vacancy->delete();
            return response()->json([
                'message' => __('message.decree.deleted'),
                'status' => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'status' => false
            ],$e->getCode());
        }
    }
}
