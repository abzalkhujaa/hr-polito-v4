<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Enums\UserRoleEnum;
use App\Models\Decree;
use App\Models\News;
use App\Models\User;
use App\Models\Vacancy;

class DashboardController extends BaseController
{

    public function index(){
        $statistics = [
            'users' => User::role([UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF])->count(),
            'news' => News::count(),
            'decree' => Decree::count(),
            'vacancy' => Vacancy::count()
        ];
        $users = User::role([UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF])->orderByDesc('created_at')->get();
        return view('dashboard.page.main.index',compact('statistics','users'));
    }
}
