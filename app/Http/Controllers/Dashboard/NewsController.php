<?php


namespace App\Http\Controllers\Dashboard;

use App\Actions\News\CreateNewsAction;
use App\Actions\News\UpdateNewsAction;
use App\DTO\NewsDTO;
use App\Http\Requests\Dashboard\Blog\NewsRequest;
use App\Models\News;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Str;

class NewsController extends BaseController
{

    public function index(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['name' => __('page.dashboard.blog.news.index')],
        ];
        $news = News::query()->latest()->get();
        return view('dashboard.page.news.index',compact('news','breadcrumbs'));
    }

    public function create(){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.news.index') ,'name' => __('page.dashboard.blog.news.index')],
            ['name' => __('page.dashboard.blog.news.create')]
        ];
        return view('dashboard.page.news.create',compact('breadcrumbs'));
    }

    public function edit(News $news){
        $breadcrumbs = [
            ['link' => route('dashboard.index'), 'name' => "<i class='feather icon-home'></i>"],
            ['link' => route('dashboard.news.index') ,'name' => __('page.dashboard.blog.news.index')],
            ['name' => Str::limit($news->alias,80)]
        ];
        return view('dashboard.page.news.edit',compact('news','breadcrumbs'));
    }

    public function store(NewsRequest $request, CreateNewsAction $createNewsAction): RedirectResponse
    {
        try {
            $news = $createNewsAction->execute(NewsDTO::fromRequest($request));
            alert()->success(__('pages.blog.news.success'));
            return redirect()->route('dashboard.news.edit',['news' => $news->alias]);
        }catch (Exception $exception){
            alert()->error(__('pages.blog.news.error'));
            return back();
        }
    }

    public function update(NewsRequest $request, News $news,UpdateNewsAction $updateNewsAction): RedirectResponse
    {
        try {
            $news = $updateNewsAction->execute(NewsDTO::fromRequest($request,NewsDTO::UPDATING),$news);
            alert()->success(__('pages.blog.news.success'));
            return redirect()->route('dashboard.news.edit',['news' => $news->alias]);
        }catch (Exception $exception){
            alert()->error(__('pages.blog.news.error'));
            return back();
        }
    }

    public function destroy(News $news): JsonResponse
    {
        try {
            $news->delete();
            return response()->json([
                'message' => __('message.decree.deleted'),
                'status' => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'status' => false
            ],$e->getCode());
        }
    }

}
