<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Illuminate\Support\Carbon;
use Livewire\Component;

class Vacation extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;


    public ?string $duration;
    public string $start;
    public ?string $finish = null;

    public function mount(){
        $this->duration = 0;
        $this->start = now()->toDateString();
        $this->finish = null;
    }

    public function updatedStart(){
        if ((int)$this->duration !== 0){
            $this->finish = Carbon::make($this->start)->addBusinessDays($this->duration)->toDateString();
        }elseif (!is_null($this->finish)){
            $this->duration = Carbon::make($this->finish)->addBusinessDay()->diffInBusinessDays(Carbon::make($this->start));
        }
    }

    public function updatedDuration(){
        if ($this->start and (int)$this->duration !== 0){
            $this->finish = Carbon::make($this->start)->addBusinessDays($this->duration)->addBusinessDays(-1)->toDateString();
        }
    }

    public function updatedFinish(){
        if ((int)$this->duration === 0 or is_null($this->duration)){
            $this->duration = Carbon::make($this->finish)->addBusinessDay()->diffInBusinessDays(Carbon::make($this->start));
        }
    }

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.vacation.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.vacation.index',['user' => $this->user->username]);
    }

    public function render()
    {
        return view('livewire.staff.pagination.vacation');
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.vacation.index');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.vacation.index');
    }
}
