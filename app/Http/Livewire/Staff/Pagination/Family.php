<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Livewire\Component;

class Family extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.family.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.family.index',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.family.store');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.family.index');
    }

    public function render()
    {
        return view('livewire.staff.pagination.family');
    }
}
