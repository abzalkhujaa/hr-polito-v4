<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Livewire\Component;

class File extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public function mount(){
        $this->listeners = ['fileSaved' => '$refresh'];
    }

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.file.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.file.update',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.file.store');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.file.update');
    }

    public function render()
    {
        return view('livewire.staff.pagination.file');
    }
}
