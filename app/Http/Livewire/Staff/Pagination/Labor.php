<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use App\Models\Department;
use App\Models\Position;
use App\Models\Section;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Labor extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public Section $section;
    public Collection $sections;
    public Department $department;
    public Collection $departments;
    public Position $position;
    public Collection $positions;

    public int $temp_section;
    public int $temp_department;
    public int $temp_position;


    public function editToggle()
    {
        $this->disabled = !$this->disabled;

        if ($this->disabled === true){
            $this->mount();
        }
    }

    public function mount(){
        $this->sections = Section::all();
        $this->departments = Department::all();
        $this->positions = Position::all();
        if ($this->user->labor){
            $this->temp_section = $this->user->labor->section_id;
            $this->temp_department = $this->user->labor->department_id;
            $this->temp_position = $this->user->labor->position_id;
        }

    }
    public function selectSection($value){
        if ($this->disabled === false){
            if (!is_null($value) and $value !== ''){
                $this->temp_section = $value;
                $this->section = Section::where('id',$value)->get()->first();
                $this->departments = $this->section->department;
            }else{
                $this->departments = new Collection();
            }
            $this->positions = new Collection();
        }

    }
    public function selectDepartment($value){
        if ($this->disabled === false){
            if (!is_null($value) and $value !== ''){
                $this->temp_department = $value;
                $this->temp_position = 0;
                $this->department = Department::where('id',$value)->get()->first();
                $this->positions = $this->department->positions;
            }else{
                $this->positions = new Collection();
            }
        }

    }

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.labor.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.labor.update',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.labor.store');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.labor.update');
    }

    public function render()
    {
        return view('livewire.staff.pagination.labor');
    }
}
