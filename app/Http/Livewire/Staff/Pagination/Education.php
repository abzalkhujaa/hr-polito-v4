<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Livewire\Component;

class Education extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.education.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.education.update',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.education.store');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.education.update');
    }

    public function render()
    {
        return view('livewire.staff.pagination.education');
    }



}
