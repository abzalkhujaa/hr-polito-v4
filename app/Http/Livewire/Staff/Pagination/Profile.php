<?php

namespace App\Http\Livewire\Staff\Pagination;

use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Livewire\Component;

class Profile extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.store');
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.profile.update',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.profile.index');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.profile.update');
    }

    public function render()
    {
        return view('livewire.staff.pagination.profile');
    }
}
