<?php

namespace App\Http\Livewire\Staff\Pagination;


use App\Helpers\Interfaces\StaffPaginationEditable;
use App\Helpers\Traits\HasStaffPaginationComponent;
use Livewire\Component;

class Information extends Component implements StaffPaginationEditable
{
    use HasStaffPaginationComponent;

    public function getStoreRoute(): string
    {
        return route('dashboard.staff.information.store',['user' => $this->user->username]);
    }

    public function getUpdateRoute(): string
    {
        return route('dashboard.staff.information.update',['user' => $this->user->username]);
    }

    public function getStaffStoreRoute(): string
    {
        return route('staff-dashboard.staff.information.store');
    }

    public function getStaffUpdateRoute(): string
    {
        return route('staff-dashboard.staff.information.update');
    }

    public function render()
    {
        return view('livewire.staff.pagination.information');
    }
}
