<?php

namespace App\Http\Livewire\Staff;


use App\Helpers\Enums\UserRoleEnum;
use App\Models\Department;
use App\Models\Section;
use App\Models\User;
use Illuminate\Support\Collection;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Str;

class AllWithFilter extends Component
{
    public Collection $staff;
    public Collection $sections;
    public Collection $departments;
    public Collection $roles;
    public string $search;


    public function mount(){
        $this->search = '';
        $this->staff = User::role([UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF])->latest()->get();
        $this->sections = Section::all();
        $this->departments = Department::all();
        $this->roles = Role::query()->whereNotIn('name',[UserRoleEnum::SUPER])->get();
    }

    public function updatedSearch(){
        $search = Str::of($this->search);
        if ($search->length() > 0)
            $this->staff = User::query()->whereRaw('concat(first_name,\' \',last_name,\' \',patronymic) LIKE ?',"%{$search->trim()->ucfirst()}%")->get();
        else
            $this->staff =  $this->staff = User::query()->latest()->get();
    }

    public function changeRoleFilter($value){
        if ($value != 0){
            $this->staff = User::role($value)->get();
        }else{
            $this->staff = User::query()->latest()->get();
        }
    }

    public function changeSectionFilter($value){
        if ($value != 0){
            $staff_id = Section::where('id',$value)->first()->labor->pluck('user_id');
            $this->staff = User::findMany($staff_id);
        }else{
            $this->staff = User::query()->latest()->get();
        }
    }
    public function changeDepartmentFilter($value){
        if ($value != 0){
            $staff_id = Department::where('id',$value)->first()->labor->pluck('user_id');
            $this->staff = User::findMany($staff_id);
        }else{
            $this->staff = User::query()->latest()->get();
        }
    }

    public function render()
    {
        return view('livewire.staff.all-with-filter');
    }
}
