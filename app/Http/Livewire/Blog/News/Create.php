<?php

namespace App\Http\Livewire\Blog\News;

use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;

    public string $image;
    public $uploaded;
    public bool $error_image_upload = false;

    public function updatedUploaded(){
        $this->emit('reRenderCKEditor');
        $this->validate([
            'uploaded' => 'required|file|max:3072|mimes:jpeg,jpg,png'
        ]);
        $this->image = $this->uploaded->store('news');
        $this->emit('fileSaved');
    }

    public function render()
    {
        return view('livewire.blog.news.create');
    }
}
