<?php

namespace App\Http\Livewire\Blog\News;

use App\Models\News;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use WithFileUploads;
    public News $news;
    public ?string $image;
    public $uploaded;

    public function mount(){
        $this->image = $this->news->image;
    }

    public function updatedUploaded(){
        $this->emit('reRenderCKEditor');
        $this->validate([
            'uploaded' => 'required|file|max:3072|mimes:jpeg,jpg,png'
        ]);
        $this->image = $this->uploaded->store('news');
        $this->emit('fileSaved');
    }
    public function render()
    {
        return view('livewire.blog.news.edit');
    }
}
