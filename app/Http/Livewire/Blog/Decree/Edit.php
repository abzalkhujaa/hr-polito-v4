<?php

namespace App\Http\Livewire\Blog\Decree;

use App\Models\Decree;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use WithFileUploads;

    public Decree $decree;

    public ?string $image;
    public ?string $file;
    public $uploaded;
    public $uploaded2;

    public function mount(){
        $this->image = $this->decree->image;
        $this->file = $this->decree->file;
    }

    public function updatedUploaded(){
        $this->emit('reRenderCKEditor');
        $this->validate([
            'uploaded' => 'required|file|max:3072|mimes:jpeg,jpg,png'
        ]);
        $this->image = $this->uploaded->store('news');
        $this->emit('fileSaved');
    }

    public function updatedUploaded2(){
        $this->emit('reRenderCKEditor');
        $this->validate([
            'uploaded2' => 'required|file|max:3072|mimes:jpeg,jpg,png,pdf,doc,docx'
        ]);
        $this->file = $this->uploaded2->store('news');
        $this->emit('fileSaved');
    }

    public function render()
    {
        return view('livewire.blog.decree.edit');
    }
}
