<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CertificateSearchRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'passport_series' => 'required|string|min:2|max:2',
            'passport_number' => 'required|string|size:7',
            'b_day' => 'required'
        ];
    }
}
