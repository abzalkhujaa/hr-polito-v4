<?php

namespace App\Http\Requests;

use App\Helpers\Enums\UserRoleEnum;
use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{

    public function authorize(): bool
    {
        return auth()->user()->hasRole([UserRoleEnum::SUPER,UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR,UserRoleEnum::STAFF]);
    }


    public function rules(): array
    {
        return [
            'file' => 'required|file',
            'type' => 'required|string',
            'folder' => 'required|string'
        ];
    }
}
