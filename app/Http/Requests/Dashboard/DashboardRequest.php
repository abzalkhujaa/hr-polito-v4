<?php


namespace App\Http\Requests\Dashboard;


use App\Helpers\Enums\UserRoleEnum;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

abstract class DashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check() && Auth::user()->hasRole([UserRoleEnum::SUPER,UserRoleEnum::ADMIN, UserRoleEnum::MODERATOR, UserRoleEnum::STAFF]);
    }
}
