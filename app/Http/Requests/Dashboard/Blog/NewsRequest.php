<?php

namespace App\Http\Requests\Dashboard\Blog;

use App\Http\Requests\Dashboard\DashboardRequest;


class NewsRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'content' => 'required|string',
            'image' => 'string|nullable'
        ];
    }
}
