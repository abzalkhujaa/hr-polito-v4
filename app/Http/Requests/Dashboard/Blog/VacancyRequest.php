<?php

namespace App\Http\Requests\Dashboard\Blog;

use App\Http\Requests\Dashboard\DashboardRequest;

class VacancyRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'responsibility' => 'string|nullable',
            'requirement' => 'required|string',
            'content' => 'string|nullable',
            'position_id' => 'required|integer'
        ];
    }
}
