<?php

namespace App\Http\Requests\Dashboard\Blog;

use App\Http\Requests\Dashboard\DashboardRequest;

class DecreeRequest extends DashboardRequest
{

    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'string|nullable',
            'file' => 'required|string|nullable'
        ];
    }
}
