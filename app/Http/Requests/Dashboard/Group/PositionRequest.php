<?php

namespace App\Http\Requests\Dashboard\Group;

use App\Http\Requests\Dashboard\DashboardRequest;

class PositionRequest extends DashboardRequest
{


    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'department_id' => 'required|integer'
        ];
    }
}
