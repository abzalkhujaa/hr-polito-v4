<?php

namespace App\Http\Requests\Dashboard\Group;

use App\Http\Requests\Dashboard\DashboardRequest;

class SectionRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string'
        ];
    }
}
