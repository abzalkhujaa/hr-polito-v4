<?php


namespace App\Http\Requests\Dashboard\User;


use App\Http\Requests\Dashboard\DashboardRequest;

class ResetPasswordUserRequest extends DashboardRequest
{
    public function rules(): array
    {
        return [
            'password' => ['nullable','boolean']
        ];
    }
}
