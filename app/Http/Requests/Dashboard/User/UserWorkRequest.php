<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;


class UserWorkRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'start' => 'required|string',
            'finish' => 'required|string',
            'place' => 'required|string'
        ];
    }
}
