<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;


class UserInformationRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'passport_number' => 'required|string|size:9',
            'b_day' => 'required|date',
            'city' => 'required|string|max:50',
            'nationality' => 'required|string|max:30',
            'region' => 'required|string|max:40',
            'district' => 'required|string|max:50',
            'address' => 'required|string',
            'phone' => 'required|string|max:50',
            'home_phone' => 'string|max:50|nullable'
        ];
    }
}
