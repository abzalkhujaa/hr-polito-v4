<?php


namespace App\Http\Requests\Dashboard\User;


use App\Helpers\Enums\UserRoleEnum;
use App\Http\Requests\Dashboard\DashboardRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends DashboardRequest{


    public function rules() : array
    {
       return [
           'username' => [
               'required',
               'string',
               'max:100',
               'min:6',
               Rule::unique('users')->ignoreModel((auth()->check() && auth()->user()->hasRole(UserRoleEnum::STAFF)) ? auth()->user() : $this->user)
           ],
           'email' => [
               'required',
               'email',
               Rule::unique('users')->ignoreModel((auth()->check() && auth()->user()->hasRole(UserRoleEnum::STAFF)) ? auth()->user() : $this->user)],
//           'password' => 'required|min:8',
           'first_name' => 'required|string',
           'last_name' => 'required|string',
           'patronymic' => 'required|string',
           'gender' => 'required|string|max:1'
       ];
    }

}
