<?php


namespace App\Http\Requests\Dashboard\User;


use App\Http\Requests\Dashboard\DashboardRequest;
use App\Rules\CheckOldPasswordRule;

class ChangePasswordUserRequest extends DashboardRequest
{

    public function rules(): array
    {
        return [
            "old_password" => ['required','string','min:8',new CheckOldPasswordRule($this->user)],
            "password" => ['required','string','min:8', 'confirmed']
        ];
    }

}
