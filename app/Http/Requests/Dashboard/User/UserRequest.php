<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;
use Illuminate\Validation\Rule;

class UserRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => ['required','string','max:30','min:5', 'unique:users'],
            'email' => ['required','email','unique:users'],
            'password' => 'required|min:8',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'patronymic' => 'required|string',
            'gender' => 'required|string|max:1'
        ];
    }
}
