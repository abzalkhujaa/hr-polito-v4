<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;


class UserVacationRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => 'required|string',
            'start' => 'required|date',
            'finish' => 'required|date',
            'duration' => 'required|integer',
            'period' => 'string|nullable',
            'additional' => 'string|nullable',
        ];
    }
}
