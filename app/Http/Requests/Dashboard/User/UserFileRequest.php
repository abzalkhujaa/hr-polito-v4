<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;


class UserFileRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'image' => 'file|max:3072|mimes:jpeg,jpg,png|nullable',
            'diploma' => 'file|max:3072|mimes:jpeg,jpg,png,pdf|nullable',
            'inn' => 'file|max:3072|mimes:jpeg,jpg,png,pdf|nullable',
            'inps' => 'file|max:3072|mimes:jpeg,jpg,png,pdf|nullable',
            'military' => 'file|max:3072|mimes:jpeg,jpg,png,pdf|nullable',
            'passport' => 'file|max:3072|mimes:jpeg,jpg,png,pdf|nullable'
        ];
    }
}
