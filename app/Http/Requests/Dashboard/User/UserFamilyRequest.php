<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;


class UserFamilyRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'family_member' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'patronymic' => 'string|nullable',
            'b_day' => 'required',
            'city' => 'required|string',
            'work' => 'required|string',
            'current_place' => 'required|string',
            'status' => 'boolean|nullable'
        ];
    }
}
