<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;

class UserEducationRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'education' => 'required|string|max:100',
            'specialty' => 'required|string|max:100',
            'partying' => 'string|nullable',
            'academic_degree' => 'string|nullable',
            'academic_title' => 'string|nullable',
            'foreign_lang' => 'required',
            'military_rank' => 'string|nullable',
            'state_award' => 'string|nullable',
            'state_member' => 'string|nullable'
        ];
    }
}
