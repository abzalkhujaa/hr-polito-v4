<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\Dashboard\DashboardRequest;

class UserLaborRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'section_id' => 'required|integer',
            'department_id' => 'required|integer',
            'position_id' => 'required|integer',
            'stavka' => 'required',
            'basis_labor' => 'required|string',
            'working_limit' => 'required|string',
            'start_command' => 'required|string',
            'finish_command' => 'string|nullable',
            'last_vacation' => 'string|nullable',
            'maternity' => 'string|nullable',
            'pensioner' => 'string|nullable',
            'disciplinary' => 'string|nullable',
            'contract_number' => 'required|string',
            'employment_num' => 'required|string|max:30',
            'development' => 'string|nullable',
            'inn_number' => 'required|string|max:20',
        ];
    }
}
