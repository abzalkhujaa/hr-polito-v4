<?php

namespace App\Http\Middleware;

use App\Helpers\Traits\HasAuthRedirectPath;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    use HasAuthRedirectPath;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect($this->authRedirectPath());
        }

        return $next($request);
    }
}
