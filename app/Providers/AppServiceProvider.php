<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use URL;


class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
//      \URL::forceScheme('https');
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
      if (!config('app.debug')){
          URL::forceScheme('https');
      }

      Schema::defaultStringLength(191);
  }
}
