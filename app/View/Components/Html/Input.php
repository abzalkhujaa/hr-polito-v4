<?php

namespace App\View\Components\Html;

use Illuminate\View\Component;

class Input extends Component
{
    public string $type;
    public ?string $value = '';
    public ?string $name;
    public string $label;
    public bool $required;
    public bool $disabled;
    public ?string $idName;
    public string $pattern = '';

    /**
     * Create a new component instance.
     *
     * @param string $type
     * @param string|null $name
     * @param string $label
     * @param bool $required
     * @param bool $disabled
     * @param string|null $idName
     * @param string $pattern
     * @param string $value
     */
    public function __construct(string $type,?string $name, string $label,bool $required, bool $disabled, string $idName = null, string $pattern = '',?string $value = '')
    {
        $this->type = $type;
        $this->name = $name;
        $this->value = $value;
        $this->label = $label;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->pattern = $pattern;
        if (is_null($idName))
            $this->idName = $name;
        else
            $this->idName = $idName;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.html.input');
    }
}
