<?php


namespace App\Helpers\Interfaces;


interface StaffPaginationEditable
{
    public function getStoreRoute(): string;
    public function getUpdateRoute(): string;
    public function getStaffStoreRoute(): string;
    public function getStaffUpdateRoute(): string;
}
