<?php


namespace App\Helpers\Enums;


final class VacationTypeEnum
{

    const TYPE1 = 'Мехнат таътили';
    const TYPE2 = 'Иш хақи сакланмаган ҳолда таътил';
    const TYPE3 = 'Ижодий таътил';
    const TYPE4 = 'Бола парвариши таътили';


    public static function toArray(): array
    {
        return [
            self::TYPE1,
            self::TYPE2,
            self::TYPE3,
            self::TYPE4,
        ];
    }

}
