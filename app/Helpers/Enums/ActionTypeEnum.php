<?php


namespace App\Helpers\Enums;


final class ActionTypeEnum
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';
}
