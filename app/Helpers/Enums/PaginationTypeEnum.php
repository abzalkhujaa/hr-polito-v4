<?php


namespace App\Helpers\Enums;


final class PaginationTypeEnum
{
    public const MAIN = 'main';
    public const INFORMATION = 'information';
    public const EDUCATION = 'education';
    public const LABOR = 'labor';
    public const WORK = 'work';
    public const FAMILY = 'family';
    public const FILE = 'file';
    public const VACATION = 'vacation';

    public static function toArray(): array
    {
        return [
            self::MAIN,
            self::INFORMATION,
            self::EDUCATION,
            self::LABOR,
            self::WORK,
            self::FAMILY,
            self::FILE,
            self::VACATION
        ];
    }

}
