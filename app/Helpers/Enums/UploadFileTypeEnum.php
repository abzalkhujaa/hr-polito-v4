<?php


namespace App\Helpers\Enums;


final class UploadFileTypeEnum
{

    const NEWS = 'news';
    const PUBLIC = 'public';
    const QRCODE = 'qrcode';
    const STAFF = 'staff';

}
