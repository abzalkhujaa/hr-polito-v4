<?php


namespace App\Helpers\Enums;


final class StaffBasisLaborEnum
{

    public const MAIN = 'Асосий штатда';
    public const INNER = 'Ички ўриндошлик';
    public const OUTER = 'Ташқи Ўриндошлик';

    public static function toArray(): array
    {
        return [
            self::MAIN,
            self::INNER,
            self::OUTER
        ];
    }
}
