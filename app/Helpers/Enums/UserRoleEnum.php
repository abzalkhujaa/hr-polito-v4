<?php


namespace App\Helpers\Enums;


final class UserRoleEnum
{
    public const SUPER = 'super-admin';
    public const ADMIN = 'admin';
    public const STAFF = 'staff';
    public const USER = 'user';
    public const MODERATOR = 'moderator';

    public static function toArray(): array
    {
        return [
            self::SUPER,
            self::ADMIN,
            self::STAFF,
            self::USER,
            self::MODERATOR
        ];
    }

}
