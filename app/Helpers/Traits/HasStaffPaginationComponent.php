<?php


namespace App\Helpers\Traits;


use App\Helpers\Enums\ActionTypeEnum;
use App\Helpers\Enums\UserRoleEnum;
use App\Models\User;

trait HasStaffPaginationComponent
{
    public string $action;
    public string $type;
    public User $user;

    public bool $disabled = true;

    public function hydrate(){
        if (auth()->check() && auth()->user()->hasRole([UserRoleEnum::SUPER,UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR])){
            switch ($this->type){
                case ActionTypeEnum::CREATE:
                    $this->action = $this->getStoreRoute();
                    break;
                case ActionTypeEnum::UPDATE:
                    $this->action = $this->getUpdateRoute();
                    break;
            }
        }elseif(auth()->check() && auth()->user()->hasRole(UserRoleEnum::STAFF)){
            switch ($this->type){
                case ActionTypeEnum::CREATE:
                    $this->action = $this->getStaffStoreRoute();
                    break;
                case ActionTypeEnum::UPDATE:
                    $this->action = $this->getStaffUpdateRoute();
                    break;
            }
        }else{
            abort(404);
        }

    }
    public function editToggle(){
        $this->disabled = !$this->disabled;
    }
}
