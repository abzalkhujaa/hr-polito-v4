<?php


namespace App\Helpers\Traits;


use App\Helpers\Enums\PaginationTypeEnum;

trait HasStaffPaginationIcon
{

    public function getIcon(string $key): string
    {
        if (in_array($key,PaginationTypeEnum::toArray())){
            if ($key === PaginationTypeEnum::MAIN)
                return 'icon-check-circle';
            elseif ($key === PaginationTypeEnum::VACATION)
                return 'icon-info';
            elseif ($key === PaginationTypeEnum::FAMILY)
                return ($this->hasFamily()) ? 'icon-check-circle' : 'icon-x-circle';
            elseif ($key === PaginationTypeEnum::WORK)
                return ($this->hasWork()) ? 'icon-check-circle' : 'icon-x-circle';
            elseif ($this->$key)
                return 'icon-check-circle';
            else
                return 'icon-x-circle';
        }
        return 'icon-warning';
    }

    public function getColor(string $key): string
    {
        if (in_array($key,PaginationTypeEnum::toArray())){
            if ($key === PaginationTypeEnum::MAIN)
                return 'text-success';
            elseif ($key === PaginationTypeEnum::VACATION)
                return 'text-info';
            elseif ($key === PaginationTypeEnum::FAMILY)
                return ($this->hasFamily()) ? 'text-success' : 'text-danger';
            elseif ($key === PaginationTypeEnum::WORK)
                return ($this->hasWork()) ? 'text-success' : 'text-danger';
            elseif (!empty($this->$key) or $this->$key)
                return 'text-success';
            else
                return 'text-danger';
        }
        return 'text-warning';
    }

}
