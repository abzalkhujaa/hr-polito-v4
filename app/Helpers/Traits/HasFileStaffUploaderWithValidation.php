<?php


namespace App\Helpers\Traits;

trait HasFileStaffUploaderWithValidation
{
    public $image;
    public $diploma;
    public $inn;
    public $inps;
    public $military;
    public $passport;

    private function getPath(): string
    {
        return 'staff/'.$this->user->username;
    }

    public function updatedImage(){
        $this->validate([
            'image' => 'required|file|max:3072|mimes:jpeg,jpg,png'
        ]);
    }
    public function saveImage(){
        $file = $this->image->store($this->getPath());
        $this->saveFile('image',$file);
    }

    public function updatedDiploma(){
        $this->validate([
            'diploma' => 'file|max:3072|mimes:jpeg,jpg,png,pdf'
        ]);
    }
    public function saveDiploma(){
        $file = $this->diploma->store($this->getPath());
        $this->saveFile('diploma',$file);
    }

    public function updatedInn(){
        $this->validate([
            'inn' => 'file|max:3072|mimes:jpeg,jpg,png,pdf'
        ]);
    }
    public function saveInn(){
        $file = $this->inn->store($this->getPath());
        $this->saveFile('inn',$file);
    }

    public function updatedInps(){
        $this->validate([
            'inps' => 'file|max:3072|mimes:jpeg,jpg,png,pdf'
        ]);
    }
    public function saveInps(){
        $file = $this->inps->store($this->getPath());
        $this->saveFile('inps',$file);
    }

    public function updatedMilitary(){
        $this->validate([
            'military' => 'file|max:3072|mimes:jpeg,jpg,png,pdf'
        ]);
    }
    public function saveMilitary(){
        $file = $this->military->store($this->getPath());
        $this->saveFile('military',$file);
    }

    public function updatedPassport(){
        $this->validate([
            'passport' => 'file|max:3072|mimes:jpeg,jpg,png,pdf'
        ]);
    }
    public function savePassport(){
        $file = $this->passport->store($this->getPath());
        $this->saveFile('passport',$file);
    }

}
