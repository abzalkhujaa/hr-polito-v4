<?php


namespace App\Helpers\Traits;


use App\Actions\CreateQrCodeAction;
use Storage;

trait HasUserDataChecker
{
    public function hasQRCode(): bool
    {
        return Storage::exists((new CreateQrCodeAction)->execute($this));
    }
    public function hasFullName(): bool
    {
        return (!is_null($this->first_name) and !is_null($this->last_name) and !is_null($this->patronymic));
    }
    public function hasInformation(?string $field = null): bool
    {
        if ($this->information){
            if (is_null($field)){
                return true;
            }else{
                if (array_key_exists($field,$this->attributesToArray())){
                    return !is_null($this->getAttribute($field));
                }
            }
        }
        return false;
    }

    public function hasEducation(?string $field = null): bool
    {
        if ($this->education)
            if (is_null($field))
                return false;
            else
                if (array_key_exists($field,$this->attributesToArray()))
                    return !is_null($this->getAttribute($field));

        return false;
    }

    public function hasLabor(?string $field = null): bool
    {
        if ($this->labor){
            if (is_null($field)){
                return false;
            }else{
                if (array_key_exists($field,$this->attributesToArray())){
                    return !is_null($this->getAttribute($field));
                }
            }
        }
        return false;
    }

    public function hasWork(): bool
    {
        return !empty($this->works->toArray());
    }

    public function hasFamily(): bool
    {
        return !empty($this->family->toArray());
    }

    public function hasFile(?string $field = null): bool
    {
        if ($this->file){
            if (is_null($field)){
                return false;
            }else{
                if (array_key_exists($field,$this->attributesToArray())){
                    return !is_null($this->getAttribute($field));
                }
            }
        }
        return false;
    }

}
