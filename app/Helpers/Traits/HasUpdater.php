<?php


namespace App\Helpers\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasUpdater
{
    public function updater(): BelongsTo
    {
        return $this->belongsTo(User::class,'updated_by','id');
    }
}
