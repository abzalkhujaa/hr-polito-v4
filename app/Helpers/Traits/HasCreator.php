<?php


namespace App\Helpers\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasCreator
{
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by','id');
    }
}
