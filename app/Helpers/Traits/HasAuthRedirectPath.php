<?php


namespace App\Helpers\Traits;

use App\Helpers\Enums\UserRoleEnum;
use Illuminate\Support\Facades\Auth;

trait HasAuthRedirectPath
{
    public function authRedirectPath(): string
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole([UserRoleEnum::SUPER,UserRoleEnum::ADMIN,UserRoleEnum::MODERATOR])){
                return route('dashboard.index');
            }elseif(Auth::user()->hasRole(UserRoleEnum::STAFF)){
                return route('staff-dashboard.index');
            }else{
                return '';
            }
        }
        return route('home.index');

    }
}
