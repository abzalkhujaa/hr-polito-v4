<?php


namespace App\Helpers\Classes;


use App\Helpers\Traits\HasAuthRedirectPath;

class AuthRedirectHelper
{
    use HasAuthRedirectPath;

    public static function getRedirectPath(): string
    {
        return (new self())->authRedirectPath();
    }
}
