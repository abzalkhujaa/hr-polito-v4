<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_education', function (Blueprint $table) {
            $table->id();
            $table->string("education");
            $table->string("specialty");
            $table->string("partying")->nullable();
            $table->string("academic_degree")->nullable();
            $table->string("academic_title")->nullable();
            $table->string("foreign_lang");
            $table->string("military_rank")->nullable();
            $table->text("state_award")->nullable();
            $table->text("state_member")->nullable();
            $table->foreignId("user_id")->unique()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_education');
    }
}
