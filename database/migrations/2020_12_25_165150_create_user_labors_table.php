<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLaborsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_labors', function (Blueprint $table) {
            $table->id();
            $table->foreignId("section_id")->constrained();
            $table->foreignId("department_id")->constrained();
            $table->foreignId("position_id")->constrained();
            $table->string("stavka",50);
            $table->string("basis_labor",150);
            $table->string("working_limit");
            $table->string("start_command");
            $table->string("finish_command")->nullable();
            $table->string("last_vacation")->nullable();
            $table->string("maternity")->nullable();
            $table->string("pensioner")->nullable();
            $table->string("disciplinary")->nullable();
            $table->string("contract_number");
            $table->string("employment_num",30);
            $table->string("development")->nullable();
            $table->string("inn_number",20);
            $table->foreignId("user_id")->unique()->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_labors');
    }
}
