<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Position;
use App\Models\Section;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{

    private function getSections(): array
    {
        return [
            [
                'id' => 1,
                'title' => 'АУП',
                'alias' => 'aup',
            ],
            [
                'id' => 2,
                'title' => 'ППС',
                'alias' => 'pps',
            ],
            [
                'id' => 3,
                'title' => 'ЭТО',
                'alias' => 'eto',
            ],
        ];
    }
    private function getDepartments(): array
    {
        return [
            [
                'id' => 1,
                'title' => 'Ректорат',
                'alias' => 'rektorat',
                'section_id' => 1
            ],
            [
                'id' => 2,
                'title' => 'Умум-техника факултети',
                'alias' => 'umum-texnika-fakulteti',
                'section_id' => 1
            ],
            [
                'id' => 3,
                'title' => 'Ёшлар билан ишлаш бўлими',
                'alias' => 'yoslar-bilan-islas-blimi',
                'section_id' => 1
            ],
            [
                'id' => 4,
                'title' => 'Сифат назорати бўлими',
                'alias' => 'sifat-nazorati-blimi',
                'section_id' => 1
            ],
            [
                'id' => 5,
                'title' => 'Ўқув услубий ва илмий бўлим',
                'alias' => 'quv-uslubii-va-ilmii-blim',
                'section_id' => 1
            ],
            [
                'id' => 7,
                'title' => 'Маркетинг бўлими',
                'alias' => 'marketing-blimi',
                'section_id' => 1
            ],
            [
                'id' => 8,
                'title' => 'Ходимлар бўлими',
                'alias' => 'xodimlar-blimi',
                'section_id' => 1
            ],
            [
                'id' => 9,
                'title' => 'Ахборот технологиялари маркази',
                'alias' => 'axborot-texnologiyalari-markazi',
                'section_id' => 1
            ],
            [
                'id' => 10,
                'title' => 'Халқаро алоқалар бўлими',
                'alias' => 'xalqaro-aloqalar-blimi',
                'section_id' => 1
            ],
            [
                'id' => 11,
                'title' => 'Ахборот-ресурс маркази',
                'alias' => 'axborot-resurs-markazi',
                'section_id' => 1
            ],
            [
                'id' => 12,
                'title' => 'Бухгалтерия',
                'alias' => 'buxgalteriya',
                'section_id' => 1
            ],
            [
                'id' => 13,
                'title' => 'Молия режалаштириш бўлими',
                'alias' => 'moliya-rezalastiris-blimi',
                'section_id' => 1
            ],
            [
                'id' => 14,
                'title' => 'Девонхона ва архив',
                'alias' => 'devonxona-va-arxiv',
                'section_id' => 1
            ],
            [
                'id' => 15,
                'title' => 'Технопарк',
                'alias' => 'texnopark',
                'section_id' => 1
            ],
            [
                'id' => 16,
                'title' => 'Малака ошириш ва тайёрлов курслари маркази',
                'alias' => 'malaka-osiris-va-taiyorlov-kurslari-markazi',
                'section_id' => 1
            ],
            [
                'id' => 17,
                'title' => 'Спорт мажмуаси',
                'alias' => 'sport-mazmuasi',
                'section_id' => 1
            ],
            [
                'id' => 18,
                'title' => 'ЭТО',
                'alias' => 'eto',
                'section_id' => 3
            ],
            [
                'id' => 19,
                'title' => 'Кафедра "Естественно-Математическиx Дисциплин"',
                'alias' => 'kafedra-estestvenno-matematiceskix-disciplin',
                'section_id' => 2
            ],
            [
                'id' => 20,
                'title' => 'Кафедра "Гуманитарно – Экономических Дисциплины"',
                'alias' => 'kafedra-gumanitarno-ekonomiceskix-discipliny',
                'section_id' => 2
            ],
            [
                'id' => 21,
                'title' => 'Кафедра "Строительство и Архитектура"',
                'alias' => 'kafedra-stroitelstvo-i-arxitektura',
                'section_id' => 2
            ],
            [
                'id' => 22,
                'title' => 'Кафедра "Машиностроение и Авикосмический Инжиниринг"',
                'alias' => 'kafedra-masinostroenie-i-avikosmiceskii-inziniring',
                'section_id' => 2
            ],
            [
                'id' => 23,
                'title' => 'Кафедра "Автоматический Контроль и Компьютерный Инжиниринг"',
                'alias' => 'kafedra-avtomaticeskii-kontrol-i-kompyuternyi-inziniring',
                'section_id' => 2
            ],
            [
                'id' => 24,
                'title' => 'Ўқув ва ўқув ёрдамчи персонал',
                'alias' => 'quv-va-quv-yordamci-personal',
                'section_id' => 1,
            ]
        ];
    }
    private function getPositions(): array
    {
        return [
            [
                'id' => 1,
                'title' => 'Ректор',
                'alias' => 'rektor',
                'department_id' => 1
            ],
            [
                'id' => 2,
                'title' => 'Молиявий-иқтисодий ишлар ва инновациялар бўйича биринчи проректор',
                'alias' => 'moliyavii-iqtisodii-islar-va-innovaciyalar-biica-birinci-prorektor',
                'department_id' => 1
            ],
            [
                'id' => 3,
                'title' => 'Ёшлар билан ишлаш бўйича проректор',
                'alias' => 'yoslar-bilan-islas-biica-prorektor',
                'department_id' => 1
            ],
            [
                'id' => 4,
                'title' => 'Бош муҳандис',
                'alias' => 'bos-muandis',
                'department_id' => 1
            ],
            [
                'id' => 5,
                'title' => 'Сифат менеджменти тизими менеджери',
                'alias' => 'sifat-menedzmenti-tizimi-menedzeri',
                'department_id' => 1
            ],
            [
                'id' => 6,
                'title' => 'Юрист маслаҳатчи',
                'alias' => 'yurist-maslaatci',
                'department_id' => 1
            ],
            [
                'id' => 7,
                'title' => 'Ишюритувчи-котиб(а)',
                'alias' => 'isyurituvci-kotiba',
                'department_id' => 1
            ],
            [
                'id' => 8,
                'title' => 'Матбуот котиби',
                'alias' => 'matbuot-kotibi',
                'department_id' => 1
            ],
            [
                'id' => 9,
                'title' => 'Ректор ёрдамчиси',
                'alias' => 'rektor-yordamcisi',
                'department_id' => 1
            ],
            [
                'id' => 10,
                'title' => 'Ёшлар билан ишлаш бўлими бошлиғи',
                'alias' => 'yoslar-bilan-islas-blimi-bosligi',
                'department_id' => 3
            ],
            [
                'id' => 11,
                'title' => 'Ёшлар билан ишлаш бўлими методисти',
                'alias' => 'yoslar-bilan-islas-blimi-metodisti',
                'department_id' => 3
            ],
            [
                'id' => 12,
                'title' => 'Ёшлар билан ишлаш бўлими методисти',
                'alias' => 'yoslar-bilan-islas-blimi-metodisti-1597904543',
                'department_id' => 3
            ],
            [
                'id' => 13,
                'title' => 'Психолог',
                'alias' => 'psixolog',
                'department_id' => 3
            ],
            [
                'id' => 14,
                'title' => 'Умум-техника факултети декани',
                'alias' => 'umum-texnika-fakulteti-dekani',
                'department_id' => 2
            ],
            [
                'id' => 15,
                'title' => 'Умум-техника факултети декани муовини',
                'alias' => 'umum-texnika-fakulteti-dekani-muovini',
                'department_id' => 2
            ],
            [
                'id' => 16,
                'title' => 'Умум-техника факултети методисти',
                'alias' => 'umum-texnika-fakulteti-metodisti',
                'department_id' => 2
            ],
            [
                'id' => 17,
                'title' => 'Умум-техника факултети инспектори',
                'alias' => 'umum-texnika-fakulteti-inspektori',
                'department_id' => 2
            ],
            [
                'id' => 18,
                'title' => 'Ўқув услубий ва илмий бўлими бошлиғи',
                'alias' => 'quv-uslubii-va-ilmii-blimi-bosligi-1597904756',
                'department_id' => 5
            ],
            [
                'id' => 19,
                'title' => 'Ўқув услубий ва илмий бўлими методисти',
                'alias' => 'quv-uslubii-va-ilmii-blimi-metodisti',
                'department_id' => 5
            ],
            [
                'id' => 20,
                'title' => 'Ўқув услубий ва илмий бўлими инспектори',
                'alias' => 'quv-uslubii-va-ilmii-blimi-inspektori',
                'department_id' => 5
            ],
            [
                'id' => 21,
                'title' => 'Ишлаб чиқариш амалиётини ташкиллаштириш бўйича методист',
                'alias' => 'islab-ciqaris-amaliyotini-taskillastiris-biica-metodist',
                'department_id' => 5
            ],
            [
                'id' => 22,
                'title' => 'Статист',
                'alias' => 'statist',
                'department_id' => 5
            ],
            [
                'id' => 23,
                'title' => 'Диспетчер',
                'alias' => 'dispetcer',
                'department_id' => 5
            ],
            [
                'id' => 24,
                'title' => 'Илмий тадқиқотлар бўйича муҳандис',
                'alias' => 'ilmii-tadqiqotlar-biica-muandis',
                'department_id' => 5
            ],
            [
                'id' => 25,
                'title' => 'Катта илмий ходим',
                'alias' => 'katta-ilmii-xodim',
                'department_id' => 5,
            ],
            [
                'id' => 26,
                'title' => 'Маркетинг бўлими бошлиғи',
                'alias' => 'marketing-blimi-bosligi',
                'department_id' => 7
            ],
            [
                'id' => 27,
                'title' => 'Маркетинг бўлими инспектори',
                'alias' => 'marketing-blimi-inspektori',
                'department_id' => 7
            ],
            [
                'id' => 28,
                'title' => 'Маркетолог',
                'alias' => 'marketolog',
                'department_id' => 7
            ],
            [
                'id' => 29,
                'title' => 'Ходимлар бўлими бошлиғи',
                'alias' => 'xodimlar-blimi-bosligi',
                'department_id' => 8
            ],
            [
                'id' => 30,
                'title' => 'Ходимлар билан ишлаш бўйича мутахассис',
                'alias' => 'xodimlar-bilan-islas-biica-mutaxassis',
                'department_id' => 8
            ],
            [
                'id' => 31,
                'title' => 'Халқаро алоқалар бўлими бошлиғи',
                'alias' => 'xalqaro-aloqalar-blimi-bosligi',
                'department_id' => 10
            ],
            [
                'id' => 32,
                'title' => 'Халқаро алоқалар бўлими методисти',
                'alias' => 'xalqaro-aloqalar-blimi-metodisti',
                'department_id' => 10
            ],
            [
                'id' => 33,
                'title' => 'Ахборот технологиялари маркази бошлиғи',
                'alias' => 'axborot-texnologiyalari-markazi-bosligi',
                'department_id' => 9
            ],
            [
                'id' => 34,
                'title' => 'Мухандис-дастурчи',
                'alias' => 'muxandis-dasturci',
                'department_id' => 9
            ],
            [
                'id' => 35,
                'title' => 'Ахборот ресурс маркази мудираси',
                'alias' => 'axborot-resurs-markazi-mudirasi',
                'department_id' => 11
            ],
            [
                'id' => 36,
                'title' => 'Кутубхоначи',
                'alias' => 'kutubxonaci',
                'department_id' => 11
            ],
            [
                'id' => 37,
                'title' => 'Электрон кутубхона кутубхоначиси',
                'alias' => 'elektron-kutubxona-kutubxonacisi',
                'department_id' => 11
            ],
            [
                'id' => 38,
                'title' => 'Бош бухгалтер',
                'alias' => 'bos-buxgalter',
                'department_id' => 12
            ],
            [
                'id' => 39,
                'title' => 'Бош бухгалтер ўринбосари',
                'alias' => 'bos-buxgalter-rinbosari',
                'department_id' => 12
            ],
            [
                'id' => 40,
                'title' => '1-тоифали бухгалтер',
                'alias' => '1-toifali-buxgalter',
                'department_id' => 12
            ],
            [
                'id' => 41,
                'title' => 'Кассир',
                'alias' => 'kassir',
                'department_id' => 12
            ],
            [
                'id' => 42,
                'title' => 'Молия режалаштириш бўлими бошлиғи',
                'alias' => 'moliya-rezalastiris-blimi-bosligi',
                'department_id' => 13
            ],
            [
                'id' => 43,
                'title' => '1-тоифали иқтисодчи',
                'alias' => '1-toifali-iqtisodci',
                'department_id' => 13
            ],
            [
                'id' => 44,
                'title' => 'Девонхона ва архив мудираси',
                'alias' => 'devonxona-va-arxiv-mudirasi',
                'department_id' => 14
            ],
            [
                'id' => 45,
                'title' => 'Девонхона ва архив методисти',
                'alias' => 'devonxona-va-arxiv-metodisti',
                'department_id' => 14
            ],
            [
                'id' => 46,
                'title' => 'Архивариус',
                'alias' => 'arxivarius',
                'department_id' => 14
            ],
            [
                'id' => 47,
                'title' => 'Катта лаборант',
                'alias' => 'katta-laborant',
                'department_id' => 24
            ],
            [
                'id' => 48,
                'title' => 'Лаборант',
                'alias' => 'laborant',
                'department_id' => 24
            ],
            [
                'id' => 49,
                'title' => 'Директор',
                'alias' => 'direktor',
                'department_id' => 15
            ],
            [
                'id' => 50,
                'title' => 'Лаборатория мудири',
                'alias' => 'laboratoriya-mudiri',
                'department_id' => 15
            ],
            [
                'id' => 51,
                'title' => 'Янги лойиҳалар бўйича муҳандис',
                'alias' => 'yangi-loiialar-biica-muandis',
                'department_id' => 15
            ],
            [
                'id' => 52,
                'title' => 'Координатор',
                'alias' => 'koordinator',
                'department_id' => 16
            ],
            [
                'id' => 53,
                'title' => 'марказ методисти',
                'alias' => 'markaz-metodisti',
                'department_id' => 16
            ],
            [
                'id' => 54,
                'title' => 'Администратор',
                'alias' => 'administrator',
                'department_id' => 17
            ],
            [
                'id' => 55,
                'title' => 'Спорт мураббийи',
                'alias' => 'sport-murabbiii',
                'department_id' => 17
            ],
            [
                'id' => 56,
                'title' => 'Йўриқчи-методист',
                'alias' => 'iriqci-metodist',
                'department_id' => 17
            ],
            [
                'id' => 57,
                'title' => 'Катта лаборант',
                'alias' => 'katta-laborant-1597906481',
                'department_id' => 17
            ],
        ];
    }

    public function run()
    {

        foreach ($this->getSections() as $section){
            $model = Section::create($section);
        }

        foreach ($this->getDepartments() as $department){
            $model = Department::create($department);
        }

        foreach ($this->getPositions() as $position){
            $model = Position::create($position);
        }


    }
}
