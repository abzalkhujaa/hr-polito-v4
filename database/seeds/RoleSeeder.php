<?php

namespace Database\Seeders;

use App\Helpers\Enums\UserRoleEnum;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = UserRoleEnum::toArray();

        foreach ($roles as $one){
            $role = Role::create(['name' => $one]);
        }

    }
}
