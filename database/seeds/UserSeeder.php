<?php

namespace Database\Seeders;

use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'username' => 'abzalkhujaa',
            'first_name' => 'Abzalkhuja',
            'last_name' => 'Abrolkhujaev',
            'patronymic' => 'Ahmadkhuja ugli',
            'email' => 'abzalkhujaa@gmail.com',
            'password' => Hash::make('qwerty123$'),
            'email_verified_at' => now(),
            'reset_password' => true,
            'gender' => 'M'
        ]);

        $user2 = User::create([
            'username' => 'j_gayratov',
            'first_name' => 'Jasur',
            'last_name' => 'Gayratov',
            'patronymic' => 'Botir ugli',
            'email' => 'j_gayratov@gmail.com',
            'password' => Hash::make('qwerty123$'),
            'email_verified_at' => now(),
            'reset_password' => true,
            'gender' => 'M'
        ]);
    }
}
