<?php

namespace Database\Seeders;

use App\Helpers\Enums\UserRoleEnum;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::whereUsername('abzalkhujaa')->firstOrFail()->syncRoles([UserRoleEnum::SUPER]);
        User::whereUsername('j_gayratov')->firstOrFail()->syncRoles([UserRoleEnum::ADMIN]);
    }
}
