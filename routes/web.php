<?php

use App\Helpers\Enums\UserRoleEnum;
use App\Http\Controllers\LanguageController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//  HOME ================================
Route::namespace('Front')->group(function (){
    Route::get('/auth/qrcode/{user:username}','QRCodeController@login')->name('auth.qrcode');
    Route::get('/auth/blocked/{user:username}','QRCodeController@blocked')->name('auth.blocked')->middleware(['auth']);
    Route::get('/check/certificate/{user:username}',function (User $user){})->name('check.certificate');
    Route::as('home.')->group(function (){

        Route::view('/', 'front.page.index')->name('index');
        Route::get('news','NewsController@index')->name('news.index');
        Route::get('news/{news:alias}','NewsController@show')->name('news.show');
        Route::get('decree','DecreeController@index')->name('decree.index');
        Route::get('decree/{decree:alias}','DecreeController@show')->name('decree.show');
        Route::get('certificate','CertificateController@index')->name('certificate.index');
        Route::post('certificate','CertificateController@search')->name('certificate.search');
        Route::get('certificate/{user:username}/download', 'CertificateController@download')->name('certificate.download');

        Route::prefix('vacancy')->as('vacancy.')->group(function (){
            Route::get('/','VacancyController@index')->name('index');
            Route::get('/{section:alias}','VacancyController@group')->name('group');
            Route::get('/{section:alias}/{vacancy:alias}','VacancyController@show')->name('show');
            Route::post('/{section:alias}/{vacancy:alias}','VacancyController@application')->name('application');
        });


    });
});

//  AUTH ================================
Auth::routes(['verify' => false]);

//  DASHBOARD ================================
Route::prefix('dashboard')
    ->namespace('Dashboard')
    ->as('dashboard.')
    ->middleware(['auth','role:'.UserRoleEnum::SUPER.'|'.UserRoleEnum::ADMIN.'|'.UserRoleEnum::MODERATOR])
    ->group(function (){
        Route::get('/','DashboardController@index')->name('index');

        //    News ======================================================================================
        Route::resource('news','NewsController')->except('show')->parameters(['news' => 'news:alias']);
        //    Decree ======================================================================================
        Route::resource('decree','DecreeController')->except('show')->parameters(['decree' => 'decree:alias']);
        //    Vacancy ======================================================================================
        Route::resource('vacancy','VacancyController')->parameters(['vacancy' => 'vacancy:alias']);

        Route::get('application','DashboardController@index')->name('vacancy.application');

        Route::prefix('group')->namespace('Group')->as('group.')->group(function (){
            Route::get("/",'GroupController@index')->name('index');

            Route::resource('section','SectionController')->except('show','edit','create');
            Route::resource('department','DepartmentController')->except('show','edit','create');
            Route::resource('position','PositionController')->except('show','edit','create');
        });

        Route::get('certificate','CertificateController@index')->name('certificate.index');
        Route::get('certificate/template','CertificateController@index')->name('certificate.template');

        Route::get('activity','DashboardController@index')->name('activity');
        Route::get('archive','DashboardController@index')->name('archive');

        Route::prefix('/staff')->namespace('Staff')->as('staff.')->group(function(){
            Route::get('/','StaffController@index')->name('index');
            Route::get('table','StaffController@table')->name('table');
            Route::get('excel','StaffController@exportExcel')->name('excel');
            Route::get('control','StaffController@control')->name('control');
            Route::get('create','ProfileController@create')->name('create');
            Route::post('create','ProfileController@store')->name('store');

            Route::prefix('{user:username}')->group(function (){
                Route::get('/','ProfileController@index')->name('profile.index');
                Route::delete('/','ProfileController@destroy')->name('profile.destroy');

                Route::put('profile','ProfileController@update')->name('profile.update');
                Route::put('profile/password/new', 'ProfileController@changePassword')->name('profile.password.new');
                Route::put('profile/password.reset', 'ProfileController@resetPassword')->name('profile.password.reset');

                Route::get('information','InformationController@index')->name('information.index');
                Route::post('information','InformationController@store')->name('information.store');
                Route::put('information','InformationController@update')->name('information.update');
                Route::delete('information','InformationController@destroy')->name('information.destroy');

                Route::get('education','EducationController@index')->name('education.index');
                Route::post('education','EducationController@store')->name('education.store');
                Route::put('education','EducationController@update')->name('education.update');
                Route::delete('education','EducationController@destroy')->name('education.destroy');

                Route::post('education/finished','EducationFinishedController@store')->name('education.finished.store');
                Route::put('education/finished/{finished}','EducationFinishedController@update')->name('education.finished.update');
                Route::delete('education/finished/{finished}','EducationFinishedController@destroy')->name('education.finished.destroy');

                Route::get('labor','LaborController@index')->name('labor.index');
                Route::post('labor','LaborController@store')->name('labor.store');
                Route::put('labor','LaborController@update')->name('labor.update');
                Route::delete('labor','LaborController@destroy')->name('labor.destroy');

                Route::get('work','WorkController@index')->name('work.index');
                Route::post('work','WorkController@store')->name('work.store');
                Route::put('work/{work}','WorkController@update')->name('work.update');
                Route::delete('work/{work}','WorkController@destroy')->name('work.destroy');

                Route::get('vacation','VacationController@index')->name('vacation.index');
                Route::post('vacation','VacationController@store')->name('vacation.store');
                Route::put('vacation/{vacation}','VacationController@update')->name('vacation.update');
                Route::delete('vacation/{vacation}','VacationController@destroy')->name('vacation.destroy');

                Route::get('family','FamilyController@index')->name('family.index');
                Route::post('family','FamilyController@store')->name('family.store');
                Route::put('family/{family}','FamilyController@update')->name('family.update');
                Route::delete('family/{family}','FamilyController@destroy')->name('family.destroy');

                Route::get('file','FileController@index')->name('file.index');
                Route::post('file','FileController@store')->name('file.store');
                Route::put('file','FileController@update')->name('file.update');
                Route::delete('file','FileController@destroy')->name('file.destroy');

                Route::get('reference', 'StaffController@reference')->name('reference');
                Route::get('certificate', 'StaffController@certificate')->name('certificate');
            });
        });
});

Route::prefix('staff-dashboard')
    ->namespace('StaffDashboard')
    ->as('staff-dashboard.')
    ->middleware(['auth','role:'.UserRoleEnum::STAFF])
    ->group(function (){
        Route::get('/','StaffDashboardController@index')->name('index');
        Route::get('reference', 'StaffController@reference')->name('staff.reference');
        Route::get('certificate', 'StaffController@certificate')->name('staff.certificate');

        Route::prefix('/pagination')->as('staff.')->namespace('Pagination')->group(function (){
            Route::get('profile','ProfileController@index')->name('profile.index');

            Route::put('profile','ProfileController@update')->name('profile.update');
            Route::put('profile/password/new', 'ProfileController@changePassword')->name('profile.password.new');

            Route::get('information','InformationController@index')->name('information.index');
            Route::post('information','InformationController@store')->name('information.store');
            Route::put('information','InformationController@update')->name('information.update');

            Route::get('education','EducationController@index')->name('education.index');
            Route::post('education','EducationController@store')->name('education.store');
            Route::put('education','EducationController@update')->name('education.update');

            Route::post('education/finished','EducationFinishedController@store')->name('education.finished.store');
            Route::put('education/finished/{finished}','EducationFinishedController@update')->name('education.finished.update');
            Route::delete('education/finished/{finished}','EducationFinishedController@destroy')->name('education.finished.destroy');

            Route::get('labor','LaborController@index')->name('labor.index');
            Route::post('labor','LaborController@store')->name('labor.store');
            Route::put('labor','LaborController@update')->name('labor.update');

            Route::get('work','WorkController@index')->name('work.index');
            Route::post('work','WorkController@store')->name('work.store');
            Route::put('work/{work}','WorkController@update')->name('work.update');
            Route::delete('work/{work}','WorkController@destroy')->name('work.destroy');

            Route::get('family','FamilyController@index')->name('family.index');
            Route::post('family','FamilyController@store')->name('family.store');
            Route::put('family/{family}','FamilyController@update')->name('family.update');
            Route::delete('family/{family}','FamilyController@destroy')->name('family.destroy');

            Route::get('file','FileController@index')->name('file.index');
            Route::post('file','FileController@store')->name('file.store');
            Route::put('file','FileController@update')->name('file.update');

            Route::get('vacation','VacationController@index')->name('vacation.index');

        });
    });

Route::prefix('file')
    ->as('file.')
    ->namespace('File')
    ->middleware(['auth','role:'.UserRoleEnum::SUPER.'|'.UserRoleEnum::ADMIN.'|'.UserRoleEnum::MODERATOR.'|'.UserRoleEnum::STAFF])
    ->group(function(){
        Route::post('upload','FileController@upload')->name('upload');
        Route::delete('delete','FileController@delete')->name('delete');
    });



// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);



// ======================================================================================================================================


