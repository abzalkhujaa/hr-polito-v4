@setup
    $user = "laravel-deployer";
    $timezone = 'Asia/Tashkent';
    $path = '/var/www/gitlab/new-hr-polito';
    $current = $path . '/current';
    $repo = 'https://gitlab.com/abzalkhujaa/hr-polito-v4.git';
    $chmod = 'storage/logs';
    $branch = 'master';
    $date = new DateTime("now", new DateTimeZone($timezone));
    $release = $path.'/releases/'.$date->format("YmdHis");
@endsetup

@servers(["production" => $user."@hr.polito.uz -p1295"])

@task("clone",["on" => $on])
    mkdir -p {{ $release }}
    git clone --depth 1 -b {{$branch}} {{$repo}} {{ $release }}

    echo "#1 - Repository has been cloned"
@endtask

@task("pull",["on" => $on])
    git config --global user.name "Abzalkhuja Abrorkhujaev"
    git config --global user.email "abzalkhujaa@gmail.com"

    cd {{ $current }}
    git add .
    git commit -m "Before pulling"
    git pull --no-commit
    echo "========== Pulling changes =========="
@endtask

@task("composer",["on" => $on])
    composer self-update

    cd {{ $release }}

    composer install --no-interaction --prefer-dist

    echo "#2 - Composer dependencies have been installed"
@endtask

@task("artisan",["on" => $on])
    cd {{ $release }}
    ln -nfs {{ $path }}/.env .env;
    chgrp -h www-data .env;

    php artisan config:clear

    php artisan vendor:publish --force --tag=livewire:assets --ansi
    php artisan storage:link

    echo "#3 - Production dependencies have been installed"
@endtask

@task("npm_runner",['on' => $on])
    cd {{ $release }}
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    npm install
    npm run production
    echo "#4 - Production dependencies have been installed"
@endtask

@task('chmod',["on" => $on])
    chgrp -R www-data {{ $release }};
    chmod -R ug+rwx {{ $release }}

    chmod -R 775 {{ $release }}/{{ $chmods }}
    chown -R {{ $user }}:www-data {{ $release }}/{{$chmods}}
    echo "Permission have been set for {{ $chmods }}"

    echo "#5 - Permission has been set"
@endtask

@task("update_symlinks")
    ln -nfs {{ $release }} {{ $current }}
    chgrp -h www-data {{ $current }}

    echo "#6 - Symlink has been set"
@endtask

@task('releases_clean')
    purging=$(ls -dt {{$release}}/* | tail -n +3);

    if [ "$purging" != "" ]; then
    echo "# Purging old releases: $purging;"
    rm -rf $purging;
    else
    echo "# No releases found for purging at this time";
    fi
@endtask

@task('migration_fresh',['on' => $on])
    cd {{ $current }}
    php artisan migrate
@endtask

@task('migration_rollback',['on' => $on])
    cd {{ $current }}
    php artisan migrate:rollback
@endtask

@task('migration_seeder',['on' => $on])
    cd {{ $current }}
    php artisan db:seed
@endtask

@macro("deploy",["on" => "production"])
    clone
    composer
    artisan
    npm_runner
    chmod
    update_symlinks
@endmacro

@macro("run_migration",["on" => "production"])
    migration_fresh
    migration_seeder
@endmacro

@macro("run_migration_fresh",["on" => "production"])
    migration_rollback
    migration_fresh
    migration_seeder
@endmacro

@macro("run_migration_rollback",["on" => "production"])
    migration_rollback
@endmacro

@macro("git_pull",["on" => "production"])
    pull
@endmacro
